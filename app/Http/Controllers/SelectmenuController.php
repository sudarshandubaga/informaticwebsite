<?php

namespace App\Http\Controllers;

use App\Models\Selectmenu;
use Illuminate\Http\Request;

class SelectmenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('backend.pages.menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view ('backend.pages.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Selectmenu  $selectmenu
     * @return \Illuminate\Http\Response
     */
    public function show(Selectmenu $selectmenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Selectmenu  $selectmenu
     * @return \Illuminate\Http\Response
     */
    public function edit(Selectmenu $selectmenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Selectmenu  $selectmenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Selectmenu $selectmenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Selectmenu  $selectmenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Selectmenu $selectmenu)
    {
        //
    }
}
