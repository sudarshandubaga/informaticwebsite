<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Template;
use App\Models\Site;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $template = Template::query();

        if (!empty($request->search_by_title)) {
            $template->where('title', 'LIKE', '%' . $request->search_by_title . '%');
        }

        if (!empty($request->type) && $request->type === 'trash') {
            $template->onlyTrashed();
        }

        $template = $template->latest()->paginate(10);

        return view('superadmin.pages.template.index', compact('template'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $themes = Site::groupBy('theme')->pluck('theme')->toArray();

        return view('superadmin.pages.template.create', compact('themes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $template        = new Template();
        $template->name = $request->name;
        $template->theme = $request->theme;
        $template->slug = $request->slug;

        $template->save();

        return redirect(route('super.admin.template.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Template $template)
    {
        $request->replace($template->toarray());
        $request->flash();

        $themes = Site::groupBy('theme')->pluck('theme')->toArray();

        return view('superadmin.pages.template.edit', compact('template', 'themes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Template $template)
    {
        $template->name = $request->name;
        $template->theme = $request->theme;
        $template->slug = $request->slug;

        $template->save();

        return redirect(route('super.admin.template.index'))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $template = Template::withTrashed()->findOrFail($id);
        // dd($request->action);

        if ($request->action) {
            switch ($request->action) {
                case 'restore':
                    $template->restore();
                    return redirect(route('super.admin.template.index'))->with('success', 'Your data has been restored.');
                    break;

                case 'permanent-delete':
                    if (!empty($template->image)) {
                        $dir = public_path('images/templates/' . $template->image);
                        if (file_exists($dir)) unlink($dir);
                        $dir = public_path('images/templates/thumbnail/' . $template->image);
                        if (file_exists($dir)) unlink($dir);
                    }
                    $template->forceDelete();

                    return redirect(route('super.admin.template.index'))->with('success', 'Your data has been deleted from trash.');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $template->delete();
            return redirect()->back()->with('success', 'Your data has been deleted.');
        }
    }
}
