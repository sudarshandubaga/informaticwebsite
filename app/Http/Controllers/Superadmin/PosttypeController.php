<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Posttype;
use App\Models\Site;
use Illuminate\Http\Request;

class PosttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $posttype = PostType::with(['post_fields']);



        if (!empty($request->search_by_title)) {
            $posttype->where('title', 'LIKE', '%' . $request->search_by_title . '%');
        }

        if (!empty($request->type) && $request->type === 'trash') {
            $posttype->onlyTrashed();
        }

        $posttype = $posttype->latest()->paginate(10);
        // dd($posttype->toArray());
        return view('superadmin.pages.posttype.index', compact('posttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $site = Site::pluck('domain', 'id')->toArray();

        return view('superadmin.pages.posttype.create', compact('site'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posttype        = new PostType();
        $posttype->name = $request->name;
        $posttype->slug = $request->slug;
        $posttype->is_category = $request->is_category;
        $posttype->site_id = $request->site_id;

        $posttype->save();

        $postFields = $request->post_field;
        $postFields['site_id'] = $request->site_id;
        $posttype->post_fields()->updateOrCreate([
            'site_id' => $request->site_id,
            'post_type' => $posttype->slug
        ], $postFields);

        return redirect(route('super.admin.posttype.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Posttype  $posttype
     * @return \Illuminate\Http\Response
     */
    public function show(Posttype $posttype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Posttype  $posttype
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Posttype $posttype)
    {

        $editData = $posttype->toArray();

        if (!empty($posttype->post_fields)) {
            $editData['post_field'] = $posttype->post_fields->toArray();
            foreach ($posttype->post_fields->toArray() as $field => $v) {
                $editData[$field] = $v;
            }
        }
        $request->replace($editData);
        $request->flash();

        $site = Site::pluck('domain', 'id')->toArray();


        return view('superadmin.pages.posttype.edit', compact('posttype', 'site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Posttype  $posttype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posttype $posttype)
    {
        $posttype->name = $request->name;
        $posttype->slug = $request->slug;
        $posttype->is_category = $request->is_category;
        $posttype->site_id = $request->site_id;

        $posttype->save();

        $postFields = $request->post_field;
        $postFields['site_id'] = $request->site_id;
        $posttype->post_fields()->updateOrCreate([
            'site_id' => $request->site_id,
            'post_type' => $posttype->slug
        ], $postFields);


        return redirect(route('super.admin.posttype.index'))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Posttype  $posttype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $posttype = Posttype::withTrashed()->findOrFail($id);
        // dd($request->action);

        if ($request->action) {
            switch ($request->action) {
                case 'restore':
                    $posttype->restore();
                    return redirect(route('super.admin.posttype.index'))->with('success', 'Your data has been restored.');
                    break;

                case 'permanent-delete':
                    if (!empty($posttype->image)) {
                        $dir = public_path('images/posttypes/' . $posttype->image);
                        if (file_exists($dir)) unlink($dir);
                        $dir = public_path('images/posttypes/thumbnail/' . $posttype->image);
                        if (file_exists($dir)) unlink($dir);
                    }
                    $posttype->forceDelete();

                    return redirect(route('super.admin.posttype.index'))->with('success', 'Your data has been deleted from trash.');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $posttype->delete();
            return redirect()->back()->with('success', 'Your data has been deleted.');
        }
    }
}
