<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Site;
use Intervention\Image\Facades\Image;


use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $site = Site::query();



        if (!empty($request->search_by_title)) {
            $site->where('title', 'LIKE', '%' . $request->search_by_title . '%');
        }

        if (!empty($request->type) && $request->type === 'trash') {
            $site->onlyTrashed();
        }

        $site = $site->latest()->paginate(10);

        return view('superadmin.pages.site.index', compact('site'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $themes = Site::groupBy('theme')->pluck('theme')->toArray();

        return view('superadmin.pages.site.create', compact('themes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $site        = new Site();
        $site->title = $request->title;
        $site->tagline = $request->tagline;
        $site->theme = $request->theme;
        $site->domain = $request->domain;

        if ($request->hasfile('logo')) {
            $site->logo = $request->file('logo')->store($this->_site->domain . '/imgs/logo/', 'public');
        }
        if ($request->hasfile('footer_logo')) {
            $site->footer_logo = $request->file('footer_logo')->store($this->_site->domain . '/imgs/footer_logo/', 'public');
        }
        if ($request->hasfile('favicon')) {
            $site->favicon = $request->file('favicon')->store($this->_site->domain . '/imgs/favicon/', 'public');
        }

        $site->save();

        return redirect(route('super.admin.site.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Site $site)
    {
        $request->replace($site->toarray());
        $request->flash();

        $themes = Site::groupBy('theme')->pluck('theme')->toArray();


        return view('superadmin.pages.site.edit', compact('site', 'themes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        $site->title = $request->title;
        $site->tagline = $request->tagline;
        $site->theme = $request->theme;

        if ($request->hasfile('logo')) {
            $site->logo = $request->file('logo')->store($this->_site->domain . '/imgs/logo/', 'public');
        }
        if ($request->hasfile('footer_logo')) {
            $site->footer_logo = $request->file('footer_logo')->store($this->_site->domain . '/imgs/footer_logo/', 'public');
        }
        if ($request->hasfile('favicon')) {
            $site->favicon = $request->file('favicon')->store($this->_site->domain . '/imgs/favicon/', 'public');
        }

        $site->save();

        return redirect(route('super.admin.site.index'))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $site = Site::withTrashed()->findOrFail($id);
        // dd($request->action);

        if ($request->action) {
            switch ($request->action) {
                case 'restore':
                    $site->restore();
                    return redirect(route('super.admin.site.index'))->with('success', 'Your data has been restored.');
                    break;

                case 'permanent-delete':
                    if (!empty($site->image)) {
                        $dir = public_path('images/sites/' . $site->image);
                        if (file_exists($dir)) unlink($dir);
                        $dir = public_path('images/sites/thumbnail/' . $site->image);
                        if (file_exists($dir)) unlink($dir);
                    }
                    $site->forceDelete();

                    return redirect(route('super.admin.site.index'))->with('success', 'Your data has been deleted from trash.');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $site->delete();
            return redirect()->back()->with('success', 'Your data has been deleted.');
        }
    }
}
