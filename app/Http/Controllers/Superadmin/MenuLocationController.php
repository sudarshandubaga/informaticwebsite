<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\MenuLocation;
use App\Models\Site;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $menulocation = MenuLocation::query();

        if (!empty($request->search_by_title)) {
            $menulocation->where('title', 'LIKE', '%' . $request->search_by_title . '%');
        }

        if (!empty($request->type) && $request->type === 'trash') {
            $menulocation->onlyTrashed();
        }

        $menulocation = $menulocation->latest()->paginate(10);

        return view('superadmin.pages.menulocation.index', compact('menulocation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $site = Site::pluck('domain', 'id')->toArray();

        $menus = Menu::pluck('location', 'location')->toArray();

        return view('superadmin.pages.menulocation.create', compact('site', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menulocation        = new MenuLocation();
        $menulocation->name = $request->name;
        $menulocation->keyword = $request->keyword;
        $menulocation->site_id = $request->site_id;

        $menulocation->save();

        return redirect(route('super.admin.menulocation.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function show(MenuLocation $menuLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,  MenuLocation $menulocation)
    {
        $request->replace($menulocation->toArray());
        $request->flash();
        $menus = Menu::pluck('location', 'location')->toArray();
        $site = Site::pluck('domain', 'id')->toArray();

        return view('superadmin.pages.menulocation.edit', compact('menulocation', 'menus', 'site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuLocation $menulocation)
    {
        $menulocation->name = $request->name;
        $menulocation->keyword = $request->keyword;
        $menulocation->site_id = $request->site_id;

        $menulocation->save();

        return redirect(route('super.admin.menulocation.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $menulocation = MenuLocation::withTrashed()->findOrFail($id);
        // dd($request->action);

        if ($request->action) {
            switch ($request->action) {
                case 'restore':
                    $menulocation->restore();
                    return redirect(route('super.admin.menulocation.index'))->with('success', 'Your data has been restored.');
                    break;

                case 'permanent-delete':
                    if (!empty($menulocation->image)) {
                        $dir = public_path('images/menulocations/' . $menulocation->image);
                        if (file_exists($dir)) unlink($dir);
                        $dir = public_path('images/menulocations/thumbnail/' . $menulocation->image);
                        if (file_exists($dir)) unlink($dir);
                    }
                    $menulocation->forceDelete();

                    return redirect(route('super.admin.menulocation.index'))->with('success', 'Your data has been deleted from trash.');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $menulocation->delete();
            return redirect()->back()->with('success', 'Your data has been deleted.');
        }
    }
}
