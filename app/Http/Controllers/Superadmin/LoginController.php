<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function index()
    {
        return view('superadmin.pages.login');
    }

    public function login(Request $request)
    {
        $is_login = \Auth::guard('superadmin')->attempt($request->only(['email', 'password'], $request->remember));

        if ($is_login) {
            return redirect(route('super.admin.dashboard'));
        } else {
            return redirect()->back()->withErrors(['msg' => 'Login failed! Credentials are not matched.']);
        }
    }

    public function logout()
    {
        \Auth::guard('superadmin')->logout();

        return redirect(route('super.admin.login'));
    }
}
