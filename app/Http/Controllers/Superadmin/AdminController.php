<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Site;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $admin = Admin::query();

        if (!empty($request->search_by_title)) {
            $admin->where('title', 'LIKE', '%' . $request->search_by_title . '%');
        }

        if (!empty($request->type) && $request->type === 'trash') {
            $admin->onlyTrashed();
        }

        $admin = $admin->latest()->paginate(10);

        return view('superadmin.pages.admin.index', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $site = Site::pluck('domain', 'id')->toArray();

        return view('superadmin.pages.admin.create', compact('site'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin        = new Admin();
        $admin->name = $request->name;
        $admin->email = $request->email;
        if (!empty($request->password))
            $admin->password = \Hash::make($request->password);
        $admin->site_id = $request->site_id;

        $admin->save();

        return redirect(route('super.admin.admin.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Admin $admin)
    {
        $request->replace($admin->toArray());
        $request->flash();
        $site = Site::pluck('domain', 'id')->toArray();
        return view('superadmin.pages.admin.edit', compact('admin', 'site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $admin->name = $request->name;
        $admin->email = $request->email;
        if (!empty($request->password))
            $admin->password = \Hash::make($request->password);
        $admin->site_id = $request->site_id;

        $admin->save();

        return redirect(route('super.admin.admin.index'))->with('success', 'Your data has been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $admin = Admin::withTrashed()->findOrFail($id);
        // dd($request->action);

        if ($request->action) {
            switch ($request->action) {
                case 'restore':
                    $admin->restore();
                    return redirect(route('super.admin.admin.index'))->with('success', 'Your data has been restored.');
                    break;

                case 'permanent-delete':
                    if (!empty($admin->image)) {
                        $dir = public_path('images/admins/' . $admin->image);
                        if (file_exists($dir)) unlink($dir);
                        $dir = public_path('images/admins/thumbnail/' . $admin->image);
                        if (file_exists($dir)) unlink($dir);
                    }
                    $admin->forceDelete();

                    return redirect(route('super.admin.admin.index'))->with('success', 'Your data has been deleted from trash.');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $admin->delete();
            return redirect()->back()->with('success', 'Your data has been deleted.');
        }
    }
}
