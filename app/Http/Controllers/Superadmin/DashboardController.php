<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function dashboard()
    {
        return view('superadmin.pages.dashboard');
    }
}
