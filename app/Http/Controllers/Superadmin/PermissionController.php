<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\Posttype;
use App\Models\Admin;
use App\Models\PermissionData;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        $admin = Admin::pluck('name', 'id');
        $themes = Site::pluck('domain', 'id');
        $all_post_types = PostType::pluck('name', 'slug');
        return view('superadmin.pages.permission.create', compact('all_post_types', 'themes', 'admin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $p_query = Permission::where('site_id', $request->site_id)->where('admin_id', $request->admin_id);

        $permission = $p_query->count() ? $p_query->first() : new Permission();
        $permission->site_id = $request->site_id;
        $permission->admin_id = $request->admin_id;
        // $permission->name = $request->name;

        $permission->save();

        PermissionData::where('permission_id', $permission->id)->delete();

        foreach ($request->names as $name) {
            $permission_data = new PermissionData();
            $permission_data->permission_id = $permission->id;
            $permission_data->name = $name;
            $permission_data->save();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }
}
