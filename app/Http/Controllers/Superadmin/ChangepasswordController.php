<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


class ChangepasswordController extends Controller
{

    public function index()
    {
        return view('superadmin.pages.changepassword');
    }

    public function update(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        $user = auth()->guard('superadmin')->user();

        if (password_verify($request->current_password, $user->password)) {
            $user->password = \Hash::make($request->password);
            $user->save();
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors(['msg' => 'Current password is not matched.']);
        }
    }
}
