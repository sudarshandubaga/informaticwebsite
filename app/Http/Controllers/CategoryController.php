<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\PostType;
use Illuminate\Http\Request;
use App\Models\Template;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostType $post_type)
    {
        $category = Category::where('type', $post_type->slug)->where('site_id', $this->_site->id)->latest()->paginate(10);
        return view('backend.pages.category.index', compact('post_type', 'category'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PostType $post_type)
    {
        $categories = Category::where('type', $post_type->slug)->where('site_id', $this->_site->id)->orderBy('title')->pluck('title', 'id');
        return view('backend.pages.category.create', compact('post_type', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_type)
    {
        $category = new Category();
        $category->title = $request->title;
        $category->slug = $request->slug;
        $category->category_id = $request->category_id;
        $category->site_id = $this->_site->id;
        $category->description = $request->description;
        $category->type = $post_type;
        
        if ($request->hasfile('image')){
            $category->image = $request->file('image')->store($this->_site->domain . '/imgs/category/', 'public');
        }

        $category->save();


        return redirect( route('admin.category.index', [$post_type]) );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $post_type_slug,Category $category)
    {
        $post_type = PostType::where('slug', $post_type_slug)->firstOrFail();
        $category_arr = $category->toArray();
        $request->replace($category_arr);

        $request->flash();

        
        $categories = Category::where('type', $post_type->slug)->where('id', '!=', $category->id)->where('site_id', $this->_site->id)->orderBy('title')->pluck('title', 'id');


        return view('backend.pages.category.edit', compact('category','post_type', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$post_type, Category $category)
    {

        $category->title = $request->title;
        $category->slug = $request->slug;
        $category->category_id = $request->category_id;
        $category->site_id = $this->_site->id;
        $category->description = $request->description;
        
        if ($request->hasfile('image')){
            $category->image = $request->file('image')->store($this->_site->domain . '/imgs/category/', 'public');
        }

        $category->save();


        return redirect( route('admin.category.index', [$post_type]) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_type, Category $category)
    {
        $category->delete();
        return redirect( route('admin.category.index', [$post_type]) );
    }
}
