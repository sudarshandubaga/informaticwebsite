<?php

namespace App\Http\Controllers\Seo;

use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function seo()
   {
        return view($this->_site->theme .'.inc.home');
   } 
}
