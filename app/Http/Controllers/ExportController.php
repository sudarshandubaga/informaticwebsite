<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin;
use App\Models\PostType;
use App\Models\PostField;
use App\Models\PostMeta;
use App\Models\Post;
use App\Models\MenuLocation;
use App\Models\Menu;

class ExportController extends Controller
{
    public function index()
    {
        $array = [];
        $array['site'] = $this->_site->toArray();
        $array['admins'] = Admin::where('site_id', $this->_site->id)->get()->toArray();

        $array['post_types'] = PostType::where('site_id', $this->_site->id)->select('name', 'slug')->get()->toArray();

        $array['post_fields'] = PostField::where('site_id', $this->_site->id)->get()->toArray();

        $array['menus'] = Menu::with(['menu_items'])->where('site_id', $this->_site->id)->get()->toArray();


        // $array['post_images'] = Post::where('site_id', $this->_site->id)->get()->toArray();


        $array['posts'] = Post::with(['categories','post_meta'])->where('site_id', $this->_site->id)->get()->toArray();

        $array['menu_locations'] = MenuLocation::where('site_id', $this->_site->id)->get()->toArray();

        $jsonResponse = [
            'token' => time(),
            'data'  => response()->json($array)
        ];

        $dir = public_path('uploads/' . $this->_site->domain . '/' . date('Y-m-d') . '/');

        if(!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        $fileName = 'export.json';
        \File::put($dir . $fileName, $jsonResponse);

        \File::copyDirectory(
            public_path($this->_site->domain),
            $dir
        );

        $zip = new \ZipArchive();
        $fileName = $this->_site->domain . '_' . date('Y-m-d') . '.zip';
        $dir2 = public_path('uploads/' . $this->_site->domain . '/');
        if ($zip->open($dir2.$fileName, \ZipArchive::CREATE)== TRUE)
        {
            $this->addContent($zip, $dir);
            $zip->close();
        }
        return response()->download($dir2.$fileName); // 'Done!! '. '<a href="'.url('uploads/' . $fileName).'" target="_blank">Download</a>'; // download(public_path('uploads/' . $fileName));

    }

    /**
     * This takes symlinks into account.
     *
     * @param ZipArchive $zip
     * @param string     $path
     */
    private function addContent(\ZipArchive $zip, string $path)
    {
        /** @var SplFileInfo[] $files */
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $path,
                \FilesystemIterator::FOLLOW_SYMLINKS
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );
    
        while ($iterator->valid()) {
            if (!$iterator->isDot()) {
                $filePath = $iterator->getPathName();
                $relativePath = substr($filePath, strlen($path));
    
                if (!$iterator->isDir()) {
                    $zip->addFile($filePath, $relativePath);
                } else {
                    if ($relativePath !== false) {
                        $zip->addEmptyDir($relativePath);
                    }
                }
            }
            $iterator->next();
        }
    }
}
