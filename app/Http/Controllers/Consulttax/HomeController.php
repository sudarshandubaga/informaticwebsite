<?php

namespace App\Http\Controllers\Consulttax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function cure(){

        return view( $this->_site->theme .'.inc.home');
    }
}
