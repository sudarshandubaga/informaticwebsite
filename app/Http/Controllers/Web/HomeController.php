<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    public function index()
    {
        $about          =  Post::where('slug', 'about-us')->first();
        $slider         =  Post::where('slug','home')->first();

        
        $data = compact('about','slider');

        return view($this->_site->theme.'.inc.Home', $data);
    }
}