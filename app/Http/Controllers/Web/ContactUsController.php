<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Site;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyContactMail;
use App\Models\Enquiry;

class ContactUsController extends Controller
{
    public function relay()
    {
        // print 'nameee';
        return view('web.inc.contact');
    }

    public function index()
    {
        return view('web.inc.contact');
    }

    public function store(Request $request)
    {
        $site = $this->_site;

        $sender = $request->name;
        $from = $request->email;

        $to = $site->email;
        $receiver = $site->title;

        $form_params = $request->except('_token');
        $form_params['subject'] .= ' ' . $site->title;

        $details = compact('site', 'from', 'to', 'sender', 'receiver');
        $details = array_merge($details, $form_params);

        Mail::send(new MyContactMail($details));

        return redirect()->back()->with('success', 'Success! Mail has sent.');
    }

    public function sendEnquiry(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            // 'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
        ]);

        $details = $request->except('_token');
        $details['subject'] = 'Contact enquiry from ' . $this->_site->title;
        $details['site'] = $this->_site;
        $details['msg'] = $request->message;

        // $details = array_merge($details, );

        // dd($details);

        \Mail::to($this->_site->email)
            ->cc('meghasvyas85@gmail.com')
            ->send(new \App\Mail\MyContactMail($details));

        return redirect()->back()->with('success', 'Your details has been sent. We will contact you shortly.');
    }
}
