<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostType;
use App\Models\PostMeta;
use App\Models\Category;
use App\Models\Template;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    protected function removeImage($post, $post_type)
    {
        if (!empty($post->image)) {
            $file = public_path($this->_site->domain . "/images/'.$post_type.'/" . $post->image);
            if (file_exists($file)) {
                unlink($file);
            }

            $medium = public_path($this->_site->domain . "/images/'.$post_type.'/medium/" . $post->image);
            if (file_exists($medium)) {
                unlink($medium);
            }

            $thumb = public_path($this->_site->domain . "/images/'.$post_type.'/thumbnail/" . $post->image);
            if (file_exists($thumb)) {
                unlink($thumb);
            }
        }
    }
    protected function uploadImage($request, $post_type, $post, $medium = false)
    {
        if ($request->hasFile('image')) {

            $this->removeImage($post, $post_type);

            $dir = public_path($this->_site->domain . '/images/' . $post_type . '/');
            if (!is_dir($dir) && !file_exists($dir)) {
                mkdir($dir, 0755, true);
            }

            $dir2 = $dir . 'thumbnail/';
            if (!is_dir($dir2) && !file_exists($dir2)) {
                mkdir($dir2, 0755, true);
            }

            $dir3 = $dir . 'medium/';
            if (!is_dir($dir3) && !file_exists($dir3) && $medium) {
                mkdir($dir3, 0755, true);
            }

            $image       = $request->file('image');
            $filename    = $post->id . '.' . $image->getClientOriginalExtension();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(1920, 845, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save($dir . $filename);

            // Thumbnail
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(128, 128, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 . $filename);

            if ($medium) {
                // Thumbnail
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(256, 256, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image_resize->save($dir3 . $filename);
            }


            $post->image = $filename;

            $post->save();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostType $post_type, Request $request)

    {
        $posts = Post::with('parent')->where('post_type', $post_type->slug)->where('site_id', $this->_site->id);
        // dd($posts->get()->toArray());
        if (!empty($request->search_by_keyword)) {

            $posts->where(function ($q) use ($request) {
                $q->where('title', 'LIKE', '%' . $request->search_by_keyword . '%')
                    ->orwhere('excerpt', 'LIKE', '%' . $request->search_by_keyword . '%');
            });
        }
        $posts = $posts->latest()->paginate(10);
        return view('backend.pages.post.index', compact('post_type', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, PostType $post_type)
    {
        $post_fields = $post_type->fields();

        $templates = Template::orderBy('name')->where('theme', $this->_site->theme)->pluck('name', 'slug');


        // dd($post_fields->extra_fields);
        $parent_posts = Post::where('post_type', $post_type->slug)->whereNull('post_id')->pluck('title', 'id');

        $templates = Template::where('theme', $this->_site->theme)->pluck('name', 'slug');

        // dd($templates);

        $request->flush();
        $parent_categories = Category::with('categories')->where('type', $post_type->slug)->where('site_id', $this->_site->id)->whereNull('category_id')->select('title', 'id', 'category_id')->get();
        return view('backend.pages.post.create', compact('post_type', 'post_fields', 'parent_categories', 'templates', 'parent_posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_type)
    {
        $site = $this->_site;
        $request->validate([
            'title' => 'required',
            'slug'  => [
                'required',
                Rule::unique('posts', 'slug')->using(function ($q) use ($post_type, $site) {
                    $q->where('post_type', $post_type)->where('site_id', $site->id);
                })
            ]
        ]);

        $post = new Post();
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->excerpt = $request->excerpt;
        $post->description = $request->description;
        $post->post_type = $post_type;
        $post->site_id = $this->_site->id;
        $post->meta_keywords = $request->meta_keywords;
        $post->meta_title = $request->meta_title;
        $post->meta_description = $request->meta_description;
        $post->template = $request->template;
        $post->post_id = $request->post_id;

        // if ($request->hasfile('image')){
        //     $post->image = $request->file('image')->store($this->_site->domain . '/imgs/post/', 'public');
        // }
        $post->save();

        $this->uploadImage($request, $post_type, $post);


        if (!empty($request->extra_fields)) :
            $post->post_meta()->delete();
            foreach ($request->extra_fields as $meta_name => $meta_value) {
                $post_meta = new PostMeta();
                $post_meta->meta_name = $meta_name;
                $post_meta->meta_value = $meta_value;
                $post_meta->post_id = $post->id;
                $post_meta->save();
            }
        endif;

        if (!empty($request->category)) {
            $post->categories()->sync($request->category);
        }

        return redirect(route('admin.post.index', [$post_type]))->with('success', 'Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post  $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $post_type_slug, $post_slug)
    {
        $post  = Post::where('slug', $post_slug)->where('site_id', $this->_site->id)->firstOrFail();
        $post_type = PostType::where('slug', $post_type_slug)->firstOrFail();

        $post_fields = $post_type->fields();
        $post_arr = $post->toArray();
        $post_arr['category'] = $post->categories()->pluck('categories.id')->toArray();

        $request->replace($post_arr);
        $request->flash();

        $templates = Template::orderBy('name')->where('theme', $this->_site->theme)->pluck('name', 'slug');
        $parent_posts = Post::where('post_type', $post_type->slug)->whereNull('post_id')->where('id', '!=', $post->id)->pluck('title', 'id');


        $parent_categories = Category::with('categories')->where('type', $post_type->slug)->where('site_id', $this->_site->id)->whereNull('category_id')->select('title', 'id', 'category_id')->get();

        return view('backend.pages.post.edit', compact('post', 'post_type', 'post_fields', 'parent_categories', 'templates', 'parent_posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post_type, $post)
    {
        $post = Post::findOrFail($post);
        $post->post_type = $post_type;
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->excerpt = $request->excerpt;
        $post->description = $request->description;
        $post->meta_keywords = $request->meta_keywords;
        $post->meta_title = $request->meta_title;
        $post->meta_description = $request->meta_description;
        $post->template = $request->template;
        $post->post_id = $request->post_id;

        // if ($request->hasfile('image')){
        //     $post->image = $request->file('image')->store($this->_site->domain . '/imgs/post/', 'public');
        // }

        $post->save();

        $this->uploadImage($request, $post_type, $post);


        if (!empty($request->extra_fields)) :
            $post->post_meta()->delete();
            foreach ($request->extra_fields as $meta_name => $meta_value) {
                $post_meta = new PostMeta();
                $post_meta->meta_name = $meta_name;
                $post_meta->meta_value = $meta_value;
                $post_meta->post_id = $post->id;
                $post_meta->save();
            }
        endif;

        $post->categories()->sync($request->category);

        return redirect(route('admin.post.index', [$post_type]))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_type, $slug)
    {
        $post = Post::where('slug', $slug)->where('post_type', $post_type)->firstOrFail();

        $this->removeImage($post, $post_type);

        $post->delete();
        return redirect(route('admin.post.index', $post_type))->with('success', 'Your data has been deleted.');
    }
}
