<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\PostType;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Media;
use App\Models\Enquiry;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $_site = null,
        $_post_types = [];

    public function __construct(Request $request)
    {
        $domain = $request->getHost();
        // dd($domain);
        $this->_site = $site = Site::where('domain', $domain)->firstOrFail();
        // dd($site);

        if ($site->staging_status === 'comming') {
            echo view('comingsoon', compact('site'));
            die;
        } else {
            $this->_post_types = $post_types = PostType::where('site_id', $site->id)->get();

            $enq_types = Enquiry::orderBy('type')->groupBy('type')->pluck('type');

            $menus = Menu::with(['menu_items' => function ($q) {
                $q->whereNull('menu_item_id');
            }])->where('site_id', $this->_site->id)->get();

            $menuItems = [];
            foreach ($menus as $key => $m) {
                $menuItems[$m->location] = $m->menu_items;
            }

            $media = Media::where('site_id', $this->_site->id)->get();

            // dd($menuItems);

            $data = compact('site', 'post_types', 'menuItems', 'media', 'enq_types');



            \View::share($data);
        }
    }
}
