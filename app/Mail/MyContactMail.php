<?php

namespace App\Mail;

use App\Models\Enquiary;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MyContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;

        $enquiry = new Enquiary();
        $enquiry->to = $details['to'];
        $enquiry->receiver = $details['receiver'];
        $enquiry->from = $details['from'];
        $enquiry->sender = $details['sender'];
        $enquiry->subject = $details['subject'];
        $enquiry->email_template = $details['email_template'];
        $enquiry->details = $details;
        $enquiry->type = $details['type'];
        $enquiry->save();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->details['subject'])
            ->to($this->details['to'], $this->details['receiver'])
            ->cc('vyasmegha08@gmail.com', 'Megha Vyas')
            ->from($this->details['from'], $this->details['sender'])
            ->replyTo($this->details['from'], $this->details['sender'])
            ->view($this->details['email_template'], $this->details);
    }
}
