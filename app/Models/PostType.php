<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostType extends Model
{
    use HasFactory, SoftDeletes;

    public function fields()
    {
        return PostField::where('post_type', $this->slug)->first();
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'id', 'post_id');
    }

    public function post_fields()
    {
        return $this->hasOne(PostField::class, 'post_type', 'slug');
    }
}
