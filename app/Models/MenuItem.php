<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use HasFactory;
    protected $with = ['menu_item', 'post', 'category'];

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function menu_item()
    {
        return $this->hasMany(MenuItem::class)->orderBy('sortby');
    }
}
