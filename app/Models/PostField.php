<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostField extends Model
{
    use HasFactory;

    protected $casts = [
        'extra_fields' => 'array'
    ];

    protected $guarded = [];

    public function post_type()
    {
        return $this->belongsTo(PostType::class);
    }
}
