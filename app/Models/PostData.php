<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostData extends Model
{
    use HasFactory;

    public static function getPostRow($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return $post;
    }

    public static function getPostData($post_type = 'post', $args = [])
    {
        $domain = request()->getHost();
        $site = Site::where('domain', $domain)->firstOrFail();

        $posts = Post::where('post_type', $post_type)->where('site_id', $site->id);

        if (!empty($args['conditions'])) {
            $posts->whereRaw($args['conditions']);
        }

        if (!empty($args['limit'])) {
            $posts = $posts->paginate($args['limit']);
        } elseif (!empty($args['type']) && $args['type'] === 'pluck') {
            $value = !empty($args['value_field']) ? $args['value_field'] : 'id';
            $posts = $posts->pluck('title', $args['value_field']);
        } else {
            $posts = $posts->get();
        }

        if (!empty($args['response_type']) && $args['response_type'] === 'array') {
            $posts = $posts->toArray();
        }

        return $posts;
    }
}
