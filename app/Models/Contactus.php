<?php

namespace App\Models;

use App\Mail\MyContactMail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Contactus extends Model
{
    use HasFactory;
    protected $table = 'enquiries';

    protected $casts = [
        'details' => 'array'
    ];
}
