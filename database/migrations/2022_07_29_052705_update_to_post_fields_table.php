<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_fields', function (Blueprint $table) {
            // $table->dropForeign(['post_id']);
            // $table->dropColumn(['post_id']);

            $table->string('post_id')->after('template')->nullable()->comment('Parent Post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_fields', function (Blueprint $table) {
            //
        });
    }
};
