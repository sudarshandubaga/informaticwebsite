<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SuperAdmin;


class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superadmin = new SuperAdmin();
        $superadmin->name = 'Mr. Super Admin';
        $superadmin->email = 'info@rudrakshatech.com';
        $superadmin->password = \Hash::make('admin@123');
        $superadmin->save();
    }
}
