<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin();
        $admin->name = 'Mr. Admin';
        $admin->email = 'info@school.com';
        $admin->password = \Hash::make('School@123');
        $admin->site_id = 1;
        $admin->save();
    }
}
