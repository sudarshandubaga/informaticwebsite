<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name'  => 'Sudarshan',
            'email' => 'demo@demo.com',
            'password' => \Hash::make('admin1234'),
            'site_id'   => 1
        ]);
    }
}
