<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\MenuLocation;

class MenuLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menuLocation = new MenuLocation();
        $menuLocation->name = 'Top Menu';
        $menuLocation->keyword = 'top-menu';
        $menuLocation->site_id = 1;
        $menuLocation->save();

        $menuLocation = new MenuLocation();
        $menuLocation->name = 'Bottom Menu';
        $menuLocation->keyword = 'bottom-menu';
        $menuLocation->site_id = 1;
        $menuLocation->save();      
    }
}
