<!DOCTYPE HTML>
<html lang="en">

<head>
    <!--=============== basic  ===============-->
    <meta charset="UTF-8">
    <title> @yield('site_title', $site->title)</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!--=============== css  ===============-->
    <link type="text/css" rel="stylesheet" href="{{ url('photography/css/reset.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ url('photography/css/plugins.css') }}">
    <link type="text/css" rel="stylesheet" href="{{url('photography/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('photography/css/yourstyle.css')}}">
    <link rel="stylesheet" href="{{ url('photography/venobox/venobox.min.css')}}" type="text/css" media="screen" />
    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="{{url('storage/'.$site->favicon)}}">
</head>

<body>
    <!--Loader  -->
    <div class="loader"><i class="fa fa-refresh fa-spin"></i></div>
    <!--LOader end  -->
    <!--================= main start ================-->
    <div id="main">
        <!--=============== header ===============-->
        <header>
            <!-- Header inner  -->
            <div class="header-inner">
                <!-- Logo  -->
                <div class="logo-holder">
                    <a href="{{route('home')}}"><img src="{{url('storage/'.$site->logo)}}" alt=""></a>
                </div>
                <!--Logo end  -->
                <!--Navigation  -->
                <div class="nav-button-holder">
                    <div class="nav-button vis-m"><span></span><span></span><span></span></div>
                </div>
                {{-- <div class="show-share isShare">Share</div> --}}
                <div class="nav-holder">
                    <nav>
                        <ul>
                            @foreach($menuItems['top-menu'] as $m)
                            <li><a href="{{route('post.show', [$m->post->slug])}}"> {{ $m->label }} </a>
                                @endforeach

                        </ul>
                    </nav>
                </div>
                <!--navigation end -->
            </div>
            <!--Header inner end  -->
        </header>


        @yield('main_section')
        <!--header end -->
        <footer>
            <div class="policy-box">
                <span>&#169; {{ $site->title }} {{ date('Y') }} . All rights reserved. </span>
                <ul>
                    <li><a href="mailto:{{ $site->email}}">{{ $site->email}}</a></li>
                    <li><a href="tel:{{ $site->phone}}">{{ $site->phone}}</a></li>
                </ul>
            </div>
            <!-- footer social -->
            <div class="footer-social">
                <ul>
                    @foreach($site->social_links as $social_name => $link)
                    <li><a href="{{ $link }}" target="_blank"><i class="fa fa-{{ $social_name }}"></i></a></li>
                    @endforeach
                    <!-- <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-tumblr"></i></a></li> -->
                </ul>
            </div>
            <!-- footer social end -->
            <div class="to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- footer end -->
    </div>
    <!-- Main end -->
    <!--=============== scripts  ===============-->
    <script type="text/javascript" src="{{url('photography/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('photography/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{url('photography/js/scripts.js')}}"></script>
    <script type="text/javascript" src="{{url('photography/venobox/venobox.min.js')}}"></script>
    <script>
        new VenoBox();
    </script>
</body>

</html>