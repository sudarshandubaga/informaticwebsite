@extends('photography.layouts.app')
@section('main_section')

<div id="wrapper">
    <!--=============== content-holder ===============-->
    <div class="content-holder elem scale-bg2 transition3">
        <div class="fixed-title"><span>Blog</span></div>
        <!--=============== content  ===============-->
        <div class="content">
            @include('photography.templates.page_title_section')


            <div class="sections-bg"></div>
            <section id="sec1">
                <div class="container column-container">
                    <div class="row">
                        <div class="col-md-7">
                            <article>
                                <ul class="blog-title">
                                    <li><a href="#" class="tag">{{ date('d M Y',strtotime($post->updated_at))}}
                                        </a></li>

                                </ul>
                                <div class="blog-media">
                                    <div class="custom-slider-holder">
                                        <div class="custom-slider
                                                        owl-carousel">
                                            <div class="item">
                                                <img src="{{url('images/bg/26.jpg')}}" class="respimg" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="images/bg/27.jpg')}}" class="respimg" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="images/bg/28.jpg" class="respimg" alt="">
                                            </div>
                                        </div>
                                        <div class="customNavigation">
                                            <a class="prev-slide"><i class="fa
                                                                fa-angle-left"></i></a>
                                            <a class="next-slide"><i class="fa
                                                                fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-text">
                                    {!! $post->description !!}

                                </div>
                            </article>


                        </div>
                        @include('photography.templates.post_sidebar')

                    </div>
                </div>
            </section>
        </div>
        <!-- Content end  -->
        <!-- Share container  -->
        <div class="share-container isShare" data-share="['facebook','pinterest','googleplus','twitter','linkedin']">
        </div>
    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>



@endsection