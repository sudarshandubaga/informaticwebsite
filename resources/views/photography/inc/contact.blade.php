@extends('photography.layouts.app')

@section('main_section')


<div id="wrapper">
    <!--=============== Conten holder  ===============-->
    <div class="content-holder elem scale-bg2 transition3">
        <!--  Page title    -->
        <div class="fixed-title"><span>{{ $post->title}}</span></div>
        <!--  Page title end   -->
        <!--  Page navigation   -->
        <div class="scroll-page-nav">
            <ul>
                <li><a href="#sec1"></a></li>
                <li><a href="#sec2"></a></li>
                <li><a href="#sec3"></a></li>
            </ul>
        </div>
        <!--  Page navigation  end -->
        <div class="content full-height">
            <!--  Page title section   -->


            @include('photography.templates.page_title_section')

            <!--  Page title section end  -->
            <!--  Section contact info   -->
            <section class="section-columns" id="sec1">
                <div class="section-columns-img">

                    {{-- <div class="bg"
                        style="background-image:url({{ !empty($post->image) ? url($site->domain .  '/images/'.$post->post_type.'/'.$post->image) : ''}})">
                    </div> --}}

                    <iframe src="{{ $site->google_map }}" style="border:0; width:100%; height:550px" allowfullscreen=""
                        loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="section-columns-text">
                    <div class="custom-inner">
                        <div class="container">
                            {!! $post->description !!}
                            <ul class="contact-list no-dec">
                                <li><span>Adress : </span> <a>{{ $site->address}}</a></li>
                                <li><span>Phone : </span><a href="tel:{{ $site->phone}}">{{ $site->phone}}</a></li>
                                <li><span>E-mail : </span><a href="mailto:{{ $site->email}}">{{ $site->email}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!--  Section contact info end   -->
            <!--  Section social   -->
            <section class="no-padding">
                <div class="content">
                    <div class="inline-facts-holder">
                        @foreach($site->social_links as $social_name => $link)
                        <div class="inline-facts" style="width: {{ 100 / count($site->social_links) }}%">
                            <h6><a href="{{ $link }}" target="_blank">{{ $social_name }}</a></h6>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
            <!-- social end  -->
            <!--  Section map   -->
            {{-- <section class="no-padding" id="sec2">
                <div class="map-box">
                    <div class="map-holder" data-top-bottom="transform: translateY(300px);"
                        data-bottom-top="transform: translateY(-300px);">
                        <div id="">
                            <iframe src="{{ $site->google_map }}" style="border:0; width:100%; height:550px"
                                allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>
                </div>
            </section> --}}
            <!--  Section map end  -->
            <!--  Section contact form  -->
            <section class="flat-form" id="sec3">
                <div class="container">
                    <h2>Write us</h2>
                    <div class="separator-image"><img src="photography/images/separator2.png" alt=""></div>
                    <div id="contact-form">
                        <div id="message"></div>

                        <!-- <form method="post" action="http://outdoor.kwst.net/site/php/contact.php" name="contactform"
                            id="contactform"> -->

                        {{ Form::open(['url'=> route('contact.send')]) }}
                        <input name="name" type="text" id="name" class="inputForm2" onClick="this.select()"
                            value="Name">
                        <input name="email" type="text" id="email" onClick="this.select()" value="E-mail">
                        <textarea name="comments" id="comments" onClick="this.select()" placeholder="Message"></textarea>
                        <input type="submit" class="send_message transition" id="submit" value="Send Message" />
                        <!-- </form> -->
                        {{ Form::close() }}
                    </div>
                </div>
            </section>
            <!--  Section contact form end  -->
        </div>
        <!-- Content end  -->
        <!-- Share container  -->
        <div class="share-container  isShare" data-share="['facebook','pinterest','googleplus','twitter','linkedin']">
        </div>
    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>

@endsection