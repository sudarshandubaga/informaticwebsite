@php
$records = \App\Models\PostData::getPostData('portfolio');

@endphp

@extends('photography.layouts.app')

@section('main_section')

<div id="wrapper">
    <!--=============== Conten holder  ===============-->
    <div class="content-holder elem scale-bg2 transition3">
        <!--=============== Content  ===============-->
        <div class="content full-height">
            <div class="fixed-title"><span>Portfolio</span></div>
            <!-- Portfolio counter  -->
            <div class="count-folio">
                <div class="num-album"></div>
                <div class="all-album"></div>
            </div>
            <!-- Portfolio counter end -->
            <!-- <div class="filter-holder column-filter">
                <div class="filter-button">Filter <i class="fa fa-long-arrow-down"></i></div>
                <div class="gallery-filters hid-filter">
                    <a href="#" class="gallery-filter transition2 gallery-filter_active" data-filter="*">All Albums</a>
                    <a href="#" class="gallery-filter transition2" data-filter=".people">People</a>
                    <a href="#" class="gallery-filter transition2" data-filter=".nature">Nature</a>
                    <a href="#" class="gallery-filter transition2" data-filter=".comercial">Comercial</a>
                    <a href="#" class="gallery-filter transition2" data-filter=".travel">Travel</a>
                </div>url( $site->domain .  '/images/slider/'.$s->image)
            </div> -->
            <!--=============== portfolio holder ===============-->
            <div class="resize-carousel-holder">
                <div class="p_horizontal_wrap">
                    <div id="portfolio_horizontal_container">

                        @foreach($records as $p )

                        <!-- portfolio item -->
                        <div class="portfolio_item people comercial">

                            <a href="{{ url('storage/' . $p->getMetaValue('attachment')) }}" target="_blank"
                                class="btn btn-dark">
                                <img src="{{ url($site->domain .  '/images/portfolio/'.$p->image)}}" alt=""></a>
                            <div class="port-desc-holder">
                                <div class="port-desc">
                                    <div class="overlay"></div>
                                    <div class="grid-item">
                                        <h3><a href="{{ url('storage/' . $p->getMetaValue('attachment')) }}"
                                                target="_blank">{{ $p->title}}</a></h3>

                                    </div>
                                </div>
                            </div>
                            <div class="port-subtitle-holder">
                                <div class="port-subtitle">
                                    <h3><a href="{{ url('storage/' . $p->getMetaValue('attachment')) }}"
                                            target="_blank">{{ $p->title}}</a></h3>
                                    <p>{{$p->excerpt}}</p>
                                </div>
                            </div>
                        </div>

                        <!-- portfolio item end -->
                        @endforeach
                    </div>
                    <!--portfolio_horizontal_container  end-->
                </div>
                <!--p_horizontal_wrap  end-->
            </div>
        </div>
        <!-- Content end  -->
        <!-- Share container  -->
        <div class="share-container  isShare" data-share="['facebook','pinterest','googleplus','twitter','linkedin']">
        </div>
    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>


@endsection