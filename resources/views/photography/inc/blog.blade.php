@extends('photography.layouts.app')
@section('main_section')

@php
$blogs = \App\Models\PostData::getPostData('blog', [
'limit' => 10
]);
@endphp

<div id="wrapper">
    <!--=============== content-holder ===============-->
    <div class="content-holder elem scale-bg2 transition3">
        <div class="fixed-title"><span>{{ $post->title}}</span></div>
        <!--=============== content  ===============-->
        <div class="content">
            @include('photography.templates.page_title_section')
            <div class="sections-bg"></div>
            <!--section    -->
            <section id="sec1">
                <div class="container column-container">
                    <!--================= articles   ================-->
                    <div class="row">
                        <div class="col-md-7">
                            @foreach($blogs as $b )

                            <article>
                                <ul class="blog-title">
                                    <li><a href="" class="tag">{{ date('d M Y',strtotime($b->updated_at))}}</a></li>

                                </ul>
                                @if(!empty($b->image))
                                <div class="blog-media">
                                    <div class="box-item">
                                        <a href="{{route('post.show', $b->slug)}}">
                                            <span class="overlay"></span>
                                            <img src="{{ url($site->domain .  '/images/'.$b->post_type.'/' . $b->image)}}" alt="" class="respimg">
                                        </a>
                                    </div>
                                </div>
                                @endif
                                <div class="blog-text">
                                    <h3>{{ $b->title}}</h3>
                                    <p>
                                        {{ $b->excerpt}}
                                    </p>
                                    <a href="{{route('post.show', $b->slug)}}" class="ajax btn"><span> more </span>
                                        <i class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </article>
                            @endforeach


                            <div class="clearfix"></div>
                            <!-- pagination   -->
                            <!-- <div class="pagination-blog">
                                <a href="blog-single.html" class="prevposts-link transition"><i
                                        class="fa fa-chevron-left"></i></a>
                                <a href="blog-single.html" class="blog-page transition">1</a>
                                <a href="blog-single.html" class="blog-page current-page transition">2</a>
                                <a href="blog-single.html" class="blog-page transition">3</a>
                                <a href="blog-single.html" class="blog-page transition">4</a>
                                <a href="blog-single.html" class="nextposts-link transition"><i
                                        class="fa fa-chevron-right"></i></a>
                            </div> -->
                            {{$blogs->links()}}
                        </div>
                        @include('photography.templates.post_sidebar')

                    </div>
                </div>
            </section>
        </div>
        <!-- Content end  -->
        <!-- Share container  -->
        <div class="share-container  isShare" data-share="['facebook','pinterest','googleplus','twitter','linkedin']">
        </div>
    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>


@endsection