@extends('photography.layouts.app')

@section('main_section')

<div id="wrapper">
    <!--=============== Content holder  ===============-->
    <div class="content-holder elem scale-bg2 transition3">
        <!--=============== Content  ===============-->
        <div class="content full-height">
            <!-- Fixed title-->
            <div class="fixed-title"><span>{{ $post->title}} </span></div>
            <!-- Page navigation-->

            <!-- Page navigation end-->
            @include('photography.templates.page_title_section')



            <!-- About   -->
            <section class="section-columns" id="sec1">
                @if(!empty($post->image))

                <div class="section-columns-img">
                    <div class="bg"
                        style="background-image:url({{ url($site->domain .  '/images/'.$post->post_type.'/'.$post->image)}} )">
                    </div>
                </div>
                @endif
                <div class="section-columns-text">
                    <div class="custom-inner">
                        <div class="container">
                            <h2>{{ $post->title}}</h2>
                            <div class="separator"></div>
                            <div class="clearfix"></div>
                            {!! $post->description !!}

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About end  -->

        </div>
        <!-- Content end  -->
        <!-- Share container  -->

    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>


@endsection