@extends($site->theme.'.layouts.app')

@section('main_section')

@php
$slider = \App\Models\PostData::getPostData('slider');
@endphp

<!--=============== wrapper ===============-->
<div id="wrapper">
    <!--=============== Content holder  ===============-->
    <div class="content-holder elem scale-bg2 transition3 slid-hol">
        <!-- Fixed title  -->
        <div class="fixed-title"><span>Home</span></div>
        <!-- Fixed title end -->
        <!--=============== Content ===============-->
        <div class="content full-height">
            <!-- full-height-wrap end  -->

            <div class="full-height-wrap">
                <div class="swiper-container" id="horizontal-slider" data-mwc="1" data-mwa="0">
                    <div class="swiper-wrapper">
                        <!--=============== 1 ===============-->
                        @foreach($slider as $s)
                        <div class="swiper-slide">
                            <div class="bg"
                                style="background-image:url({{ !empty($s->image) ? url( $site->domain .  '/images/slider/'.$s->image) : '' }})">
                            </div>
                            <div class="overlay"></div>
                            <!-- <div class="zoomimage"><img
                                    src="{{ url( $site->domain .  '/images/slider/'.$s->image)  }}" class="intense"
                                    alt=""><i class="fa fa-expand"></i></div> -->
                            <div class="slide-title-holder">
                                <div class="slide-title">
                                    <span class="subtitle">{{ $s -> excerpt}} </span>
                                    <div class="separator-image"><img src="photography/images/separator.png" alt="">
                                    </div>
                                    <h3 class="transition">{{ $s-> title}}</h3>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- 1 end -->

                    </div>
                </div>
                <!-- slider  pagination -->
                <div class="pagination"></div>
                <!-- pagination  end -->
                <!-- slider navigation  -->
                <div class="swiper-nav-holder hor hs">
                    <a class="swiper-nav arrow-left transition " href="#"><i class="fa fa-angle-left"></i></a>
                    <a class="swiper-nav  arrow-right transition" href="#"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- slider navigation  end -->
            </div>
            <!-- full-height-wrap end  -->
        </div>
        <!-- Content end  -->
        <!-- Share container  -->
        <div class="share-container  isShare" data-share="['facebook','pinterest','googleplus','twitter','linkedin']">
        </div>
    </div>
    <!-- content holder end -->
</div>
<!-- wrapper end -->
<div class="left-decor"></div>
<div class="right-decor"></div>
<!--=============== Footer ===============-->
@endsection