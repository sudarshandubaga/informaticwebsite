<!--section  page title   -->
<section class="parallax-section">
    <div class="overlay"></div>
    <div class="bg"
        style="background-image:url({{ !empty($post->image) ? url($site->domain .  '/images/'.$post->post_type.'/'.$post->image) : 'photography/images/bg/22.jpg'}})"
        data-top-bottom="transform: translateY(200px); background-repeat: no-repeat"
        data-bottom-top="transform: translateY(-200px);">
    </div>
    <div class="container">
        <h2> {{ $post->title}}</h2>
        <div class="separator"></div>
        <h3 class="subtitle">{{ $post->excerpt}}</h3>
    </div>
    <a class="custom-scroll-link sect-scroll" href="blog-single.html"><i class="fa fa-angle-double-down"></i></a>
</section>
<!--section  page title end  -->