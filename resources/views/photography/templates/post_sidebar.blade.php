@php
$blogs = \App\Models\PostData::getPostData('blog');
@endphp
<!--================= sidebar  ================-->


<div class="col-md-4">
    <div class="sidebar">
        <!-- widget -->
        <!-- <div class="widget"> 
                                    <div class="searh-holder">
                                        <form action="#" class="searh-inner">
                                            <input name="se" id="se" type="text" class="search" placeholder="Search.."
                                                value="Search..." />
                                            <button class="search-submit" id="submit_btn"><i class="fa
                                                                fa-search
                                                                transition"></i>
                                            </button>
                                        </form>
                                    </div>
                                 </div> -->
        <!-- widget -->

        <!-- widget -->
        <div class="widget">
            <h3>Latest posts</h3>
            <ul class="widget-posts">
                @foreach($blogs as $b )

                <li class="clearfix">
                    <a href="{{route('post.show', $b->slug)}}" class="widget-posts-img"><img
                            src="{{ url($site->domain.'/images/blog/'.$b->image)}}" class="respimg" alt=""></a>
                    <div class="widget-posts-descr">
                        <a href="{{route('post.show', $b->slug)}}" title="">{{ $b->title}}

                        </a>
                        <span class="widget-posts-date">
                            {{ date('d M Y',strtotime($b->updated_at))}}
                        </span>
                    </div>
                </li>
                @endforeach


            </ul>
        </div>
        <!-- widget -->

        <!-- end sidebar -->