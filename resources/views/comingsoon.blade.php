<html>
    <head>
        <title>The Click Factory - Comming Soon</title>
        <style>
            body {
                font-family: Arial;
                background-color: #161616;
                background-image: url('https://wallpapercave.com/wp/wp1933240.jpg');
                background-size: cover;
                height: 100vh;
                margin: 0;
            }
            body div {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                z-index: 1;
                background: rgba(50, 50, 50, 0.7);
                display: flex;
                flex-direction: column;
                justify-content: center;
                text-align: center;
                color: #fff;
            }
            h1 {
                font-size: 48px;
            }
        </style>
    </head>
    <body>
        <div>
            <h1>{{ $site->title }} | Comming Soon</h1>
            <p>We're comming soon at website.</p>
        </div>
    </body>
</html>
