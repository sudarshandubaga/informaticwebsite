<div class="mb-3">
    {{ Form::label('name') }}
    {{ Form::text('name', null, ['class' => 'form-control title', 'placeholder' => 'Enter Name', 'data-target' => '#slug', 'required'=>'required']) }}

    <div class="invalid-feedback">
        Please Enter Name.
      </div>
</div>
<div class="mb-3">
    {{Form::label('site_id', null, ['class' =>'form-label'])}}
    {{Form::select('site_id',$site,null , ['class' => 'form-control', 'placeholder'=> 'Select Site', 'autocomplete' => 'off']) }}
</div>

<div class="mb-3">
    {{Form::label('keyword', null, ['class' =>'form-label'])}}
    {{Form::select('keyword',$menus,null , ['class' => 'form-control', 'placeholder'=> 'Select keyword', 'autocomplete' => 'off']) }}
</div>
