
<div class="mb-3">
    {{Form::label('site_id','Site', null, ['class' =>'form-label'])}}
    {{Form::select('site_id',$themes,null , ['class' => 'form-select', 'placeholder'=> 'Select Site', 'autocomplete' => 'off']) }}
</div>

<div class="mb-3">
    {{Form::label('admin_id','Admin', null, ['class' =>'form-label'])}}
    {{Form::select('admin_id',$admin,null , ['class' => 'form-select', 'placeholder'=> 'Select Site', 'autocomplete' => 'off']) }}
</div>

<div class="mb-3 ">
    {{Form::label('menu','Menu', null, ['class' =>'form-label'])}} 
      <label class="switch">
        <input type="checkbox" name="names[]" value="menu">
        <span class="slider round"></span>
      </label>
     <br>
    {{Form::label('media','Media', null, ['class' =>'form-label'])}}
    <label class="switch">
        <input type="checkbox"  name="names[]" value="media">
        <span class="slider round"></span>
      </label>
      @foreach($all_post_types as $pid => $pname)
      <br>
      <label for="names_{{$pid}}" >Add {{$pname}}</label>
     <label class="switch">
         <input type="checkbox"  name="names[]" value="Add {{$pname}}" id="names_{{$pid}}">
         <span class="slider round"></span>
       </label>

       <br>
       <label for="names_view_{{$pid}}" >View {{$pname}}</label>
      <label class="switch">
          <input type="checkbox"  name="names[]" value="View {{$pname}}" id="names_view_{{$pid}}">
          <span class="slider round"></span>
        </label>
       @endforeach
</div>
