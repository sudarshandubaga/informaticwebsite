@extends('superadmin.layouts.inner')
@section('site_title','Add Admin')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add Admin</h1>
        <ol class="breadcrumb mb-4">  
            <li class="breadcrumb-item">
                <a href="{{ route('super.admin.dashboard')}}">Dashboard</a>
            </li>        
            <li class="breadcrumb-item">
                <a href="{{ route('super.admin.admin.index')}}">Admin</a>
            </li>
            <li class="breadcrumb-item active">Add Admin</li>
        </ol>

            {{ Form::open([
                'url' => route('super.admin.admin.store'),
                'files' => true, 
                'method' => 'post',
                'class' =>'needs-validation',
                'novalidate' =>'true',
            ]) }}
    <!-- DataTales Example -->
    <div class="row">
        <div class="col-sm-8 col-lg-9">
            
                    @include('superadmin.pages.admin.form')
        </div>
        <div class="col-sm-4 col-lg-3">
            <div class="card">
                <div class="card-body"style="text-align: center;padding: 32px;">
                    <button class="btn btn-primary">
                        Save
                    </button>
                    <button type="reset" class="btn btn-secondary">
                        Reset
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

</div>
@endsection