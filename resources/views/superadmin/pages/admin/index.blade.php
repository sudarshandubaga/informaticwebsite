@extends('superadmin.layouts.inner')
@section('site_title','View Admin')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Admin</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <a href="{{ route('super.admin.dashboard')}}">Dasgboard</a>
        </li>
        <li class="breadcrumb-item active">View Admin</li>
    </ol>

        {{Form::open(['method'=>'GET'])}}

            <div class="form-outline mb-4">
            {{ Form::label('search_by_keyword', null, ['class' => 'form-label']) }}
            {{ Form::search('search_by_keyword', request('search_by_keyword'), ['class' => 'form-control', 'placeholder' => 'Search By Keyword']) }}
            </div>
            <div id="datatable">
            </div>

        {{Form::close()}}

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            @include('superadmin.templates.messages')

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Name</th>
                            <th>E-Mail</th>
                            {{-- <th>Password</th> --}}
                            <th>Site id</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($admin as $i => $a)
                        <tr>
                            <td>{{ $i + $admin->firstItem() }}</td>
                            <td>{{ $a->name }}</td>
                            <td>{{ $a->email }}</td>
                            {{-- <td>{{ $a->password }}</td> --}}
                            <td>{{ $a->site_id }}</td>
                            <td>
                                <div class="dropdown show">
                               <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               </a>
                                       <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           @if(request()->get('type') && request()->get('type') === 'trash')
                                           <li>
                                               {{ Form::open(['url' => route('super.admin.admin.destroy', [$a->id, 'action' => 'restore']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                               <button type="button" class="delete-btn dropdown-item">Restore</button>
                                               {{ Form::close() }}
                                           </li>
                                           <li>
                                               {{ Form::open(['url' => route('super.admin.admin.destroy', [$a->id, 'action' => 'permanent-delete']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                               <button type="button" class="delete-btn dropdown-item">Permanent Delete</button>
                                               {{ Form::close() }}
                                           </li>
                                           @else
                                           <li><a class="dropdown-item"
                                                   href="{{  route ('super.admin.admin.edit',[$a->id ] ) }}">Edit</a></li>
                                           <li>
                                               {{ Form::open(['url' => route('super.admin.admin.destroy', [$a->id]),
                                               'method' => 'delete', 
                                               'class' => 'delete-form',
                                               
                                               ]) }}
                                               <button type="button" class="delete-btn dropdown-item">Delete</button>
                                               {{ Form::close() }}
                                           </li>
                                           @endif
                                       </ul>
                                   </div>
                             </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $admin->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
</div>
@endsection