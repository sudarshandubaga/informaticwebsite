<div class="card shadow mb-4">
    <div class="card-header py-3">
       Basic Information
    </div>
    <div class="card-body">
    <div class="mb-3">
    {{ Form::label('name') }}
    {{ Form::text('name', null, ['class' => 'form-control title', 'placeholder' => 'Enter Name', 'data-target' => '#slug', 'required'=>'required']) }}

    <div class="invalid-feedback">
        Please Enter Name.
      </div>
    </div>
    <div class="mb-3">
    {{ Form::label('email') }}
    {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter E-mail']) }}
    </div>
   <div class="mb-3">
    {{Form::label('site_id', null, ['class' =>'form-label'])}}
    {{Form::select('site_id',$site,null , ['class' => 'form-control', 'placeholder'=> 'Select Site', 'autocomplete' => 'off']) }}
    </div>
    <div class="mb-3">
     {{Form::label('password', null, ['class' =>'form-label'])}}
     {{Form::text('password',null, ['class' => 'form-control', 'placeholder'=> 'Enter Password']) }}
     </div>

    </div>
</div>