@extends('superadmin.layouts.inner')

@section('content')
    <div class="mainDiv" >
        <div class="cardStyle">
            <form action="" method="post" name="signupForm" id="signupForm">
            
            <!-- <img src="{{url('admin\assets\images\portfolio\item-3.jpg')}}" id="signupLogo"/> -->
            
            <h2 class="formTitle align-center">
                Login to your account
            </h2>

            <div class="inputDiv">

            <label class="inputLabel" for="password">Current Password</label>
            <input type="password" id="password" name="current_password" required>
            </div>
            
            <div class="inputDiv">

            <label class="inputLabel" for="password">New Password</label>
            <input type="password" id="password" name="password" required>
            </div>
            
            <div class="inputDiv">
            <label class="inputLabel" for="confirmPassword">Confirm Password</label>
            <input type="password" id="confirmPassword" name="confirmed_password">
            </div>

            <div class="buttonWrapper">
            <button type="submit" id="submitButton" onclick="validateSignupForm()" class="submitButton pure-button pure-button-primary">
                <span>Continue</span>
                <!-- <span id="loader"></span> -->
            </button>
            </div>
            
        </form>
        </div>
        </div>
@endsection