<div class="mb-3">
    {{ Form::label('title', 'Site Title') }}
    {{ Form::text('title', null, ['class' => 'form-control title', 'placeholder' => 'Enter Title', 'data-target' => '#slug', 'required'=>'required']) }}

    <div class="invalid-feedback">
        Please Enter Title.
      </div>
</div>
<div class="mb-3">
    {{ Form::label('tagline') }}
    {{ Form::text('tagline', null, ['class' => 'form-control', 'placeholder' => 'Enter tagline']) }}
</div>

<div class=" mb-3 ">
    {{ Form::label('domain') }}
    {{ Form::text('domain',null, ['class' => 'form-control', 'placeholder' => 'Enter Domain', 'required'=>'required']) }}

    
    <div class="invalid-feedback">
        Please Enter Domain.
      </div>
</div>


<div class="mb-3">
    {{Form::label('theme', null, ['class' =>'form-label'])}}
    {{Form::text('theme',null , ['class' => 'form-control', 'placeholder'=> 'Enter Theme', 'list' => 'allthemes', 'autocomplete' => 'off']) }}
    <datalist id="allthemes">
        @foreach($themes as $t)
        <option value="{{ $t }}">{{ $t }}</option>
        @endforeach
    </datalist>
</div>

<div class="row style="margin: 9px;>

<div class="mb-3 col-lg-3 " style="margin: auto;">
    {{ Form::label('logo') }}
    @if(!empty($site->logo))
    <label for="logo" class="card mb-3" style="width:10rem;">
        <img src="{{url('images/sites/thumbnail/'.$site->logo)}}" alt=".." class="card-img-top"
            name="image">
    </label>
    @endif
    <label class="btn  btn-primary">
        {{Form::file('logo',['style'=>'display:none;']) }}
        Choose Footer logo
    </label>
</div>

<div class="mb-3 col-lg-3 " style="margin: auto;">
    {{ Form::label('footer_logo') }}
    @if(!empty($site->footer_logo))
    <label for="footer_logo" class="card mb-3" style="width:10rem;">
        <img src="{{url('images/sites/thumbnail/'.$site->footer_logo)}}" alt=".." class="card-img-top"
            name="image">
    </label>
    @endif
    <label class="btn  btn-primary">
        {{Form::file('footer_logo',['style'=>'display:none;']) }}
        Choose Footer logo
    </label>
</div>


<div class="mb-3 col-lg-3 " style="margin: auto;">
    {{ Form::label('favicon') }}
    @if(!empty($site->favicon))
    <label for="favicon" class="card mb-3" style="width:10rem;">
        <img src="{{url('images/sites/thumbnail/'.$site->favicon)}}" alt=".." class="card-img-top"
            name="image">
    </label>
    @endif
    <label class="btn  btn-primary">
        {{Form::file('favicon',['style'=>'display:none;']) }}
        Choose favicon
    </label>
</div>
</div>
{{-- 
@php
$label = !empty($site->favicon) ? $site->favicon : 'Featured favicon';
@endphp --}}
