@extends('superadmin.layouts.inner')
@section('site_title','View Sites')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">View Sites</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <a href="{{ route('super.admin.dashboard')}}">Dasgboard</a>
        </li>
        <li class="breadcrumb-item active">View Site</li>
    </ol>

        {{Form::open(['method'=>'GET'])}}

            <div class="form-outline mb-4">
            {{ Form::label('search_by_keyword', null, ['class' => 'form-label']) }}
            {{ Form::search('search_by_keyword', request('search_by_keyword'), ['class' => 'form-control', 'placeholder' => 'Search By Keyword']) }}
            </div>
            <div id="datatable">
            </div>

        {{Form::close()}}

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            @include('superadmin.templates.messages')

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Title</th>
                            <th>Tagline</th>
                            <th>Logo</th>
                            <th>Footer-Logo</th>
                            <th>Favicon</th>
                            <th>Domain</th>
                            <th>Theme</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($site as $i => $s)
                        <tr>
                            <td>{{ $i + $site->firstItem() }}</td>
                            <td>{{ $s->title }}</td>
                            <td>{{ $s->tagline }}</td>
                          
                            <td>                 
                               <img src="{{ url('storage/'. '/images/'.'/thumbnail/'.$s->logo)}}" alt=""style="width: 50px; height: 50px; object-fit:contain">
                            </td>

                            <td>                 
                               <img src="{{ url('storage/'. '/images/thumbnail/'.$s->footer_logo)}}
                               " alt=""style="width: 50px; height: 50px; object-fit:contain">
                            </td>
                            
                            <td>                 
                               <img src="{{ url('storage/'. '/images/thumbnail/'.$s->favicon)}}" alt=""style="width: 50px; height: 50px; object-fit:contain">
                            </td>

                            <td>{{ $s->domain }}</td>
                            <td>{{ $s->theme }}</td>
                            <td>
                                 <div class="dropdown show">
                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </a>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            @if(request()->get('type') && request()->get('type') === 'trash')
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.site.destroy', [$s->id, 'action' => 'restore']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                                <button type="button" class="delete-btn dropdown-item">Restore</button>
                                                {{ Form::close() }}
                                            </li>
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.site.destroy', [$s->id, 'action' => 'permanent-delete']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                                <button type="button" class="delete-btn dropdown-item">Permanent Delete</button>
                                                {{ Form::close() }}
                                            </li>
                                            @else
                                            <li><a class="dropdown-item"
                                                    href="{{  route ('super.admin.site.edit',[$s->id ] ) }}">Edit</a></li>
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.site.destroy', [$s->id]),
                                                'method' => 'delete', 
                                                'class' => 'delete-form',
                                                
                                                ]) }}
                                                <button type="button" class="delete-btn dropdown-item">Delete</button>
                                                {{ Form::close() }}
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                              </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $site->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
</div>
@endsection