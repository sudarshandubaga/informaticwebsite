@extends('superadmin.layouts.inner')
@section('site_title','Add sites')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add Sites</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('super.admin.dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('super.admin.site.index')}}">Site</a>
                </li>
                <li class="breadcrumb-item active">Add Site</li>
            </ol>
        </div>
        <div class="card-body ">
            {{ Form::open([
                'url' => route('super.admin.site.store'),
                'files' => true, 
                'method' => 'post',
                'class' =>'needs-validation',
                'novalidate' =>'true',
                ]) }}
            @include('superadmin.pages.site.form')
            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
            {{ Form::close() }}
        </div>

        </div>
    </div>

</div>
@endsection