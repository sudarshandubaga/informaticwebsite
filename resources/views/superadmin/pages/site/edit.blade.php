@extends('superadmin.layouts.inner')
@section('site_title','Edit sites')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Sites</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('super.admin.dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('super.admin.site.index')}}">Sites</a>
                </li>
                <li class="breadcrumb-item active">Edit Sites</li>
            </ol>
        </div>
        <div class="card-body">
            {{ Form::open([
                'url' => route('super.admin.site.update',$site->id),
                'files' => true, 
                'method' => 'PUT'
                ]) }}
            @include('superadmin.pages.site.form')
            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>

</div>
@endsection