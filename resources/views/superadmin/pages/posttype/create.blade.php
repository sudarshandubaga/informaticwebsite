@extends('superadmin.layouts.inner')
@section('site_title','Add Post type')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add Post Type</h1>
        <ol class="breadcrumb mb-4">  
            <li class="breadcrumb-item">
                <a href="{{ route('super.admin.dashboard')}}">Dashboard</a>
            </li>        
            <li class="breadcrumb-item">
                <a href="{{ route('super.admin.posttype.index')}}">Post Type</a>
            </li>
            <li class="breadcrumb-item active">Add Post Type</li>
        </ol>

            {{ Form::open([
                'url' => route('super.admin.posttype.store'),
                'files' => true, 
                'method' => 'post',
                'class' =>'needs-validation',
                'novalidate' =>'true',
            ]) }}
    <!-- DataTales Example -->
    <div class="row">
        <div class="col-sm-8 col-lg-9">
            
                    @include('superadmin.pages.posttype.form')
        </div>
        <div class="col-sm-4 col-lg-3">
            <div class="card">
                <div class="card-body"style="text-align: center;padding: 32px;">
                    <button class="btn btn-primary">
                        Save
                    </button>
                    <button type="reset" class="btn btn-secondary">
                        Reset
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>


@endsection