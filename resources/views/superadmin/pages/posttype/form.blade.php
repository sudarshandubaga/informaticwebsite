<div class="card shadow mb-4">
    <div class="card-header py-3">
       Basic Information
    </div>
    <div class="card-body">
    <div class="mb-3">
    {{ Form::label('name') }}
    {{ Form::text('name', null, ['class' => 'form-control title', 'placeholder' => 'Enter Name', 'data-target' => '#slug', 'required'=>'required']) }}

    <div class="invalid-feedback">
        Please Enter Name.
      </div>
    </div>
    <div class="mb-3">
    {{ Form::label('slug') }}
    {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter Slug']) }}
    </div>
   <div class="mb-3">
    {{Form::label('site_id', null, ['class' =>'form-label'])}}
    {{Form::select('site_id',$site,null , ['class' => 'form-control', 'placeholder'=> 'Select Site', 'autocomplete' => 'off']) }}
    </div>

    {{Form::label('is_category', 'Category Availability')}}

    <div class="mb-3 d-flex p-1">
    {{ Form::radio('is_category', 'Y' , false,['id'=>'inlineRadio1']) }}
    {{Form::label('inlineRadio1','YES', ['class' => 'form-label']) }}
    &nbsp; &nbsp;
    &nbsp;
    {{ Form::radio('is_category', 'N' ,true ,['id'=>'inlineRadio2']) }}
    {{Form::label('inlineRadio2','NO', ['class' => 'form-label']) }}
    </div>
    
    </div>
</div>
    <div class="card">
    <div class="card-header py-3">
             Post Fields
    </div>

    <div class="postfields card-body">
            <div class="row  my-4">           
            <div class="col-4 d-flex main-form">
                {{ Form::checkbox('title','Title', true, ['class'=>'enable_cb','id'=>'title']) }} 
                {{ Form::label('title') }}
            </div>
            <div class="col-8">
                <div class="max_tickets ">
                    {{ Form::text('post_field[title]',null, [ 'class'=>'form-control','placeholder' => 'Enter Title']) }} 
                </div>
            </div>
            </div>
            
            <div class="row  my-4">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('exceprt',null, true, ['class'=>'enable_cb','id'=>'exceprt']) }} 
                    {{ Form::label('exceprt') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[excerpt]',null, [ 'class'=>'form-control','placeholder' => 'Enter Exceprt']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('description',null, true, ['class'=>'enable_cb','id'=>'description']) }} 
                    {{ Form::label('description') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[description]',null, [ 'class'=>'form-control','placeholder' => 'Enter Description']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('image',null, true, ['class'=>'enable_cb','id'=>'image']) }} 
                    {{ Form::label('image') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[image]','Image', [ 'class'=>'form-control','placeholder' => 'Image']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('meta_title',null, true, ['class'=>'enable_cb','id'=>'meta_title']) }} 
                    {{ Form::label('meta_title') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets">
                        {{ Form::text('post_field[meta_title]',null, [ 'class'=>'form-control','placeholder' => 'Enter meta_title']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4 ">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('meta_keywords',null, true, ['class'=>'enable_cb','id'=>'meta_keyword']) }} 
                    {{ Form::label('meta_keyword') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[meta_keywords]',null, [ 'class'=>'form-control','placeholder' => 'Enter meta_keyword']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4 ">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('meta_description',null, true, ['class'=>'enable_cb','id'=>'meta_description']) }} 
                    {{ Form::label('meta_description') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[meta_description]','meta description', [ 'class'=>'form-control','placeholder' => 'Enter meta_description']) }} 
                    </div>
                </div>
            </div>
            
            <div class="row  my-4 ">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('template',null, false, ['class'=>'enable_cb','id'=>'template']) }} 
                    {{ Form::label('template') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[template]','Template', [ 'class'=>'form-control','placeholder' => 'Enter template' ]) }} 
                    </div>
                </div>
            </div>

            
            <div class="row  my-4 ">           
                <div class="col-4 d-flex main-form">
                    {{ Form::checkbox('post_id',null, false, ['class'=>'enable_cb','id'=>'post_id']) }} 
                    {{ Form::label('post_id') }}
                </div>
                <div class="col-8">
                    <div class="max_tickets ">
                        {{ Form::text('post_field[post_id]','Parent', [ 'class'=>'form-control','placeholder' => 'Enter Parent' ]) }} 
                    </div>
                </div>
            </div>
    
        <div class="card mt-3">
            <div class="card-header py-3">
                 Custom Fields
            </div>
            <div class="container">
                <div class="my-5">
                  <button class="btn btn-success add-btn" type="button">Add Custom Fields</button>
                </div>
                <div class="lists"></div>
            </div>
              <script
                src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous"
              ></script>
              <script>
                $(function () {
                    var i = 0;
                  $(document).on("click", ".add-btn", function () {
                    // Append
                    var html = `
                    <div class="row mb-3">
                     <div class="col">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="post_field[extra_fields][${i}][meta_name]" />
                      </div>
                      <div class="col">
                        <label class="form-label">Label</label>
                        <input type="text" class="form-control"  placeholder="Label" name="post_field[extra_fields][${i}][label]"/>
                      </div>
                      <div class="col">
                        <label class="form-label">Type</label>
                        <select class="form-select" id="validationCustom04" required placeholder="Label" name="post_field[extra_fields][${i}][type]>
                           <option selected disabled value=>Choose...</option>
                           <option value="file">File</option>
                           <option value="text">Text Input</option>
                           <option value="textarea">Textarea</option>
                           <option value="editor">Editor</option>
                        </select>
                      </div>
                      <div class="col-2 mt-4">
                        <button class="btn btn-danger btn-remove">Remove</button>
                      </div>
                    </div>
                    `;
                    i++;
                    $(".lists").before(html);
                  });
          
                  $(document).on("click", ".btn-remove", function () {
                    $(this).parent().parent().remove();
                  });
                });
              </script>
        </div>
    </div>

    </div>