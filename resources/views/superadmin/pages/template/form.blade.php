<div class="mb-3">
    {{ Form::label('name', null, ['class' =>'form-label']) }}
    {{ Form::text('name', null, ['class' => 'form-control title', 'placeholder' => 'Enter Name', 'data-target' => '#slug', 'required'=>'required']) }}
    
    <div class="invalid-feedback">
        Please Enter Name.
      </div>
</div>

<div class="mb-3">
    {{Form::label('slug', null, ['class' =>'form-label'])}}
    {{Form::text('slug',null , ['class' => 'form-control', 'placeholder'=> 'Enter Slug']) }}
</div>

<div class="mb-3">
    {{Form::label('theme', null, ['class' =>'form-label'])}}
    {{Form::text('theme',null , ['class' => 'form-control', 'placeholder'=> 'Enter Theme', 'list' => 'allthemes', 'autocomplete' => 'off']) }}
    <datalist id="allthemes">
        @foreach($themes as $t)
        <option value="{{ $t }}">{{ $t }}</option>
        @endforeach
    </datalist>
</div>
