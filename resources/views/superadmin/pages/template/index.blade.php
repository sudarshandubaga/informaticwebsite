@extends('superadmin.layouts.inner')
@section('site_title','View Templates')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">View Templates</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <a href="{{ route('super.admin.dashboard')}}">Dasgboard</a>
        </li>
        <li class="breadcrumb-item active">View Templates</li>
    </ol>

        {{Form::open(['method'=>'GET'])}}

            <div class="form-outline mb-4">
            {{ Form::label('search_by_keyword', null, ['class' => 'form-label']) }}
            {{ Form::search('search_by_keyword', request('search_by_keyword'), ['class' => 'form-control', 'placeholder' => 'Search By Keyword']) }}
            </div>
            <div id="datatable">
            </div>

        {{Form::close()}}

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            @include('superadmin.templates.messages')

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Name</th>
                            <th>Theme</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($template as $i => $t)
                        <tr>
                            <td>{{ $i + $template->firstItem() }}</td>
                            <td>{{ $t->name }}</td>
                            <td>{{ $t->theme }}</td>
                            <td>
                                 <div class="dropdown show">
                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </a>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            @if(request()->get('type') && request()->get('type') === 'trash')
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.template.destroy', [$t->id, 'action' => 'restore']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                                <button type="button" class="delete-btn dropdown-item">Restore</button>
                                                {{ Form::close() }}
                                            </li>
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.template.destroy', [$t->id, 'action' => 'permanent-delete']), 'method' => 'DELETE', 'class' => 'delete-form']) }}
                                                <button type="button" class="delete-btn dropdown-item">Permanent Delete</button>
                                                {{ Form::close() }}
                                            </li>
                                            @else
                                            <li><a class="dropdown-item"
                                                    href="{{  route ('super.admin.template.edit',[$t->id ] ) }}">Edit</a></li>
                                            <li>
                                                {{ Form::open(['url' => route('super.admin.template.destroy', [$t->id]),
                                                'method' => 'delete', 
                                                'class' => 'delete-form',
                                                
                                                ]) }}
                                                <button type="button" class="delete-btn dropdown-item">Delete</button>
                                                {{ Form::close() }}
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                              </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $template->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
</div>
@endsection