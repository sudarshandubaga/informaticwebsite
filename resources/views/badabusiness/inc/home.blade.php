@extends('badabusiness.layouts.app')
@section('main_section')

@php
$about = \App\Models\PostData::getPostRow('about-us');
$slider = \App\Models\PostData::getPostData('slider');
@endphp
<div class="slider-area">
    <div class="slider-active owl-carousel">
        @foreach($slider as $s)
        <div class="single-slider slider-height-1 bg-img"
            style="background-image:url('{{ url($site->domain . '/images/slider/' . $s->image) }}');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-7 col-12 col-sm-12">
                        <div class="slider-content pt-230">
                            <h1 class="animated">{{$s->title}}</h1>
                            <p class="animated">{{$s->excerpt}}</p>
                            <div class="slider-btn">
                                <a class="animated default-btn btn-green-color"
                                    href="{{ route('post.show', $about['slug']) }}">{{$about->title}}</a>
                                <a class="animated default-btn btn-white-color"
                                    href="{{ route('post.show','contact') }}">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="slider-single-img
                                    slider-animated-1">
                    <img class="animated" src="business/assets/img/slider/single-slide-1.png" alt="">
                </div> -->
            </div>
        </div>
        @endforeach
    </div>
</div>

@php
// $sliders = \App\Models\PostData::getPostRow('sliders');
$facility = \App\Models\PostData::getPostData('facility');

$colorArr = [
    "light-blue",
    "yellow",
    "blue",
    "green"    
];
@endphp
<div class="choose-us section-padding-1">
    <div class="container-fluid">
        <div class="row no-gutters choose-negative-mrg">
        @foreach($facility as $index => $f)
            <div class="col-lg-3 col-md-6">
                <div class="single-choose-us
                                    choose-bg-{{ $colorArr[$index] }}">
                    <div class="choose-img">
                        <img class="animated" src="{{ url($site->domain. '/images/facility/' . $f->image) }}" alt="">
                    </div>
                    <div class="choose-content">
                        <h3>{{$f->title}}</h3>
                        <p>{{$f->excerpt}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@php
@endphp
<div class="about-us pt-130 pb-130">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="about-content">
                    <div class="section-title
                                        section-title-green
                                        mb-30">
                        <h2>{{$about['title']}}</h2>
                    </div>
                    <p>{{ $about['excerpt'] }}</p>
                    <div class="about-btn mt-45">
                        <a class="default-btn" href="{{ route('post.show', $about['slug']) }}">ABOUT
                            US</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="about-img default-overlay">
                    <img src="https://img.youtube.com/vi/{{ $about->getMetaValue('video_id') }}/0.jpg" alt="">
                    <a class="video-btn video-popup"
                        href="https://www.youtube.com/watch?v={{ $about->getMetaValue('video_id') }}">
                        <img class="animated" src="business/assets/img/icon-img/video.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@php
$course = \App\Models\PostData::getPostRow('courses');
$courses = \App\Models\PostData::getPostData('course');
@endphp
<div class="course-area bg-img pt-130 pb-10" style="background-image:url(business/assets/img/bg/bg-1.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2> {{ $course->title }}</h2>
            <p>{{ $course->excerpt }}</p>
        </div>
        @if(!$courses->isEmpty())
        <div class="course-slider-active nav-style-1
                            owl-carousel">
            @foreach($courses as $c)
            <div class="single-course">
                <div class="course-img">
                    <a href="course-details.html"><img class="animated" src="{{ url($site->domain .'/images/course/' . $c->image) }}"
                            alt=""></a>
                    <span>Addmission Going On</span>
                </div>
                <div class="course-content" >
                    <h4><a href="course-details.html">{{ $c->title }}</a></h4>
                    <p>{{$c->excerpt}}</p>
                </div>
                <div class="course-position-content">
                    <div class="credit-duration-wrap">
                        <!-- <div class="sin-credit-duration">
                            <i class="fa fa-diamond"></i>
                            <span>Credits : 125</span>
                        </div> -->
                        <div class="sin-credit-duration">
                            <i class="fa fa-clock-o"></i>
                            <span>Duration : 4yrs</span>
                        </div>
                    </div>
                    <div class="course-btn">
                        <a class="default-btn" href="#">APPLY
                            NOW</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>
@php
$achievement = \App\Models\PostData::getPostRow('our-achievement');
$testimonials = \App\Models\PostData::getPostData('testimonial');
@endphp
<div class="achievement-area pt-130 pb-115">
    <div class="container">
        <div class="section-title mb-75">
            <h2>{{$achievement->title}}</h2>
            <p>{{$achievement->excerpt}}</p>
        </div>
        <!-- <--<div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-one">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/achieve-1.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">890</h2>
                        <span>STUDENTS</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-two">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/achieve-2.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">670</h2>
                        <span>GRADUATE</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-three">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/achieve-3.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">160</h2>
                        <span>AWARD WINNING</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-four">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/achieve-4.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">200</h2>
                        <span>FACULTIES</span>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="testimonial-slider-wrap mt-45">
            <div class="testimonial-text-slider">
                @foreach ($testimonials as $t)
                <div class="testi-content-wrap">
                    <div class="testi-big-img">
                        <img alt="" src="{{ url($site->domain . '/images/testimonial/' . $t->image) }}" alt="{{ $t->title }}">
                    </div>
                    <div class="row g-0">
                        <div class="ms-auto col-lg-6 col-md-12">
                            <div class="testi-content bg-img
                            default-overlay" style="background-image:url(business/assets/img/bg/testi.png);">
                                <div class="quote-style
                            quote-left">
                                    <i class="fa fa-quote-left"></i>
                                </div>
                                <p>{!! $t->description !!}</p>
                                <div class="testi-info">
                                    <h5><a href="{{ route('post.show', $t->slug) }}">{{ $t->title }}</a></h5>
                                    <span>{{ $t->excerpt }}</span>
                                </div>
                                <div class="quote-style
                                                    quote-right">
                                    <i class="fa
                                                        fa-quote-right"></i>
                                </div>
                                <div class="testi-arrow">
                                    <img alt="" src="business/assets/img/icon-img/testi-icon.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="testimonial-image-slider">
            @foreach ($testimonials as $t)
            <div class="sin-testi-image">
                <img src="{{ url($site->domain . '/images/testimonial/thumbnail/' . $t->image) }}" alt="">
            </div>
            @endforeach

        </div>
        </div>

    </div>
    <div class="testimonial-text-img">
        <img alt="" src="business/assets/img/icon-img/testi-text.png">
    </div>
</div>
</div>

@include('badabusiness.inc.registration')

@php
$teacher = \App\Models\PostData::getPostRow('our-teacher');
$teachers = \App\Models\PostData::getPostData('teachers');
@endphp
<div class="teacher-area pt-130 pb-100">
    <div class="container">
        <div class="section-title mb-75">
            <h2>{{$teacher->title}}</h2>
            <p>{!!$teacher->description!!}</p>
        </div>
        <div class="custom-row">
            @foreach ($teachers as $t)
            <div class="custom-col-5">
                <div class="single-teacher mb-30">
                    <div class="teacher-img">
                        <img src="{{ url($site->domain.'/images/teachers/' . $t->image) }}" alt="">
                    </div>
                    <div class="teacher-content-visible">
                        <h4>{{$t->title}}</h4>
                        <h5>{{$t->getMetaValue('designation')}}</h5>
                    </div>
                    <div class="teacher-content-wrap">
                        <div class="teacher-content">
                            <h4>{{$t->title}}</h4>
                            <h5>{{$t->getMetaValue('designation')}}</h5>
                            <p>{{$t->exceprt}}</p>
                            <div class="teacher-social">
                                <ul>
                                    <li><a class="facebook" href="#"><i class="fa
                                                                fa-facebook"></i></a></li>
                                    <li><a class="youtube-play" href="#"><i class="fa
                                                                fa-youtube-play"></i></a></li>
                                    <li><a class="twitter" href="#"><i class="fa
                                                                fa-twitter"></i></a></li>
                                    <li><a class="google-plus" href="#"><i class="fa
                                                                fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>

<!--<div class="event-area bg-img default-overlay pt-130 pb-130" style="background-image:url(business/assets/img/bg/bg-3.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2><span>Our</span> Event</h2>
            <p>tempor incididunt ut labore et dolore magna
                aliqua.
                Ut
                enim ad minim <br>veniam, quis nostrud
                exercitation
                ullamco laboris nisi ut aliquip </p>
        </div>
        <div class="event-active owl-carousel nav-style-1">
            <div class="single-event event-white-bg">
                <div class="event-img">
                    <a href="event-details.html"><img src="business/assets/img/event/event-1.jpg" alt=""></a>
                    <div class="event-date-wrap">
                        <span class="event-date">1st</span>
                        <span>Dec</span>
                    </div>
                </div>
                <div class="event-content">
                    <h3><a href="event-details.html">Aempor
                            incididunt
                            ut labore ejam.</a></h3>
                    <p>Pvolupttem accusantium doloremque
                        laudantium,
                        totam erspiciatis unde omnis iste natus
                        error .</p>
                    <div class="event-meta-wrap">
                        <div class="event-meta">
                            <i class="fa fa-location-arrow"></i>
                            <span>Mascot Plaza ,Uttara</span>
                        </div>
                        <div class="event-meta">
                            <i class="fa fa-clock-o"></i>
                            <span>11:00 am</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-event event-white-bg">
                <div class="event-img">
                    <a href="event-details.html"><img src="business/assets/img/event/event-2.jpg" alt=""></a>
                    <div class="event-date-wrap">
                        <span class="event-date">10th</span>
                        <span>Dec</span>
                    </div>
                </div>
                <div class="event-content">
                    <h3><a href="event-details.html">Global
                            Conference
                            on Business.</a></h3>
                    <p>Pvolupttem accusantium doloremque
                        laudantium,
                        totam erspiciatis unde omnis iste natus
                        error .</p>
                    <div class="event-meta-wrap">
                        <div class="event-meta">
                            <i class="fa fa-location-arrow"></i>
                            <span>Shubastu ,Dadda</span>
                        </div>
                        <div class="event-meta">
                            <i class="fa fa-clock-o"></i>
                            <span>08:30 am</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-event event-white-bg">
                <div class="event-img">
                    <a href="event-details.html"><img src="business/assets/img/event/event-3.jpg" alt=""></a>
                    <div class="event-date-wrap">
                        <span class="event-date">1st</span>
                        <span>Dec</span>
                    </div>
                </div>
                <div class="event-content">
                    <h3><a href="event-details.html">Academic
                            Conference
                            Maui.</a></h3>
                    <p>Pvolupttem accusantium doloremque
                        laudantium,
                        totam erspiciatis unde omnis iste natus
                        error .</p>
                    <div class="event-meta-wrap">
                        <div class="event-meta">
                            <i class="fa fa-location-arrow"></i>
                            <span>Banasree ,Rampura</span>
                        </div>
                        <div class="event-meta">
                            <i class="fa fa-clock-o"></i>
                            <span>10:00 am</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-event event-white-bg">
                <div class="event-img">
                    <a href="event-details.html"><img src="business/assets/img/event/event-2.jpg" alt=""></a>
                    <div class="event-date-wrap">
                        <span class="event-date">1st</span>
                        <span>Dec</span>
                    </div>
                </div>
                <div class="event-content">
                    <h3><a href="event-details.html">Social
                            Sciences
                            &
                            Education.</a></h3>
                    <p>Pvolupttem accusantium doloremque
                        laudantium,
                        totam erspiciatis unde omnis iste natus
                        error .</p>
                    <div class="event-meta-wrap">
                        <div class="event-meta">
                            <i class="fa fa-location-arrow"></i>
                            <span>Shubastu ,Badda</span>
                        </div>
                        <div class="event-meta">
                            <i class="fa fa-clock-o"></i>
                            <span>10:30 am</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

@php
$articles = \App\Models\PostData::getPostRow('our-articals');
$article = \App\Models\PostData::getPostData('article');
@endphp
<div class="blog-area pt-130 pb-100">
    <div class="container">
        <div class="section-title mb-75">
            <h2>{{$articles->title}}</h2>
            <p>{{$articles->excerpt}}</p>
        </div>.

        <div class="row">
            @foreach($article as $a)
            <div class="col-lg-3 col-md-6">
                <div class="single-blog mb-30">
                    <div class="blog-img">
                        <a href="" alt="">
                            <img src="{{ url($site->domain.'/images/article/' . $a->image) }}" alt=""></a>
                    </div>
                    <div class="blog-content-wrap">
                        <div class="blog-content">
                            <h4><a href="blog-details.html">{{$a->title}}</a></h4>
                            <p>{{$a->excerpt}}</p>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
</div>


@endsection