

<div class="register-area  bg-img pt-130 pb-130" style="background-image:url(business/assets/img/bg/11.jpg);">
    <div class="container">
    @if(\Session::has('success'))
        <p class="alert alert-success">{{ \Session::get('success') }}</p>
        @endif
        <div class="section-title-2 mb-75 white-text">
            <h2>Register <span>Now</span></h2>
            <p>Winter Admission Is Going On. We are announcing
                Special
                discount for winter batch 2019.</p>
        </div>
        <div class="register-wrap">
            <!-- <div id="register-3" class="mouse-bg">
                <div class="winter-banner">
                    <img src="business/assets/img/banner/regi-1.png" alt="">
                    <div class="winter-content">
                        <span>WINTER </span>
                        <h3>2019</h3>
                        <span>ADMISSION </span>
                    </div>
                </div>
            </div> -->
            {{ Form::open(['url' => route('registration.send')]) }}

            <div class="row">
                <div class="col-lg-10 col-md-8">
                    <div class="register-form">
                        <h4>Get A free Registration</h4>
                        <form>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="contact-form-style
                                                        mb-20">
                                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) }}                                    
                                </div>
                                </div>
                              
                                <div class="col-lg-6">
                                    <div class="contact-form-style
                                                        mb-20">
                                    {{ Form::tel('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone', 'required' => 'required']) }}                                   
                                 </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="contact-form-style
                                                        mb-20">
                                    {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email Address','required' => 'required']) }}                                    
                                </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="contact-form-style">
                                    <textarea placeholder="Message*" class="textarea form-control" name="message" id="form-message" rows="8" cols="20" data-error="Message field is required" required></textarea>                                       
                                    <div class="btn mt-10px">
                                    <button class="submit default-btn" type="submit">SUBMIT
                                            NOW</button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="register-1" class="mouse-bg"></div>
    <div id="register-2" class="mouse-bg"></div>
</div>