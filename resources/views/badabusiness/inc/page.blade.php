@extends('badabusiness.layouts.app')

@section('main_section')
<div class="breadcrumb-area">
    <div class="breadcrumb-top default-overlay bg-img breadcrumb-overly-3 pt-100 pb-95"
        style="background-image:url(business/assets/img/bg/breadcrumb-bg-3.jpg);">
        <div class="container">
            <h2>{{$post->title}}</h2>
            <p>{{$post->exceprt}}
            </p>
        </div>
    </div>
    <div class="breadcrumb-bottom">
        <div class="container">
            <ul>
                <li><a href="#">Home</a> <span><i class="fa fa-angle-double-right"></i>{{$post->title}}</span></li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-us-page2-area" style="margin: 59px;">
    <div class="container">
        <p>
            {!! $post->description !!}
        </p>
    </div>
</div>

@endsection