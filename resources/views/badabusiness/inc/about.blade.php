@extends('badabusiness.layouts.app')

<!-- "background-image:url('{{url('images/bg/45.jpg')}}')" -->
@section('main_section')

<div class="breadcrumb-area">
    <div class="breadcrumb-top default-overlay bg-img pt-100 pb-95"
        style="background-image:url('{{url('business/assets/img/bg/breadcrumb-bg.jpg')}}')">
        <div class="container">
            <h2>{{ $post->title }}</h2>
            <p>{{ $post->excerpt }}

            </p>
        </div>
    </div>
    <div class="breadcrumb-bottom">
        <div class="container">
            <ul>
                <li><a href="{{route('home')}}">Home</a> <span><i class="fa fa-angle-double-right"></i> About Page</span></li>
            </ul>
        </div>
    </div>
</div>
@php
// $sliders = \App\Models\PostData::getPostRow('sliders');
$facility = \App\Models\PostData::getPostData('facility');
@endphp
<div class="choose-area bg-img pt-90" style="background-image:url('{{url('business/assets/img/bg/bg-8.jpg')}}')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="about-chose-us pt-120">
                    <div class="row">
                @foreach($facility as $f)
                        <div class="col-lg-6 col-md-6">
                            <div class="single-about-chose-us mb-95">
                                <div class="about-choose-img">
                                    <img src="{{ url($site->domain. '/images/facility/thumbnail/' . $f->image) }}" alt="">
                                </div>
                                <div class="about-choose-content text-light-blue">
                                    <h3>{{$f->title}}</h3>
                                    <p>{{$f->excerpt}}</p>
                                </div>
                            </div>
                        </div>
                @endforeach

                        <!-- <div class="col-lg-6 col-md-6">
                            <div class="single-about-chose-us mb-95 about-negative-mrg">
                                <div class="about-choose-img">
                                    <img src="business/assets/img/icon-img/service-10.png" alt="">
                                </div>
                                <div class="about-choose-content text-yellow">
                                    <h3>Best Teacher </h3>
                                    <p>magna aliqua. Ut enim ad minim veniam conse ctetur adipisicing elit, sed do
                                        exercitation ullamco</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-about-chose-us mb-95">
                                <div class="about-choose-img">
                                    <img src="business/assets/img/icon-img/service-11.png" alt="">
                                </div>
                                <div class="about-choose-content text-blue">
                                    <h3>Library & Book Store</h3>
                                    <p>magna aliqua. Ut enim ad minim veniam conse ctetur adipisicing elit, sed do
                                        exercitation ullamco</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-about-chose-us mb-95 about-negative-mrg">
                                <div class="about-choose-img">
                                    <img src="business/assets/img/icon-img/service-12.png" alt="">
                                </div>
                                <div class="about-choose-content text-green">
                                    <h3>25 Years Of Experience</h3>
                                    <p>magna aliqua. Ut enim ad minim veniam conse ctetur adipisicing elit, sed do
                                        exercitation ullamco</p>
                                </div>
                            </div>
                        </div> -->
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="about-img">
                    <img src="{{url('business/assets/img/banner/about.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@php
$about = \App\Models\PostData::getPostRow('about-us');
$slider = \App\Models\PostData::getPostData('slider');
@endphp

<div class="video-area bg-img pt-270 pb-270"
    style="background-image:url(https://img.youtube.com/vi/{{ $about->getMetaValue('video_id') }}/0.jpg);">
    <div class="video-btn-2">
        <a class="video-popup" href="https://www.youtube.com/watch?v={{ $about->getMetaValue('video_id') }}">
            <img class="animated" src="business/assets/img/icon-img/viddeo-btn.png" alt="">
        </a>
    </div>
</div>

@php
$teacher = \App\Models\PostData::getPostRow('our-teacher');
$teachers = \App\Models\PostData::getPostData('teachers');
@endphp
<div class="teacher-area pt-130 pb-100">
    <div class="container">
        <div class="section-title mb-75">
            <h2>{{$teacher->title}}</h2>
            <p>{!!$teacher->description!!}</p>
        </div>
        <div class="custom-row">
            @foreach ($teachers as $t)
            <div class="custom-col-5">
                <div class="single-teacher mb-30">
                    <div class="teacher-img">
                        <img src="{{ url($site->domain.'/images/teachers/' . $t->image) }}" alt="">
                    </div>
                    <div class="teacher-content-visible">
                        <h4>{{$t->title}}</h4>
                        <h5>{{$t->getMetaValue('designation')}}</h5>
                    </div>
                    <div class="teacher-content-wrap">
                        <div class="teacher-content">
                            <h4>{{$t->title}}</h4>
                            <h5>{{$t->getMetaValue('designation')}}</h5>
                            <p>{{$t->exceprt}}</p>
                            <div class="teacher-social">
                                <ul>
                                    <li><a class="facebook" href="#"><i class="fa
                                                                fa-facebook"></i></a></li>
                                    <li><a class="youtube-play" href="#"><i class="fa
                                                                fa-youtube-play"></i></a></li>
                                    <li><a class="twitter" href="#"><i class="fa
                                                                fa-twitter"></i></a></li>
                                    <li><a class="google-plus" href="#"><i class="fa
                                                                fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
<div class="fun-fact-area bg-img pt-130 pb-100" style="background-image:url(business/assets/img/bg/bg-2.jpg);">
    <div class="container">
        <div class="section-title-3 section-shape-hm2-2 white-text text-center mb-100">
            <h2><span>Fun</span> Fact</h2>
            <p>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <br> quis nostrud
                exercitation ullamco laboris nisi ut aliquip </p>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-one count-white">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/funfact-1.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">160</h2>
                        <span>AWARD WINNING</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-two count-white">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/funfact-2.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">200</h2>
                        <span>GRADUATE</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-three count-white">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/funfact-1.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">160</h2>
                        <span>AWARD WINNING</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-6 col-sm-6">
                <div class="single-count mb-30 count-four count-white">
                    <div class="count-img">
                        <img src="business/assets/img/icon-img/funfact-2.png" alt="">
                    </div>
                    <div class="count-content">
                        <h2 class="count">200</h2>
                        <span>FACULTIES</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@php
$achievement = \App\Models\PostData::getPostRow('our-achievement');
$testimonials = \App\Models\PostData::getPostData('testimonial');
@endphp
<div class="achievement-area pt-130 pb-115">
    <div class="container">
        <!-- <div class="section-title mb-75">
            <h2>What <span>People Say</span></h2>
            <p>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim <br>veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip </p>
        </div> -->
        <div class="testimonial-slider-wrap mt-45">
            <div class="testimonial-text-slider">
                @foreach ($testimonials as $t)
                <div class="testi-content-wrap">
                    <div class="testi-big-img">
                        <img alt="" src="{{ url($site->domain . '/images/testimonial/' . $t->image) }}" alt="{{ $t->title }}">
                    </div>
                    <div class="row g-0">
                        <div class="ms-auto col-lg-6 col-md-6">
                            <div class="testi-content bg-img default-overlay"
                                style="background-image:url(business/assets/img/bg/testi.png);">
                                <div class="quote-style quote-left">
                                    <i class="fa fa-quote-left"></i>
                                </div>
                                <p>{!! $t->description !!}</p>
                                <div class="testi-info">
                                    <h5><a href="{{ route('post.show', $t->slug) }}">{{ $t->title }}</a></h5>
                                    <span>{{ $t->excerpt }}</span>
                                </div>
                                <div class="quote-style quote-right">
                                    <i class="fa fa-quote-right"></i>
                                </div>
                                <div class="testi-arrow">
                                    <img alt="" src="business/assets/img/icon-img/testi-icon.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="testimonial-image-slider">
            @foreach ($testimonials as $t)
            <div class="sin-testi-image">
                <img src="{{ url($site->domain . '/images/testimonial/thumbnail/' . $t->image) }}" alt="">
            </div>
            @endforeach
        </div>
        </div>

    </div>
    <div class="testimonial-text-img">
        <img alt="" src="business/assets/img/icon-img/testi-text.png">
    </div>
</div>
</div>
<div class="brand-logo-area pb-130">
    <div class="container">
        <div class="brand-logo-active owl-carousel">
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/1.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/3.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/4.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/5.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/6.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
            </div>
        </div>
    </div>
</div>

@endsection