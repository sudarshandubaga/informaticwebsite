@extends('badabusiness.layouts.app')

@section('main_section')

<div class="breadcrumb-area">
    <div class="breadcrumb-top default-overlay bg-img breadcrumb-overly-5 pt-100 pb-95"
        style="background-image:url(business/assets/img/bg/breadcrumb-bg-6.jpg);">
        <div class="container">
            @if(\Session::has('success'))
            <p class="alert alert-success">{{ \Session::get('success') }}</p>
            @endif
            <h2>Contact Us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore .
            </p>
        </div>
    </div>
    <div class="breadcrumb-bottom">
        <div class="container">
            <ul>
                <li><a href="#">Home</a> <span><i class="fa fa-angle-double-right"></i>Contact Us</span></li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-area pt-130 pb-130">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="contact-map mr-70">
                    <div>
                        <iframe src="{{ $site->google_map }}" style="border:0; width: 100%; height: 650px"
                            allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="contact-form">
                    <div class="contact-title mb-45">
                        <h2>Stay <span>Connected</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipis do eiusmod tempor indunt ut labore et dolore
                            magna aliqua.</p>
                    </div>
                    {{ Form::open(['url' => route('contact.send')]) }}
                    

                    <label>Name</label>
                    <input type="text" placeholder="Name*" class="form-control" name="name" id="form-name"
                        data-error="Name field is required" required>
                    <div class="help-block with-errors"></div>
                    <label>Email</label>
                    <input type="email" placeholder="Email*" class="form-control" name="email" id="form-email"
                        data-error="Email field is required" required>
                    <div class="help-block with-errors"></div>
                    <label>Subject</label>
                    {{ Form::select('subject', ['js' => 'JS', 'html' => 'HTML'], null, ['class' => 'form-select', 'required' => 'required', 'placeholder' => 'Select Any Option']) }}
                    <div class="help-block with-errors">
                        <div>
                            <label>Message</label>
                            <textarea placeholder="Message*" class="textarea form-control" name="message"
                                id="form-message" rows="8" cols="20" data-error="Message field is required"
                                required></textarea>
                            <div class="help-block with-errors"></div>
                            <button class="submit btn-style" type="submit">SEND MESSAGE</button>
                            {{ Form::close() }}
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-info-area bg-img pt-180 pb-140 default-overlay"
            style="background-image:url(business/assets/img/bg/contact-info.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="single-contact-info mb-30 text-center">
                            <div class="contact-info-icon">
                                <span><i class="fa fa-calendar-o"></i></span>
                            </div>
                            <p>{{$site->address}}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="single-contact-info mb-30 text-center">
                            <div class="contact-info-icon">
                                <span><i class="fa fa-calendar-o"></i></span>
                            </div>
                            <div class="contact-info-phn">
                                <div class="info-phn-title">
                                    <span>Phone : </span>
                                </div>
                                <div class="info-phn-number">
                                    <p>{{$site->phone}}</p>
                                    <p>+091111111111</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="single-contact-info mb-30 text-center">
                            <div class="contact-info-icon">
                                <span><i class="fa fa-calendar-o"></i></span>
                            </div>
                            <a href="#">{{$site->email}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="brand-logo-area pt-130 pb-130">
            <div class="container">
                <div class="brand-logo-active owl-carousel">
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/1.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/3.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/4.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/5.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/6.png" alt=""></a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

        @endsection