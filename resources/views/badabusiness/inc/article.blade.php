@extends('badabusiness.layouts.app')

@section('main_section')

@php
$articles = \App\Models\PostData::getPostRow('our-articals');
$article = \App\Models\PostData::getPostData('article');
@endphp

<div class="breadcrumb-area">
    <div class="breadcrumb-top default-overlay bg-img breadcrumb-overly-3 pt-100 pb-95"
        style="background-image:url(business/assets/img/bg/breadcrumb-bg-3.jpg);">
        <div class="container">
            <h2>{{$articles->title}}</h2>
            <p>{{$articles->excerpt}}
            </p>
        </div>
    </div>
    <div class="breadcrumb-bottom">
        <div class="container">
            <ul>
                <li><a href="{{route('home')}}">Home</a> <span><i class="fa fa-angle-double-right"></i>Event Grid</span></li>
            </ul>
        </div>
    </div>
</div>

<div class="blog-area pt-130 pb-100">
    <div class="container">
        <div class="section-title mb-75">
            <h2>{{$post->title}}</h2>
            <p>{{$post->excerpt}}</p>
        </div>.

        <div class="row">
            @foreach($article as $a)
            <div class="col-lg-3 col-md-6">
                <div class="single-blog mb-30">
                    <div class="blog-img">
                        <a href="" alt="">
                            <img src="{{ url($site->domain.'/images/article/' . $a->image) }}" alt=""></a>
                    </div>
                    <div class="blog-content-wrap">
                        <div class="blog-content">
                            <h4><a href="blog-details.html">{{$a->title}}</a></h4>
                            <p>{{$a->excerpt}}</p>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
</div>




<div class="brand-logo-area pb-130">
    <div class="container">
        <div class="brand-logo-active owl-carousel">
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/1.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/3.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/4.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/5.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/6.png" alt=""></a>
            </div>
            <div class="single-brand-logo">
                <a href="#"><img src="business/assets/img/brand-logo/2.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
@endsection