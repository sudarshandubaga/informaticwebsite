@extends('badabusiness.layouts.app')

@section('main_section')

<div class="breadcrumb-area">
    <div class="breadcrumb-top default-overlay bg-img breadcrumb-overly-3 pt-100 pb-95"
        style="background-image:url(business/assets/img/bg/breadcrumb-bg-3.jpg);">
        <div class="container">
            <h2>{{$post_type->title}}</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore .
            </p>
        </div>
    </div>
    <div class="breadcrumb-bottom">
        <div class="container">
            <ul>
                <li><a href="#">Home</a> <span><i class="fa fa-angle-double-right"></i>Event Grid</span></li>
            </ul>
        </div>
    </div>
</div>

<h1>Join HBC</h1>
@endsection