<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $site->title }}</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{url('storage/'.$site->favicon)}}">

    <!-- CSS -->
    <link rel="stylesheet" href="business/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="business/assets/css/icons.min.css">
    <link rel="stylesheet" href="business/assets/css/plugins.css">
    <link rel="stylesheet" href="{{url('business/assets/css/style.css')}}">

    <link rel="shortcut icon" href="{{url('storage/'.$site->favicon)}}" type="image/x-icon">
    <link rel="icon" href="{{url('storage/'.$site->favicon)}}" type="image/x-icon">


    <!-- Modernizer JS -->
    <script src="business/assets/js/vendor/modernizr-3.11.7.min.js"></script>
</head>

<body>
    <header class="header-area">
        <div class="header-top bg-img" style="background-image:url(business/assets/img/icon-img/header-shape.png);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-7 col-12 col-sm-8">
                        <div class="header-contact">
                            <ul>
                                <li><i class="fa fa-phone"></i><a href="tel:{{ $site->phone }}">{{ $site->phone }}</a>
                                </li>
                                <li><i class="fa fa-envelope-o"></i><a href="mailto: {{ $site->email }}">{{ $site->email
                                        }}</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
        <div class="header-bottom sticky-bar clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-4">
                        <div class="logo">
                            <a href="{{route('home')}}">
                                <img alt="" src="{{ url( 'storage/' . $site->logo) }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-6 col-8">
                        <div class="menu-cart-wrap">
                            <div class="main-menu">
                                <nav>
                                    <ul>
                                        @foreach($menuItems['top-menu'] as $mitem)
                                        @php
                                        $link = '';
                                        switch($mitem->type) {

                                        case 'post':
                                        $link = route('post.show', $mitem->post->slug);
                                        break;

                                        case 'category':
                                        $link = '#'; // route('post.index', ['category' => @$mitem->category->slug]);
                                        break;

                                        case 'external':

                                        default:
                                        $link = $mitem->link;
                                        break;
                                        }
                                        @endphp

                                        <li>
                                            <a href="{{ $link }}" target="{{ $mitem->target }}">{{ $mitem->label }}</a>
                                            @if(!$mitem->menu_item->isEmpty())
                                            @include('badabusiness.layouts.recursive', ['mitems' => $mitem->menu_item])
                                            @endif
                                        </li>
                                        @endforeach


                                        @php /*
                                        <li><a href="{{route('about')}}"> ABOUT </a></li>
                                        <li class="mega-menu-position
                                            top-hover"><a href="{{route('courses.index')}}">
                                                Courses </a>

                                        <li><a href="{{route('event')}}"> Articles </a>

                                        <li><a href="{{route('contact')}}">
                                                Contact</a></li>
                                        <li><a href="{{route('join')}}"> Join
                                                HBC
                                            </a>
                                        </li>
                                        <li><a href="{{route('policy')}}">
                                                Privacy Policy
                                            </a></li>
                                        */ @endphp
                                    </ul>
                                </nav>

                            </div>
                        </div>
                    </div>
                    <div class="mobile-menu-area">
                        <div class="mobile-menu">
                            <nav id="mobile-menu-active">
                                <ul class="menu-overflow">
                                    @foreach($menuItems['top-menu'] as $mitem)
                                    @php
                                    $link = '';
                                    switch($mitem->type) {

                                    case 'post':
                                    $link = route('post.show', $mitem->post->slug);
                                    break;

                                    case 'category':
                                    $link = '#'; // route('post.index', ['category' => @$mitem->category->slug]);
                                    break;

                                    case 'external':

                                    default:
                                    $link = $mitem->link;
                                    break;
                                    }
                                    @endphp
                                    <li>

                                        <a href="{{ $link }}" target="{{ $mitem->target }}">{{ $mitem->label }}</a>
                                        @if(!$mitem->menu_item->isEmpty())
                                        @include('badabusiness.layouts.recursive', ['mitems' => $mitem->menu_item])
                                        @endif
                                    </li>
                                    @endforeach

                                    @php /*
                                    <li><a href="{{route('about')}}"> ABOUT </a></li>
                                    <li class="mega-menu-position
                                            top-hover"><a href="{{route('courses.index')}}">
                                            Courses </a>

                                    <li><a href="{{route('event')}}"> Articles </a>

                                    <li><a href="{{route('contact')}}">
                                            Contact</a></li>
                                    <li><a href="{{route('join')}}"> Join
                                            HBC
                                        </a>
                                    </li>
                                    <li><a href="{{route('policy')}}">
                                            Privacy Policy
                                        </a></li>
                                    */ @endphp
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
    </header>
    @yield('main_section')

    @php
    $about = \App\Models\PostData::getPostRow('about-us');
    @endphp
    <footer class="footer-area">
        <div class="footer-top bg-img default-overlay pt-130 pb-80"
            style="background-image:url(business/assets/img/bg/bg-4.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget mb-40">
                            <div class="footer-title">
                                <h4>{{$about->title}}</h4>
                            </div>
                            <div class="footer-about">
                                <p>{{$about->excerpt}}</p>
                                <div class="f-contact-info">
                                    <div class="single-f-contact-info">
                                        <i class="fa fa-home"></i>
                                        <span><a href="{{$site->address}}">{{$site->address}}</a></span>
                                    </div>
                                    <div class="single-f-contact-info">
                                        <i class="fa fa-envelope-o"></i>
                                        <span><a href="mailto: {{ $site->email }}">{{ $site->email }}"</a></span>
                                    </div>
                                    <div class="single-f-contact-info">
                                        <i class="fa fa-phone"></i>
                                        <span> <a href="tel:{{ $site->phone }}">{{ $site->phone }}</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="footer-widget mb-40">
                            <div class="footer-title">
                                <h4>QUICK LINK</h4>
                            </div>
                            <div class="footer-list">
                                <ul>
                                    @foreach($menuItems['footer-1'] as $mitem)
                                    @php
                                    $link = '';
                                    switch($mitem->type) {

                                    case 'post':
                                    $link = route('post.show', $mitem->post->slug);
                                    break;

                                    case 'category':
                                    $link = route('post.index', ['category' => @$mitem->category->slug]);
                                    break;

                                    case 'external':

                                    default:
                                    $link = $mitem->link;
                                    break;
                                    }
                                    @endphp
                                    <li>
                                        <a href="{{ $link }}">{{ $mitem->label }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="footer-widget negative-mrg-30
                                        mb-40">
                            <div class="footer-title">
                                <h4>COURSES</h4>
                            </div>
                            <div class="footer-list">
                                <ul>
                                    @foreach($menuItems['footer-2'] as $mitem)
                                    @php
                                    $link = '';
                                    switch($mitem->type) {

                                    case 'post':
                                    $link = route('post.show', $mitem->post->slug);
                                    break;

                                    case 'category':
                                    $link = route('post.index', ['category' => @$mitem->category->slug]);
                                    break;

                                    case 'external':

                                    default:
                                    $link = $mitem->link;
                                    break;
                                    }
                                    @endphp
                                    <li>
                                        <a href="{{ $link }}">{{ $mitem->label }}</a>
                                    </li>
                                    @endforeach
                                    <!-- <li><a href="#">Under Graduate
                                            Programmes </a></li>
                                    <li><a href="#">Graduate
                                            Programmes
                                        </a></li>
                                    <li><a href="#">Diploma Courses</a></li>
                                    <li><a href="#">Others
                                            Programmes</a></li>
                                    <li><a href="#">Short Courses</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget mb-40">
                            <div class="footer-title">
                                <h4>NewsLetter</h4>
                            </div>
                            <div class="subscribe-style">
                                <p>Dugiat nulla pariatur. Edeserunt
                                    mollit
                                    anim id est laborum. Sed ut
                                    perspiciatis
                                    unde</p>
                                <div id="mc_embed_signup" class="subscribe-form">
                                    <form id="mc-embedded-subscribe-form" class="validate" novalidate="" target="_blank"
                                        name="mc-embedded-subscribe-form" method="post"
                                        action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                                        <div id="mc_embed_signup_scroll" class="mc-form">
                                            <input class="email" type="email" required="" placeholder="Your
                                                            E-mail
                                                            Address" name="EMAIL" value="">
                                            <div class="mc-news" aria-hidden="true">
                                                <input type="text" value="" tabindex="-1"
                                                    name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                                            </div>
                                            <div class="clear">
                                                <input id="mc-embedded-subscribe" class="button" type="submit"
                                                    name="subscribe" value="SUBMIT">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="footer-bottom pt-15 pb-15">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-12">
                        <div class="copyright">
                            <p>
                                Copyright &copy;
                                <a href="{{route('home')}}">{{ $site->title }}</a>
                                . All Right Reserved.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="footer-menu-social">
                            <div class="footer-menu">
                                <ul>
                                    @foreach($menuItems['footer-3'] as $mitem)
                                    @php
                                    $link = '';
                                    switch($mitem->type) {

                                    case 'post':
                                    $link = route('post.show', $mitem->post->slug);
                                    break;

                                    case 'category':
                                    $link = route('post.index', ['category' => @$mitem->category->slug]);
                                    break;

                                    case 'external':

                                    default:
                                    $link = $mitem->link;
                                    break;
                                    }
                                    @endphp
                                    <li>
                                        <a href="{{ $link }}">{{ $mitem->label }}</a>
                                    </li>
                                    @endforeach
                                    <!-- <li><a href="#">Privacy & Policy</a></li>
                                    <li><a href="#">Terms & Conditions of Use</a></li> -->
                                </ul>
                            </div>
                            <!-- <div class="footer-social">
                                <ul>
                                    <li><a class="facebook" href="#"><i class="fa
                                                            fa-facebook"></i></a></li>
                                    <li><a class="youtube" href="#"><i class="fa
                                                            fa-youtube-play"></i></a></li>
                                    <li><a class="twitter" href="#"><i class="fa
                                                            fa-twitter"></i></a></li>
                                    <li><a class="google-plus" href="#"><i class="fa
                                                            fa-google-plus"></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>













    <!-- JS
============================================ -->
    <!-- jQuery JS -->
    <script src="business/assets/js/vendor/jquery-v2.2.4.min.js"></script>
    <!-- Popper JS -->
    <script src="business/assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="business/assets/js/bootstrap.min.js"></script>
    <!-- Plugins JS -->
    <script src="business/assets/js/plugins.js"></script>
    <!-- Ajax Mail -->
    <script src="business/assets/js/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="business/assets/js/main.js"></script>
</body>

</html>