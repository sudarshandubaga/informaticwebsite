@extends('vyom.layouts.app')

@section('website_content')

                <!-- Page Banner Section Start -->
                <section class="page_banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pb_content text-center">
                                    <h2>About-us</h2>
                                    <p><a href="/">Home</a><b>-</b><span>About us</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Page Banner Section End -->
            
                <!-- about-royal-finserve-start -->
                    <section>
                            <div class="container">
                                   <div class="royal-finserve">
                                       <h2>About Royal Finserv</h2>
                                       <p>  
                                        Royal Finserv Consultants Pvt. Ltd.(RFCPL)is a corporate Sales Channel & comprehensive financial planning consultancy. RFCPL's services are comprehensive in terms of offered products (PL, BL, HL, LAP, Credit Cards, Gold Loan, Auto Loans, LAS, General Insurance , Life Insurance etc.) and depth of research. Although it costs a fair amount of money for RFCPL to do an in-depth amount of research into prospective investments as well as possible options for the client.
                                       </p>
                                       <p>
                                        Royal Finserv Consultants Pvt. Ltd has presence across India with more than 450+ branches. RFCPL will be generating new clients through a combination of networking and monthly travel plans that introduces otherwise unreachable segments of the population.
                                       </p>
                                       <p>
                                        Royal Finserv Consultants Pvt. Ltd.is a comprehensive financial planning service provider that has branches across India.
                                       </p>
                                       <p>
                                        RFCPL is a Private Limited Co. managed by two directors, Mr. Bhasker Dobriyal & Mrs. Riya Chakravarty and headed by Aditya Srivastava(CEO).
                                       </p>
                                       <p>
                                        RFCPL will offer comprehensive financial planning for all segment of customers in all corners of the country.
                                       </p>
                                           
                                   </div>

                            </div>
                    </section>
                <!-- about-royal-finserve-end -->

@endsection