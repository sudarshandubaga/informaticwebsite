@extends('vyom.layouts.app')

@section('website_content')



{{-- <div class="carousel-item">
    <img src="vyom/images-2/slider-1.jpg"  alt="">
    <div class="carousel-caption d-none d-md-block">
      <h5>...</h5>
      <p>...</p>
    </div>
  </div> --}}

  
@php
$slider = \App\Models\PostData::getPostData('slider');
@endphp

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($slider as $i => $s)
      <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{$i===0 ? 'active' : null}}"></li>
      @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach($slider as $i => $s)
        <div class="carousel-item {{$i===0 ? 'active' : null}}">
            <img src="{{ url($site->domain . '/images/slider/' . $s->image) }}"  alt="">
            <div class="carousel-caption d-none d-md-block">
              <h5 class="silder-title">{{$s->title}}</h5>
              <p>{{$s->excerpt}}</p>
              <div class="slider-btn mb-4">
               <a class="animated btn-default btn-white-color" href="{{ route('post.show','contact') }}">Contact</a>
              </div>
            </div>
        </div>
          @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

        @php
        $about = \App\Models\PostData::getPostRow('about-us');
        @endphp

        <!-- About US Section Start -->
        <section class="about_us_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="ab_images">
                            <img src="{{ url($site->domain . '/images/page/' . $about->image) }}" alt="" style="width: 470px; height:420px;"/>
                     <a data-rel="lightcase" href="https://www.youtube.com/embed/{{ $about->getMetaValue('video_id') }}" class="vp_btn light_pops">
                        Play<span><i class="bx bx-play"></i>
                        </span></a>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6 noPaddingRight">
                        <div class="ab_content">
                            <h5>{{$about->title}}</h5>
                            <h2>
                    <a href="{{ route('post.show', $about->slug) }}">
                        {{$about->excerpt}}</a>
                            </h2>
                            <div class="dvd_bar"></div>
                            <p>
                               {!! $about->description !!} 
                            </p>
                            {{-- <ul class="list_ul">
                                <li>Trowels & Planting Tools</li>
                                <li>So easy, anyone can do it</li>
                                <li>Trugs & Harvest Baskets</li>
                                <li>Low weed maintenance</li>
                                <li>Weeders & Cultivators</li>
                                <li>No fertilizer needed</li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About US Section End -->

        @php
        $factloan = \App\Models\PostData::getPostData('fact-loan');
        @endphp
       
        <!-- Fact Section Start -->
        <section class="fact_section">
             <div class="container">
                <div class="row mb-5 text-center">
                    <div class="col-lg-12">
                        <div class="hr_row"></div>
                    </div>
                    @foreach($factloan as  $fl)
                    <div class="col-md-6 col-lg-3">
                        <div class="fact_01 mb-5">
                            <img src="{{ url($site->domain . '/images/fact-loan/thumbnail/' . $fl->image) }}" alt="" class="mb-3" style="width: 44px;">
                            <h2 >{{$fl->excerpt}}<sup>*</sup>%</h2>
                            <h5>{{$fl->title}}</h5>
                        </div>
                    </div>
                    @endforeach
                    {{-- <div class="col-md-6 col-lg-3">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/loan.svg" alt="" class="mb-3">
                            <h2>11.25<sup>*</sup>%</h2>
                            <h5>Personal Loan</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/car.svg" alt="" class="mb-3">
                            <h2>9.00<sup>*</sup>%</h2>
                            <h5>Car Loan</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/loan.svg" alt="" class="mb-3">
                            <h2>16.00<sup>*</sup>%</h2>
                            <h5>Buseness Loan</h5>
                        </div>
                    </div> --}}
                    {{-- <div class="col-lg-12">
                        <div class="hr_row"></div>
                    </div> --}}
                    {{-- <div class="col-md-6 col-lg-4">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/loan.svg" alt="" class="mb-3">
                            <h2 >10.00<sup>*</sup>%</h2>
                            <h5>Loan Against Property</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/car (1).svg" alt="" class="mb-3">
                            <h2>10.00<sup>*</sup>%</h2>
                            <h5>Small And Medium Enterprises</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="fact_01">
                            <img src="vyom/images-2/home-loan-icon/mortgage.svg" alt="" class="mb-3">
                            <h2>9.25<sup>*</sup>%</h2>
                            <h5>Auto Loan</h5>
                        </div>
                    </div> --}}
                    <div class="col-lg-12">
                        <div class="hr_row"></div>
                    </div>
                </div>
                <!-- copy-fact-end -->
            </div>
        </section>
        <!-- Fact Section End -->

        @php
        $loan = \App\Models\PostData::getPostData('loan', [
            'conditions' => 'post_id IS NULL'
        ]);
        @endphp
        <!-- Find Loan Products Start -->
        <section class="service_section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="sec_title">Find Loan Products We Offers</h2>
                        <h5 class="sub_title">We will match you with a loan program that meet your financial need. In short term liquidity, by striving to make funds available to them <strong>within 24 hours of application.</strong> </h5>
                    </div>
                </div>
                <div class="row mt-5 ">
                    <div class="col-lg-12 noPadding">
                        <div class="service_01_slider owl-carousel">
                            @foreach($loan as $i => $l)
                            <div class="service_01">
                                <div class="sr01_thumb">
                                    
                                </div>
                                <div class="sr01_dtl text-center">
                                    <i class="flaticon-202-piggy-bank"></i>
                                    <h3><a href="single_service.html">{{$l->title}}</a></h3>
                                    <a class="learn_more_lnk" href="{{ route('post.show', $l->slug) }}">Read More</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="notes">
                            We provide new ways to manage your businesses <a href="{{ route('post.show','contact') }}"><span>Request your Quote</span><i class="bx bx-right-arrow-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Find Loan Products End -->

        <!-- application process start -->
           <section>
                <div class="container Application-Process">
                       <h3>Fast & Easy Application Process.</h3>
                          <div class="row ">
                                  <!-- --- -->
                                  <div class="content">

                                      <div class="col-lg-4 appication-card">
                                             <h4>Choose Loan Amount</h4>
                                             <h6>Choose the loan amount according to your need.</h6>
                                           <div class="overlay-number">
                                               <i class="fa-solid fa-1"></i>
                                           </div>
                                          
                                      </div>
                                         <!-- --- -->
       
                                         <!-- --- -->
                                      <div class="col-lg-4 appication-card">
                                       <h4>Approved Your Loan</h4>
                                       <h6>We approve your loan upfront and provide it soon.</h6>
                                     <div class="overlay-number">
                                         <i class="fa-solid fa-2"></i>
                                     </div>
                                    
                                </div>
                                   <!-- --- -->
       
                                   <!-- --- -->
                                   <div class="col-lg-4 appication-card">
                                       <h4>Get Your Cash</h4>
                                       <h6>We provide your loan as soon as possible.</h6>
                                     <div class="overlay-number">
                                         <i class="fa-solid fa-3"></i>
                                     </div>
                                    
                                </div>
                                   <!-- --- -->
                                  </div>


                          </div>
                </div>
           </section>
        <!-- application process end -->

        
        <!-- Features Section Start -->
        <!-- why people choose us start-->
        <section class="features_section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h5 class="sub_title">Our Features</h5>
                        <h2 class="sec_title lights">Why People Choose Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="icon_box_02 text-center">
                            <div class="ib02_icon">
                                <i class="fa-solid fa-people-group"></i>
                            </div>
                            <h3>Dedicated Specialists</h3>
                            <p>
                                We meet you with our specialist dedicated team who can explain to you in full.
                            </p>
                            <a href="{{ route('post.show','businesspartner') }}" class="learn_more_lnk">Meet The Team</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="icon_box_02 text-center">
                            <div class="ib02_icon">
                                <i class="fa-solid fa-trophy"></i>
                            </div>
                            <h3>Success Stories Rating</h3>
                            <p>
                                We do not tell about our success stories, our client say that our good work is.
                            </p>
                            <a href="#testimonial" class="learn_more_lnk">View Client Review</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="icon_box_02 text-center">
                            <div class="ib02_icon">
                                <i class="fa-solid fa-calculator"></i>
                            </div>
                            <h3>No front Appraisal Fees!</h3>
                            <p>
                                We do not take any appraisal fees from our client, we don't take the hidden charge of any method.
                            </p>
                            <a href="{{ route('post.show','about-us') }}" class="learn_more_lnk">Why Choose Us</a>
                        </div>
                    </div>
                    
                </div>
                <div class="row pdt30">
                    <div class="col-lg-12 text-center">
                        <div class="notes lights">
                            We consider all the drivers of change – from the ground up <a href="{{ route('post.show','contact') }}"><span>Request your Quote</span><i class="bx bx-right-arrow-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- why people choose us end-->
        <!-- Features Section End -->

        

        <!-- Quote Section Start -->
        <section class="quote_section" id="request_quote_section">
            <div class="container">
                @if(\Session::has('success'))
            <p class="alert alert-success">{{ \Session::get('success') }}</p>
            @endif
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h5 class="sub_title lights">Get a Quote</h5>
                        <h2 class="sec_title lights">Request a Free Quote Today</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="quote_form">
                            <form method="post" action="#">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <input type="text" name="q_fname" placeholder="First Name *"/>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <input type="text" name="q_lname" placeholder="Last Name *"/>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <input type="email" name="q_email" placeholder="Your Email *"/>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <input type="text" name="q_phone" placeholder="Your Phone *"/>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <select name="q_type">
                                            <option value="">I Would Like to Discuss</option>
                                            <option value="1">It's About Mutual Fund Investment</option>
                                            <option value="2">Bank Loan Interest</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <button type="submit" name="q_submit"><span>Request a Quote<i class="bx bx-right-arrow-alt"></i></span></button>
                                        <div class="form_note text-right">
                                            Join Now! With <span>50% Discount</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @php
        $ourpartners = \App\Models\PostData::getPostData('our-partners');
        @endphp
                <div class="row">
                    <div class="col-lg-12">
                        <h5 class="client_notes text-center">Our Partners</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="client_slider owl-carousel">
                            @foreach($ourpartners as $i => $op)
                            <div class="cs_item">
                                <a href="#">
                                    <img src="{{ url($site->domain . '/images/our-partners/' . $op->image) }}" alt="   "/>
                                </a>
                            </div>
                            @endforeach
                            {{-- <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled2.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled3.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled3.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled4.png" alt=""/>
                                </a>
                            </div>                          
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled6.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled7.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled9.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled10.png" alt=""/>
                                </a>
                            </div>
                            <div class="cs_item">
                                <a href="#">
                                    <img src="vyom/images-2/clients/untitled11.png" alt=""/>
                                </a>
                            </div> --}}
                            <!-- ---------- -->

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Quote Section End -->
@php
$testimonial = \App\Models\PostData::getPostData('testimonial');
@endphp
        <!-- Testimonial Section Start -->
        <section  id ="testimonial" class="testimonial_section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h5 class="sub_title">Testimonials</h5>
                        <h2 class="sec_title">Some of our Awesome Testimonials</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 noPadding">
                        @foreach($testimonial as $i => $t)
                        <div class="testimonial_slider_01 owl-carousel">
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="{{ url($site->domain . '/images/testimonial/' . $t->image) }}" alt=""/>
                                    <h3>{!! $t->description !!}</h3>
                                    <p>{{$t->title}}</p>
                                </div>
                                <p>{{$t->excerpt}}</p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                         @endforeach
                            {{-- <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images-2/testimonil-boy-img.jpg" alt=""/>
                                    <h3>Rahul Kumar </h3>
                                    <p>PUNE</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images-2/testimonil-boy-img.jpg" alt=""/>
                                    <h3>Saurabh Bajpai</h3>
                                    <p>Lucknow</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/4.jpg" alt=""/>
                                    <h3>Cooper Moore</h3>
                                    <p>Payoneer Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/5.jpg" alt=""/>
                                    <h3>Dan Boyle</h3>
                                    <p>Codecanyon Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/6.jpg" alt=""/>
                                    <h3>Arnaldo Kozey</h3>
                                    <p>Behance Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/7.jpg" alt=""/>
                                    <h3>Mike Hardson</h3>
                                    <p>Kopalpora Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/8.jpg" alt=""/>
                                    <h3>Fae Hoppe</h3>
                                    <p>Kamla Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div>
                            <div class="testimonial_01 text-center">
                                <div class="t01_head">
                                    <img src="vyom/images/testimonial/9.jpg" alt=""/>
                                    <h3>Kalia Fince</h3>
                                    <p>Master Inc.</p>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <div class="t01_rate">
                                    <i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i><i class="bx bx-star"></i>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Testimonial Section End -->


@endsection
