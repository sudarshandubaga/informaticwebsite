@extends('vyom.layouts.app')

@section('website_content')

                <!-- Page Banner Section Start -->
                <section class="page_banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pb_content text-center">
                                    <h2>{{$post->title}}</h2>
                                    <p><a href="/">Home</a><b>-</b><span>{{$post->title}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Page Banner Section End -->
            
                <!-- about-royal-finserve-start -->
                    <section>
                            <div class="container">
                                   <div class="royal-finserve">
                                       <h2>{{$post->title}}</h2>
                                       <p>{{$post->excerpt}}</p>
                                       <p class="mt-2">{!! $post->description !!}</p>
                                   </div>
                            </div>
                    </section>
                <!-- about-royal-finserve-end -->

@endsection