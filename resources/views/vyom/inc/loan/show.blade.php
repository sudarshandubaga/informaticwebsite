@extends('vyom.layouts.app')

@section('website_content')
@php
$loan = \App\Models\PostData::getPostData('loan', [
    'conditions' => 'post_id IS NULL'
]);
@endphp
    <!-- Page Banner Section Start -->
    <section class="page_banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pb_content text-center">
                        <h2>{{$post->title}}</h2>
                        <p><a href="/">Home</a><b>-</b><span>About Us</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Banner Section end -->

          <!-----------------
            my-loan-start
          ------------------>

              <!--
                =========================
                about-personal-loan-start
                =========================
               -->
               
        @php
        $loans = \App\Models\PostData::getPostData('loan', [
            'conditions' => 'post_id = ' . $post->id
        ]);
        // dd($loans);
        @endphp

    <section>
        <div class="container">
            <div class="row g-0 my-5">

                <!--  -->
                <div class="col-lg-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach($loans as $i => $l)
                        <a class="nav-link {{$i==0 ? 'active' : null}}" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-{{ $l->slug }}"
                            role="tab" aria-controls="v-pills-home" aria-selected="true"> 
                            <i class="fa-solid fa-magnifying-glass mr-2"></i>{{$l->title}}
                        </a>
                        @endforeach
                        {{-- <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">
                            <i class="fa-solid fa-star mr-2"></i>How We Can Help?
                        </a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                            role="tab" aria-controls="v-pills-messages" aria-selected="false"><i
                                class="fa-solid fa-square-check mr-2"></i>Eligibility Criteria
                        </a> --}}
                        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
                            role="tab" aria-controls="v-pills-settings" aria-selected="false"><i
                                class="fa-solid fa-mug-saucer mr-2"></i>Apply Online</a>
                    </div>
                </div>
                <!--  -->

                <!--  -->
                <div class="col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        @foreach($loans as $i => $l)
                        <div class="tab-pane fade show {{$i==0 ? 'active' : null}}" id="v-pills-{{ $l->slug }}" role="tabpanel"
                            aria-labelledby="v-pills-home-tab">
                            <!-- overview-content-start -->
                            <div class="about-pl">
                                {!! $l->description !!}
                                {{-- <ul>
                                    <li>
                                        Personal loans are usually “Unsecured Loans”, which means they are offered
                                        without any collateral
                                        in return. Such loans are mainly based on the credit history of the customer who
                                        hasn’t defaulted on
                                        his/her credit card payments.
                                    </li>

                                    <li>
                                        The interest rates of Personal Loan are allotted based on the salary of the
                                        customer, amount being borrowed and credit history, amongst others
                                    </li>

                                    <li>
                                        A short-tenure loan, the repayment period are usually of the time period of 1-5
                                        years
                                    </li>

                                    <li>
                                        Requires the least bit of paper-work and is usually approved within 48 hours of
                                        application submission
                                    </li>

                                    <li>
                                        Additional to the processing fee, charges for late payment penalty, pre-payment
                                        fee, administration charges and cheque bounce charges are also included in the
                                        personal loan
                                    </li>
                                </ul> --}}

                                <div class="d-flex">
                                    <div>
                                        <button type="button" class="btn btn-dark mr-3">Call</button>
                                    </div>
                                    <div>
                         <button type="button" class="btn btn-dark">Mail Us</button>
                                    </div>
                                </div>
                            </div>
                            <!-- overview-content-end -->
                        </div>
                        @endforeach
                        {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <!-- how-can-we-content-start -->
                            <div class="about-pl">
                                <h3>How we can help?</h3>
                                <p>As one of the leading loan & finance Compant in India. Royal Finserv provides you
                                    with an unbiased platform to analyze and decide upon the available deals in the
                                    market.
                                </p>
                                <h5>How do we help you borrow right?
                                </h5>
                                <ul>
                                    <li>
                                        We offer a customized loan consulting experience to each customer
                                    </li>

                                    <li>
                                        Our experienced loan consultants help you to understand which is the right loan
                                        for you
                                    </li>

                                    <li>
                                        We make sure that your documents are in order
                                    </li>

                                    <li>
                                        We make the process smooth, efficient and easy
                                    </li>

                                    <li>
                                        We take care of any issues on the way
                                    </li>
                                </ul>



                                <h5>Why Royal Finserv</h5>
                                <ul>
                                    <li>
                                        get best offers & deals on from our partners banks & NBFCs.
                                    </li>

                                    <li>
                                        get best offers & deals on from our partners banks & NBFCs.
                                    </li>

                                    <li>
                                        Apply with the best bank or NBFC.
                                    </li>

                                    <li>
                                        Lowest Rate of Interest in offering.
                                    </li>

                                    <li>
                                        Hassle free process and easy documentation.
                                    </li>
                                    <li>
                                        Free Document Pickup by Royal Finserv Team.
                                    </li>
                                    <li>
                                        Free expert guidance & advice from our dedicated team of experts.
                                    </li>
                                    <li>
                                        We are not an agent of any bank and you can apply with the bank of your choice.
                                    </li>
                                    <li>
                                        No Fees for services provided.
                                    </li>
                                    <li>
                                        We are here to help & assit you .We understand that it’s an important decision
                                        for you as you are in immediate need of money; and trust us we will ensure quick
                                        processing and disbursal of loan.
                                    </li>
                                </ul>
                                <h5>Point To Be Considered Before Taking A Personal Loan:</h5>
                                <ul>
                                    <li>Share all the accurate information at time of filling up of application form.
                                        Bank will process your loan based the information you provide</li>
                                    <li>Processing fees are deducted from Loan amount and thus do not pay any fees in
                                        cash/cheque to anyone to process your loan sanctioning
                                    </li>
                                    <li>
                                        Compare Loan offers & deals but handover document to any one of the bank of your
                                        choice
                                    </li>
                                </ul>

                                <div class="d-flex">
                                    <div>
                                        <button type="button" class="btn btn-dark mr-3">Call</button>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-dark">Mail Us</button>
                                    </div>
                                </div>
                            </div>
                            <!-- how-can-we-content-end -->
                        </div>

                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                            aria-labelledby="v-pills-messages-tab">
                            <!-- eligibility-content-start -->
                            <div class="about-pl">
                                <h3>Eligibility Criteria of Personal Loan</h3>
                                <ul>
                                    <li>
                                        Any Indian citizen in good standing who is salaried, self-employed or business
                                        person with regular source of income can apply for a personal loan.
                                    </li>

                                    <li>
                                        The applicant should be above the age of 24 years
                                    </li>

                                    <li>
                                        Should be currently employed with existing organization or been involved in your
                                        business for a specific number of years.
                                    </li>

                                    <li>
                                        Professional stability and savings history play a major role in approval of the
                                        loan, especially minimum required monthly salary and repaying capacity
                                    </li>

                                    <li>
                                        Bad credit history would prove to be a put-off, especially anytime within 3
                                        months prior to applying for personal loan.
                                    </li>
                                </ul>

                                <div class="d-flex">
                                    <div>
                                        <button type="button" class="btn btn-dark mr-3">Call</button>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-dark">Mail Us</button>
                                    </div>
                                </div>
                            </div>
                            <!-- eligibility-content-end -->
                        </div> --}}

                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                            aria-labelledby="v-pills-settings-tab">
                            <!-- Apply-online-content-start -->
                            <div class="about-pl">
                                <h3>About Personal Loan</h3>
                                <form action="">

                                    <!--1  -->
                                    <div class="row ">

                                        <div class="col-lg-4">
                                            <label for="fname">First Name</label>
                                            <input type="text" id="fname" name="fname" placeholder="First Name">
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Middle Name</label>
                                            <input type="text" id="fname" name="fname"  placeholder="Middle Name">
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Last Name</label>
                                            <input type="text" id="fname" name="fname" placeholder="Last Name">
                                        </div>

                                    </div>
                                    <!-- 1 -->

                                    <!--  2-->
                                    <div class="row mt-4">

                                        <div class="col-lg-4">
                                            <label for="fname">Your Email</label>
                                            <input type="text" id="fname" name="fname" placeholder="Your Email"> 
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Your Number</label>
                                            <input type="text" id="fname" name="fname" placeholder="Your Number">
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Resendency Type</label>
                                            <input type="text" id="fname" name="fname" placeholder="Resendency Type">
                                        </div>

                                    </div>
                                    <!--  2-->

                                    <!--  3-->
                                    <div class="row mt-4">

                                        <div class="col-lg-4">
                                            <label for="fname">Current EMI</label>
                                            <input type="text" id="fname" name="fname " placeholder="Current EMI">
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <label for="fname">Loan Tenure</label>
                                            <input type="text" id="fname" name="fname" placeholder="Loan Tenure">
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <label for="fname">No.Of Paid EMI</label>
                                            <input type="text" id="fname" name="fname" placeholder="No.Of Paid EMI">
                                        </div>
                                    
                                    </div>
                                    <!-- 3 -->

                                    <!-- 4 -->
                                       <div class="row mt-4">
                                            <div class="col-lg-6">
                                                <label for="ltype">Loan Type</label>
                                                <input type="text" id="ltype" name="ltype" placeholder="Loan Type">
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="ltype">Loan Item</label>
                                                <input type="text" id="ltype" name="ltype " placeholder="Loan Item">
                                            </div>
                                       </div>
                                    <!-- 4 -->

                                    <!-- 5 -->
                                    <div class="row mt-4">

                                        <div class="col-lg-4">
                                            <label for="fname">Address 1</label>
                                            <input type="text" id="fname" name="fname" placeholder="Address1">
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Address 2</label>
                                            <input type="text" id="fname" name="fname" placeholder="Address 2">
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="fname">Landmark</label>
                                            <input type="text" id="fname" name="fname" placeholder="Landmark">
                                        </div>

                                    </div>
                                    <!-- 5 -->

                                    <!-- 6 -->
                                    <div class="row mt-4">

                                        <div class="col-lg-4">
                                            <label for="fname">City</label>
                                            <input type="text" id="fname" name="fname" placeholder="City">
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <label for="fname">State</label>
                                            <input type="text" id="fname" name="fname" placeholder="State">
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <label for="fname">Postal Code</label>
                                            <input type="text" id="fname" name="fname" placeholder="Postal Code">
                                        </div>
                                    
                                    </div>
                                    <!-- 6 -->
                                </form>
                                <button type="button" class="btn btn-dark mr-3 mt-4 px-5">Submit</button>
                            </div>
                            <!-- Apply-online-content-end -->

                        </div>
                    </div>
                </div>
                <!--  -->

            </div>
        </div>
    </section>
              <!--
                =========================
                about-personal-loan-start
                =========================
               -->
               
          <!-----------------
              my-loan-end
          ------------------>


@endsection