@extends('vyom.layouts.app')

@section('website_content')

@php
$blacklisted = \App\Models\PostData::getPostData('blacklisted', ['limit' => 1]);
// dd($blacklisted);
@endphp
        <!--black-listed-start  -->
        <section>
               <div class="container-fluid">

                     <div class="blacklisted-table my-5">
                        <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">S.N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Location</th>
                                <th scope="col">Contact</th>
                                <th scope="col">Email Id</th>
                                <th scope="col">Department	</th>
                                <th scope="col">Photo</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($blacklisted as $i => $bl)
                              <tr>
                                <th scope="row">{{ $i + $blacklisted->firstItem() }}</th>
                                <td>{{$bl->title}}</td>
                                <td>{{$bl->excerpt}}</td>
                                <td>{{ $bl->getMetaValue('contact') }}</td>
                                <td>{{ $bl->getMetaValue('email') }}</td>
                                <td>{{ $bl->getMetaValue('department') }}</td>
                                <td><img src="{{ url($site->domain . '/images/'.$bl->post_type.'/thumbnail/'.$bl->image) }}" alt=""></td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                {{ $blacklisted->links("pagination::bootstrap-4") }}
                              
                     </div>

               </div>
         </section>  
        <!--black-listed-end  -->

@endsection