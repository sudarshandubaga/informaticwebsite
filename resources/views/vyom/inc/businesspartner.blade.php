@extends('vyom.layouts.app')

@section('website_content')

    <!-- Page Banner Section Start -->
    <section class="page_banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pb_content text-center">
                        <h2>BUSINESS PARTNERS, PROMOTERS & OTHER TEAM MEMBERS</h2>
                        <p><a href="/">Home</a><b>-</b><span>{{$post->title}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Banner Section end -->


          <!-----------------
            my-loan-start
          ------------------>

              <!--
                =========================
                about-personal-loan-start
                =========================
       
            -->

            @php
            $bussinesspartner = \App\Models\PostData::getPostData('bussinesspartner');
            @endphp

    <section>
        <div class="container">
            <div class="row g-0 my-5">

                <!--  -->
                <div class="col-lg-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach($bussinesspartner as $i => $bp)
                        <a class="nav-link {{$i==0 ? 'active' : null}}"  data-toggle="pill" href="#v-pills-{{ $bp->slug }}"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">
                            <i class="fa-solid fa-star mr-2"></i>{{$bp->title}}
                        </a>
                        @endforeach
                    </div>
                </div>
                <!--  -->

                <!--  -->
                <div class="col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        @foreach($bussinesspartner as $i => $bp)
                        <div class="tab-pane fade show {{$i==0 ? 'active' : null}}" id="v-pills-{{ $bp->slug }}" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <!-- how-can-we-content-start -->
                            <div class="about-pl">
                                <div class="row">
                                    <div class="col-sm-3 person">
                                        <img src="{{ url($site->domain . '/images/bussinesspartner/' . $bp->image) }}" alt="">
                                    </div>
    
                                    <div class="col-sm-9">
                                        <h3>{{$bp->title}}</h3>
                                        <p>{!! $bp->excerpt !!}</p>
                                    </div>
                                </div>
                                <div class="d-flex mt-4">
                                    <div class="new">
                                        <button type="button" class="btn btn-dark mr-3">
                                        <a href="tel:{{ $bp->getMetaValue('contact') }}">Call</a>
                                        </button>
                                    </div>
                                    <div class="new">
                                        <button type="button" class="btn btn-dark">
                                        <a href="mailto:{{ $bp->getMetaValue('email') }}">Mail Us</a>
                                            </button>
                                    </div>
                                </div>
                            </div>
                            <!-- how-can-we-content-end -->
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--  -->

            </div>
        </div>
    </section>
              <!--
                =========================
                about-personal-loan-start
                =========================
               -->
               
          <!-----------------
              my-loan-end
          ------------------>


@endsection