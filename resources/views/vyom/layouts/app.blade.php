<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ $site->title }}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Include All CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/flaticon.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/boxicons.css')}}"/>

        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/owl.carousel.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/owl.theme.default.min.css')}}"/>

        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/lightcase.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/settings.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/animate.css')}}"/>

        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/preset.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/ignore_for_wp.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/theme.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('vyom/css/responsive.css')}}"/>
        <link rel="stylesheet" href="{{url('vyom/css/gaurav.css')}}">

        <!-- Favicon Icon -->
        <link rel="icon"  type="image/png" href="{{url('vyom/images/favicon.png')}}">
        <!-- Favicon Icon -->

        <!-- font-awesom-start -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!-- font-awesom-end -->
    </head>

    <body>

        <!-- scroling text start -->
        <marquee>
            ”Royal Finserv or its associate will never demand any money for getting the loan processed/passed. Any person/institution doing so will be solely doing at their own risk. Royal Finserv will not be responsible for this act.”
        </marquee>
        <!-- scroling text end -->

        <!-- Preloader Start -->
        <div class="preloader">
            <div class="la-ball-scale-multiple la-2x">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- Preloader Start -->
        
        <!-- Top Bar Start -->
        <section class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="info_item">
                            <i class="bx bx-support"></i>
                            <h5>Free Call:</h5>
                            <h6 class="active tel:{{ $site->phone }}">{{ $site->phone }}</h6>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="info_item">
                            <i class="bx bx-at"></i>
                            <h5>Email Us:</h5>
                            <h6><a href="mailto: {{ $site->email }}">{{ $site->email }}</a></h6>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3 noPaddingRight">
                        <div class="info_item">
                            <i class="bx bxs-map"></i>
                            <h5>Visit Us:</h5>
                            <h6>{{$site->address}}</h6>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="bar_btns">
                            <a href="#request_quote_section" class="req_btn scroll_to_btn"><span>Request a Quote</span><i class="bx bx-right-arrow-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Top Bar End -->

        <!-- Header 01 Start -->
        <header class="header_01 isSticky">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-3">
                        <div class="logo_01 finserve-logo">
                            <a href="index.html">
                                <img src="{{ url('storage/' . $site->logo) }}"
                                alt="Logo of {{ $site->title }}" alt="Finserve"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-9">
                        <nav class="menu_1">
                            <div class="menuButton">
                                <a href="">Menu<i class="bx bx-menu"></i></a>
                            </div>
                            <ul>
                                @foreach($menuItems['top-menu'] as $mitem)
                                @php
                                $link = '';
                                switch($mitem->type) {

                                case 'post':
                                $link = route('post.show', $mitem->post->slug);
                                break;

                                case 'category':
                                $link = '#'; // route('post.index', ['category' => @$mitem->category->slug]);
                                break;

                                case 'external':

                                default:
                                $link = $mitem->link;
                                break;
                                }
                                @endphp

                                <li>
                                    <a href="{{ $link }}" target="{{ $mitem->target }}">{{ $mitem->label }}</a>
                                    @if(!$mitem->menu_item->isEmpty())
                                    @include('vyom.layouts.recursive', ['mitems' => $mitem->menu_item])
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div class="blanks"></div>
        <!-- Header 01 End -->

        <!-- Popup Search Start -->
        <section class="popup_search_sec">
            <div class="popup_search_overlay"></div>
            <div class="pop_search_background">
                <div class="middle_search">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <div class="popup_search_form">
                                    <form method="get" action="#">
                                        <input type="search" name="s" id="s" placeholder="Type Words and Hit Enter">
                                        <button type="submit"><i class="bx bx-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Popup Search End -->

<!-- ---------------------common--header----------------------------------- -->

@yield('website_content')

<!-- --------------------common--fotter--------------------------------- -->

        <!-- Pricing Section Start -->
        <!-- call us start -->
        <section class="pricing_section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h5 class="sub_title lights">Our mission is to deliver reliable, latest news and opinions.</h5>
                        <h2 class="sec_title lights">We are Here to Help You</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="pricing_01 text-center">
                            <div class="p01_head">
                                <h2><i class="fa-solid fa-calendar-days"></i></h2>
                                <h5>APPLY FOR LOAN</h5>
                            </div>
                            <div class="p01_body">
                                <ul>
                                    <li>Looking for a Loan ?</li>
                                    <li>Apply for loan now</li>
                                </ul>
                            </div>
                            <div class="p01_footer">
                                <a href="{{ route('post.show','contact') }}"><span>Get Appointment<i class="bx bx-right-arrow-alt"></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="pricing_01 text-center active">
                            <div class="p01_head">
                                <h2><i class="fa-solid fa-phone"></i></h2>
                                <h5>CALL US AT</h5>
                            </div>
                            <div class="p01_body">
                                <ul>
                                    <li class="contact-number" style="font-size: 30px;"><b>+91-1149090549</b></li>
                                    <li>info@royalfinserv.co.in</li>
                                    <li>royal.finserv@gmail.com</li>
                                </ul>
                            </div>
                            <div class="p01_footer">
                                <a href="{{ route('post.show','contact') }}"><span>Contact us<i class="bx bx-right-arrow-alt"></i></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="pricing_01 text-center">
                            <div class="p01_head">
                                <h2><i class="fa-solid fa-people-line"></i></h2>
                                <h5>TALK TO ADVISOR</h5>
                            </div>
                            <div class="p01_body">
                                <ul>
                                    <li>Need to loan advise ?</li>
                                    <li> Talk to our Loan advisors.</li>
                                </ul>
                            </div>
                            <div class="p01_footer">
                                <a href="{{ route('post.show','contact') }}"><span>Meet The Advisor<i class="bx bx-right-arrow-alt"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="notes">
                            Need a custom solutions for your organization <a href="#"><span>Contact Us Here</span><i class="bx bx-right-arrow-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- call us start -->
        <!-- Pricing Section End -->

        <!-- CTA Section Start -->
        {{-- <section class="cta_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <h2>Looking for a First-Class Business Plan Consultant?</h2>
                    </div>
                    <div class="col-md-4 col-lg-4 text-right">
                        <a href="#" class="fnc_btn wh"><span>Request a Quote<i class="bx bx-right-arrow-alt"></i></span></a>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- CTA Section End -->

        <!-- Footer Start -->
        <footer class="footer_01">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-3 col-xl-4">
                        <div class="widget">
                            <div class="about_widget">
                                <div class="aw_logo">
                                    <a href="#"><img src="{{ url('storage/' . $site->footer_logo) }}"
                                        alt="Logo of {{ $site->title }}" alt=""/></a>
                                </div>
                                <ul>
                                    <li><a href="active tel:{{ $site->phone }}">Phone No. :- {{ $site->phone }}</a></li>
                                    <li class=" address">
                                        <a href="mailto: {{ $site->email }}">E-mail :-{{ $site->email }}
                                        </a>
                                    </li>
                                    <li class=" address"><a href="#">Address:- {{$site->address}}</a></li>

                                </ul>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-2">
                        <div class="widget">
                            <h3 class="widget-title">Help Center</h3>
                            <ul>
                                @foreach($menuItems['footer-3'] as $mitem)
                                    @php
                                    $link = '';
                                    switch($mitem->type) {

                                    case 'post':
                                    $link = route('post.show', $mitem->post->slug);
                                    break;

                                    case 'category':
                                    $link = route('post.index', ['category' => @$mitem->category->slug]);
                                    break;

                                    case 'external':

                                    default:
                                    $link = $mitem->link;
                                    break;
                                    }
                                    @endphp
                                    <li>
                                        <a href="{{ $link }}">{{ $mitem->label }}</a>
                                    </li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>
                    @php
                    $loan = \App\Models\PostData::getPostData('loan', [
                        'conditions' => 'post_id IS NULL'
                    ]);
                    @endphp
                    <div class="col-md-6 col-lg-3 col-xl-2">
                        <div class="widget">
                            <h3 class="widget-title">Our Services</h3>
                            <ul>
                                {{-- @foreach($loan as $i => $l)
                                <li><a href="#">{{$l->title}}</a></li>
                                 @endforeach --}}
                                 @foreach($menuItems['footer-2'] as $mitem)
                                 @php
                                 $link = '';
                                 switch($mitem->type) {

                                 case 'post':
                                 $link = route('post.show', $mitem->post->slug);
                                 break;

                                 case 'category':
                                 $link = route('post.index', ['category' => @$mitem->category->slug]);
                                 break;

                                 case 'external':

                                 default:
                                 $link = $mitem->link;
                                 break;
                                 }
                                 @endphp
                                 <li>
                                     <a href="{{ $link }}">{{ $mitem->label }}</a>
                                 </li>
                                 @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-4 pdl95">
                        {{-- <div class="widget subscribe_widgets">
                            <h3 class="widget-title">Stay Updated</h3>
                            <div class="subscribe_content">
                                <p>
                                    Enter your email address into our subscribe form to keep yourself updating with our newslatter.
                                </p>
                                <form method="post" action="#">
                                    <input type="email" placeholder="Your Email *" name="s_email"/>
                                    <button type="submit" name="s_submit"><i class="bx bx-right-arrow-alt"></i></button>
                                </form>
                                <div class="form_note text-right">Get all <span>updates and offers</span></div>
                            </div>
                        </div> --}}
                        <div class="fo_social mt-4" >
                            @if(!empty($site->social_links['facebook']))
                            <a class="icon-box" href="{{$site->social_links['facebook']}}"><i class="bx bxl-facebook"></i></a>
                            @endif
                        @if(!empty($site->social_links['facebook']))
                        <a class="icon-box" href="{{$site->social_links['instagram']}}"><i class="bx bxl-instagram"></i></a>
                        @endif
                        @if(!empty($site->social_links['facebook']))
                        <a class="icon-box" href="{{$site->social_links['youtube']}}"><i class="bx bxl-youtube"></i></a>
                        @endif
                        @if(!empty($site->social_links['facebook']))
                        <a class="icon-box" href="{{$site->social_links['linkedin']}}"><i class="bx bxl-linkedin"></i></a>
                        @endif
                        @if(!empty($site->social_links['skype']))
                        <a class="icon-box" href="{{$site->social_links['skype']}}"><i class="bx bxl-skype"></i></a> 
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- Copyright Section Start -->
        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copbar"></div>
                        <p class="copies">
                            {{date('Y')}} © <a href="#">{{$site->title}}</a>. All rights reserved.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Copyright Section End -->
        
        <!-- Back To Top Start -->
        <a href="#" id="backtotop"><i class="bx bxs-arrow-to-top"></i></a>
        <!-- Back To Top End -->

        <!-- Include All JS START-->
        <script src="vyom/js/jquery.js"></script>
        <script src="vyom/js/jquery-ui.js"></script>
        <script src="vyom/js/bootstrap.min.js"></script>
        <script src="vyom/js/owl.carousel.min.js"></script>
        <script src="vyom/js/jquery.shuffle.min.js"></script>
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyBJtPMZ_LWZKuHTLq5o08KSncQufIhPU3o"></script>
        <script src="vyom/js/gmaps.js"></script>
        <script src="vyom/js/jquery.plugin.min.js"></script>
        <script src="vyom/js/jquery.countdown.min.js"></script>
        <script src="vyom/js/jquery.appear.js"></script>
        <script src="vyom/js/lightcase.js"></script>
        <script src="vyom/js/jquery.themepunch.tools.min.js"></script>
        <script src="vyom/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Include All JS END-->
        
        <!-- Rev slider Add on Start -->
        <script src="vyom/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vyom/js/extensions/revolution.extension.video.min.js"></script>
        <!-- Rev slider Add on End -->

        <script src="vyom/js/theme.js"></script>
        {!! $site->footer_script !!}
    </body>
</html>

