@extends('backend.layouts.inner')
@section('site_title','Add Media')
@section('content')

<div class="container">
       <div class="content">
            <div class="row">
                <div class="col-5">
                    {{ Form::open(['url' => route('admin.media.store'),'files' => true] ) }}
                    <div class="mb-3">
                        {{ Form::label('title') }}
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']); }}
                    </div>
                    <div class="mb-3">
                        {{ Form::label('type') }}
                        {{ Form::select('type', ['image'=>'Image', 'pdf'=>'PDF'],null, ['class' => 'form-control']); }}
                    </div>

                    <div class="mb-3">
                    {{ Form::label('file') }}
                    @if(!empty($post->file))
                        <label for="file" class="card mb-3" style="width:10rem;">
                            <img src="{{url('storage/'.$media->file)}}" alt=".." class="card-img-top" name="file">
                        </label>
                     @endif
                        <label class="btn btn-block btn-primary">
                            {{Form::file('file',['style'=>'display:none;']) }}
                            Choose File
                        </label>                        
                    </div>
                     <input class="btn btn-primary" type="submit" value="Submit">    
                     
                     {{ Form::close() }}
                </div>
            </div>

        </div>
        
    </div>

</div>

@endsection