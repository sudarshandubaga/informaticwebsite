@extends('backend.layouts.inner')
@section('site_title','View menu items')
@section('content')
<div class="card">
   <div class="card-header">
      <i class="fas fa-table mr-1"></i>
      DataTable Example
   </div>
   <div class="card-body">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
         @foreach($menus as $i => $m)
         <li class="nav-item">
            <a class="nav-link{{$i == 0 ? ' active' : ''}}" id="home-tab-{{ $m->id }}" data-toggle="tab" href="#home-{{ $m->id }}" role="tab" aria-controls="home" aria-selected="true">{{ $m->name }}</a>
         </li>
         @endforeach
      </ul>
      <div class="tab-content" id="myTabContent">
         @foreach($menus as $i => $m)
         <div class="tab-pane fade{{ $i == 0 ? ' show active' : ''}}" id="home-{{ $m->id }}" role="tabpanel" aria-labelledby="home-tab">
            <div class="menu-items mt-3">
               <ul class="ul-sortable">
                     @foreach($m->menu_items as $mitem)
                     <li class="item-{{ $mitem->id }}" data-id="{{ $mitem->id }}">
                        <div class="menu-item-list">
                            <strong>{{ $mitem->label }}</strong>
                            <div>
                                {{ $mitem->type }}
                    
                                <!-- <a href=""> <i class="fas fa-ellipsis-v"></i> </a> -->
                                <div class="dropdown">
                                    <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('admin.menu-item.edit', [$mitem->id]) }}">Edit</a>
                                        {{ Form::open(['url' => route('admin.menu-item.destroy', [$mitem->id]), 'method' => 'delete']) }}
                                        <button class="dropdown-item">Delete</button>
                                        {{ Form::close() }}
                                    </div>
                                </div>  
                            </div>
                        </div>
                        @if(!$mitem->menu_item->isEmpty())
                        @include('backend.pages.menu_item.recursive', ['mitems' => $mitem->menu_item])
                        @endif
                     </li>
                     @endforeach
               </ul>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</div>
@endsection

@section('extra_scripts')
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script>
    $( function() {
        $( ".ul-sortable" ).sortable({
            axis: "y",
            update: function (event, ui) {
                // console.log('sort: ', $(this).sortable('serialize'));
                var li_ids = [];
                $.each($(this).find('li'), function (i, row) {
                    li_ids.push($(row).data('id'));
                });
                // console.log('sort: ', data);
                $.ajax({
                    url: "{{ route('admin.menu-item.update.sort') }}",
                    method: 'POST',
                    data: {
                        ids: li_ids,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(res) {
                        console.log('order updated.');
                    }
                });
            }
        });
    });
    </script>
@endsection