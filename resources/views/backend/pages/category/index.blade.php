@extends('backend.layouts.inner')
@section('site_title','View Page')
@section('content')



<main>
                    <div class="container-fluid">
                    <h1 class="mt-4">View {{ $post_type->name }} Category</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard')  }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.category.create', [$post_type->slug])  }}">{{ $post_type->name }} Category</a>
            </li>
            <li class="breadcrumb-item active">View {{ $post_type->name }} Category</li>
         </ol>
                     
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                DataTable Example
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                        <th>S.NO</th>
                                        <th>Name</th>
                                        <th>Discription</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach( $category as $i => $c)
                                            <tr>
                                                <td>{{ $i + $category->firstItem() }}</td>
                                                <td>{{ $c->title}}</td>
                                                <td>{{ $c->description }}</td>
                                                <td>                        <img src="{{ url('storage/'.$c->image) }}" alt="" style="width: 50px; height: 50px; object-fit: contain"></td>
                                                <td>  
                                        <div class="dropdown">
                                        <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('admin.category.edit', [$post_type->slug, $c->slug]) }}">Edit</a>
                                        {{ Form::open(['url' => route('admin.category.destroy', [$post_type->slug, $c->slug]), 'method' => 'delete']) }}
                                        <button class="dropdown-item">Delete</button>
                                        {{ Form::close() }}
                                        </div>
                                        </div>
                                              </td>                                             
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
        @endsection