<div class="row">
    <div class="col-sm-8">
        <div class="mb-3">
            {{ Form::label('title') }}
            {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
        </div>
        <div class="mb-3">
            {{ Form::label('slug') }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter SEO URL']) }}
        </div>
        <div class="mb-3">
            {{ Form::label('category_id', 'Parent Category') }}
            {{ Form::select('category_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'ROOT']) }}
        </div>
    </div>
    <div class="col-sm-4">
        <label class="d-block">
            <img src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg?20200913095930" alt="" class="image-preview" id="imagePreview">
            <input type="file" name="image" id="category_image" accept="image/*" data-id="imagePreview" class="image-picker d-none">
        </label>

        <label for="category_image" class="btn btn-primary btn-block">Choose Image</label>
    </div>
</div>
<div class="mb-3">
    {{ Form::label('description') }}
    {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
</div>
                