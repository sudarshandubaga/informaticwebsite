@extends('backend.layouts.inner')
@section('site_title','View Page')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">{{ $post_type->name }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard')}}">Dasgboard</a>
        </li>
        <li class="breadcrumb-item active">View {{$post_type->name}}</li>
    </ol>

        {{Form::open(['method'=>'GET'])}}

            <div class="form-outline mb-4">
            {{ Form::label('search_by_keyword', null, ['class' => 'form-label']) }}
            {{ Form::search('search_by_keyword', request('search_by_keyword'), ['class' => 'form-control', 'placeholder' => 'Search By Keyword']) }}
            </div>
            <div id="datatable">
            </div>

        {{Form::close()}}

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            @include('backend.templates.messages')

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" cellspacing="0">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th>S.NO</th>
                            <th>Title</th>
                            <th style="white-space: nowrap">Short Description</th>
                            <th style="white-space: nowrap"> Description</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $i => $p)
                        <tr>
                            <td>{{ $i + $posts->firstItem() }}</td>
                            <td>{{ $p->title }}
                                @if(!empty($p->parent))
                                <br />
                                <small>&laquo; {{ $p->parent->title }}</small>
                                @endif
                            </td>
                            <td style="width: 60%;">{{ $p->excerpt }}</td>
                            <td>{{ $p->description }}</td>
                            <td>
                                <img src="{{ url($site->domain . '/images/'.$p->post_type.'/thumbnail/'.$p->image) }}" alt=""
                                    style="width: 50px; height: 50px; object-fit: contain">
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-link" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"
                                            href="{{ route('admin.post.edit', [$post_type->slug, $p->slug]) }}">Edit</a>
                                        {{ Form::open([
                                            'url' => route('admin.post.destroy', [$post_type->slug, $p->slug]), 
                                            'method' => 'delete', 
                                            'class' => 'delete-form'
                                        ]) }}
                                        <button type="button" class="dropdown-item delete-btn">Delete</button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $posts->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
</div>
@endsection