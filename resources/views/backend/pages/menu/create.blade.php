@extends('backend.layouts.inner')
@section('site_title','Add items')
@section('content')


   <div class="container">
       <div class="content">
            <div class="row">
                <div class="col-5">
                    {{ Form::open(['url' => route('admin.menu.store')]) }}
                    <div class="mb-3">
                        {{ Form::label('name') }}
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Menu Name']); }}
                    </div>
                    
                    @foreach($menu_locations as $keyword => $name)

                    <div class="custom-control custom-checkbox mb-3">
                        {{ Form::checkbox('menu_location[]', $keyword, 0, ['class' => 'custom-control-input', 'id' => 'menu_location_' . $keyword]) }}
                        {{ Form::label('menu_location_' . $keyword, $name, ['class' => 'custom-control-label']) }}
                    </div>

                    @endforeach
    
                     <input class="btn btn-primary" type="submit" value="Submit">    
                     
                     {{ Form::close() }}
                </div>
            </div>

        </div>
        
    </div>

</div>

@endsection