@extends('backend.layouts.inner')
@section('site_title','View Page')
@section('content')



<main>
                    <div class="container-fluid">
                    <h1 class="mt-4">{{ $type }}</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard')  }}">Dashboard</a>
            </li>
           
            <li class="breadcrumb-item active">{{ $type }}</li>
         </ol>
                     
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                DataTable Example
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <!-- <th>Sender</th> -->
                                                <th>Subject</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <!-- <th>Receiver</th> -->
                                                <th>Type</th>
                                                <th>Date and Time</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($data as $key=>$da) 
                                            <tr>
                                                <td>{{$data->firstItem() + $key}}</td>
                                                <td><a href="{{route('admin.contact.show', $da->id)}}">{{$da->subject}}</a></td>
                                                <td>
                                                    <strong>{{$da->sender}}</strong><br>
                                                    ({{ $da->from }})
                                                </td>
                                                <td>
                                                    <strong>{{$da->receiver}}</strong><br>
                                                    ({{ $da->to }})
                                                </td>
                                                <td>{{$da->type}}</td>
                                                <td>
                                                    <div style="white-space: nowrap;">
                                                        {{ date("d/m/Y h:i A", strtotime($da->created_at)) }}
                                                    </div>
                                                </td>
                                                <td>  
                                                    {{ Form::open(['url' => route('admin.contact.destroy', [$da->id]), 'method' => 'delete']) }}
                                                    <button class="btn btn-sm btn-danger">Delete</button>
                                                    {{ Form::close() }}
                                                </td>                                             
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
        @endsection