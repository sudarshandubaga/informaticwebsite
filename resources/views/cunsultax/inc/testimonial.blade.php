@extends('web.layouts.app')
@section('main_section')
<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Testimonials</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li class="active">Testimonials</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

			<section class="wpb_row row-fluid row-o-equal-height row-flex section-padd no-bot">
			    <div class="container">
			        <div class="row">
			            <div class="wpb_column column_container col-sm-12">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="section-head ">
			                            <h6><span>Testimonial</span></h6>
			                            <h2 class="section-title">What Our Clients Say</h2>
			                        </div>

			                        <div class="wpb_text_column wpb_content_element">
			                            <div class="wpb_wrapper">
			                                <p>We pay attention to details and quality, good communication and strong customer relationship. Here is what they say about us.</p>
											<div class="empty_space_45"></div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Maria Sharapova<span class="font12 normal">from Berlin</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Quynh Anh<span class="font12 normal">from Hanoi</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Nora Roberts<span class="font12 normal">from Moscow</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Caroline Cummings<span class="font12 normal">from London</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. </p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Neil Tyson<span class="font12 normal">from Berlin</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.</p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Robert Dugoni<span class="font12 normal">from Berlin</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p>
			                            </div>
			                        </div>
									<div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>John Doe<span class="font12 normal">from NewYork</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p>
			                            </div>
			                        </div>
			                        <div class="empty_space_30 lg-hidden"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Kendra Elliot<span class="font12 normal">from Roma</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.</p>
			                            </div>
			                        </div>

			                        <div class="empty_space_30  lg-hidden"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="testi-item single ">
			                            <div class="testi-head">
			                                <img width="50" height="50" src="https://via.placeholder.com/50" alt="">
			                                <h5>Nora Roberts<span class="font12 normal">from Paris</span></h5>
			                            </div>
			                            <div class="line"></div>
			                            <div class="testi-content">
			                                <i class="ion-md-quote"></i>
			                                <img width="86" height="12" src="images/stars.png" alt="">
			                                <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p>
			                            </div>
			                        </div>

			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>

			<section class="wpb_row row-fluid section-padd">
			    <div class="container">
			        <div class="row">
			            <div class="wpb_column column_container col-sm-12">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="section-head ">
			                            <h6><span>Support</span></h6>
			                            <h2 class="section-title">How can we help</h2>
			                        </div>

			                        <div class="empty_space_30_12"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-7">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="wpcf7">
			                            <div class="screen-reader-response"></div>
			                            <form action="#" method="post" class="wpcf7-form">
			                                <div class="row">
			                                    <div class="col-md-6">
			                                    	<span class="wpcf7-form-control-wrap your-name">
				                                    	<input type="text" name="your-name" value="" size="40" class="wpcf7-form-control" required="" placeholder="Your Name">
				                                    </span>
			                                    </div>
			                                    <div class="col-md-6">
			                                    	<span class="wpcf7-form-control-wrap your-email">
			                                    		<input type="email" name="your-email" value="" size="40" class="wpcf7-form-control" required="" placeholder="Email Address">
			                                    	</span>
			                                    </div>
			                                </div>
			                                <div class="contact-mess">
			                                	<span class="wpcf7-form-control-wrap your-message">
			                                		<textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control" required="" placeholder="Messenger"></textarea>
				                                </span>
				                            </div>
			                                <p>
			                                    <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit btn">
			                                </p>
			                            </form>
			                        </div>
			                        <div class="empty_space_30_30 lg-hidden md-hidden"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-5">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="wpb_text_column wpb_content_element text-light contact-box">
			                            <div class="wpb_wrapper">
			                                <h6 class="font12">Hotline</h6>
			                                <h4>+917 8124 – 3527</h4>
			                                <h6 class="font12">Location</h6>
			                                <h4>307 Jesse Centers<br>
											London City, England 12568</h4>
			                                <p><a class="pagelink" href="contact.html">Contact Us</a></p>

			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>

		</div>
	    

@endsection