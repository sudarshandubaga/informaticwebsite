@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Financial Consulting</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li><a href="services.html">Services</a></li>
			                        <li class="active">Financial Consulting</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="entry-content">
				<div class="container">
					<div class="row">

						<div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
						    <main id="main" class="site-main">

						        <article class="ot_service type-ot_service status-publish has-post-thumbnail hentry">
						            <div class="inner-post">
						                <section class="wpb_row row-fluid">
						                    <div class="container">
						                        <div class="row">
						                            <div class="wpb_column column_container col-sm-12">
						                                <div class="column-inner">
						                                    <div class="wpb_wrapper">
						                                        <h2>Our specific work</h2>
						                                        <div class="wpb_text_column wpb_content_element">
						                                            <div class="wpb_wrapper">
						                                                <p>Just before European Union leaders agreed their guidelines for the Brexit negotiations last week, the president of the EU council, Donald Tusk, said: It is clear that progress on people, money and Ireland must come first. It was rather startling to find Irish concerns up there on the list of fundamental priorities, with the rights of EU citizens in Britain.</p>
						                                                <p>And when the guidelines were agreed, it was clear that this was more than rhetoric. EU governments have essentially committed themselves to allowing Northern Ireland to rejoin the EU if Ireland is united. This is a very big dealrather startling to find Irish concerns up there on the list of fundamental priorities,.</p>

						                                            </div>
						                                        </div>
						                                        <div class="empty_space_30"></div>
						                                        <h4>Our process</h4>
						                                        <div class="wpb_text_column wpb_content_element">
						                                            <div class="wpb_wrapper">
						                                                <p><strong><span class="orange">Step 1: </span></strong> Receive and Evaluate
						                                                    <br>
						                                                    <strong><span class="red">Step 2: </span></strong> Analysis and Planning
						                                                    <br>
						                                                    <strong><span class="green">Step 3: </span></strong> Make plans and Implement</p>

						                                            </div>
						                                        </div>
						                                        <div class="empty_space_30"></div>
						                                        <h4>Analysis charts and statistics</h4>
						                                        <img src="https://via.placeholder.com/700x410" alt="">
																<div class="empty_space_45"></div>
						                                        <h4>What you got ?</h4>
						                                        <div class="wpb_text_column wpb_content_element">
						                                            <div class="wpb_wrapper">
						                                                <p>Britain’s departure from the EU is (in principle) to be final; Northern Ireland’s is now contingent. Britain is getting a divorce; Northern Ireland is being offered a trial separation. For Britain, there is a one-way ticket; for Northern Ireland, there is an automatic right of return. The implicit offer is two unions for the price of one: unite Ireland and you reunite with Europe.</p>

						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                </section>
						            </div>
						        </article>

						    </main>
						    <!-- #main -->
						</div>

						<aside id="sidebar" class="widget-area service-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
						    <section id="nav_menu-1" class="widget widget_nav_menu">
						        <h4 class="widget-title">Services</h4>
						        <div class="menu-service-menu-container">
						            <ul id="menu-service-menu" class="menu">
						                <li><a href="service-detail.html">Audit &amp; Assurance</a></li>
						                <li class="current-menu-item"><a href="service-detail.html">Financial Consulting</a></li>
						                <li><a href="service-detail.html">Trades & Stocks</a></li>
						                <li><a href="service-detail.html">Strategic Planning</a></li>
						                <li><a href="service-detail.html">Financial Projections</a></li>
						                <li><a href="service-detail.html">Bonds & Commodities</a></li>
						                <li><a href="service-detail.html">Taxes and Efficiency</a></li>
						                <li><a href="service-detail.html">Finance and Restructuring</a></li>
						                <li><a href="service-detail.html">International Business</a></li>
						        </div>
						    </section>
						    <section id="text-1" class="widget widget_text bg-second text-light">
						        <h4 class="widget-title">Do you need support !</h4>
						        <div class="textwidget">
						            <ul class="semi-bold">
						                <li><span class="normal">Phone:</span> <a href="tel:+1253 1245 1245">+1253 1245 1245</a></li>
						                <li><span class="normal">Office London:</span> Flat 60, Ross Green,
						                    <br> South Lilyberg, Q7M 8ZV, London,
						                    <br> England
						                </li>
						                <li><span class="normal">Mail:</span> <a href="mailto:info.londonconsulting.com">info.londonconsulting.com</a></li>
						            </ul>
						            <div class="gaps style-parent"></div>
						            <div class="wpcf7">
						                <div class="screen-reader-response"></div>
						                <form action="#" method="post" class="wpcf7-form">
						                    <p>
						                    	<span class="wpcf7-form-control-wrap your-email">
							                    	<input type="email" name="your-email" value="" size="40" class="wpcf7-form-control" required="" placeholder="Email Address">
							                    </span>
						                    	<span class="wpcf7-form-control-wrap your-phone">
						                    		<input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control" required="true" placeholder="Phone Number">
						                    	</span>
						                        <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit btn">
						                    </p>
						                </form>
						            </div>
						        </div>
						    </section>
						</aside>

					</div>
				</div>
			</div>

		</div>
	    
@endsection