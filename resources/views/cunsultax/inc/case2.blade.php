@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Planning The Development</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li><a href="projects.html">Case Study</a></li>
			                        <li class="active">Planning The Development</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="entry-content">
				<div class="container">
					<div class="boxed-content">
					
						<section class="wpb_row row-fluid section-padd">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="section-head">
						                            <h6><span>project details</span></h6>
						                            <h2 class="section-title">Planning the development<br> for the viral world</h2>
						                        </div>
						                        <div class="empty_space_30"></div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-4">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_single_image wpb_content_element align_left">
						                            <div class="wpb_wrapper figure">
						                                <div class="single_image-wrapper box_border_grey">
						                                	<img src="https://via.placeholder.com/150x30" alt="logo2">
						                                </div>
						                            </div>
						                        </div>
						                        <div class="empty_space_12"></div>
						                        <div class="custom_heading">Contract Project: August 27, 2018</div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-4">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="empty_space_20 lg-hidden"></div>
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <div>
						                                	<strong class="font12 font-second">Website:</strong>
						                                	<a class="text-primary" href="https://themeforest.net/user/thememodern/portfolio" target="_blank">www.viralworld.com</a>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-4">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="empty_space_20 lg-hidden"></div>

						                        <div class="wpb_text_column wpb_content_element socials small text-right mobile-left">
						                            <div class="wpb_wrapper">
						                                <div>
						                                	<strong class="font12 font-second">Share: </strong>
						                                	<a href="#" target="_blank"><i class="fa fa-facebook-official"></i></a><a href="#" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

						<section class="wpb_row row-fluid row-full-width row-no-padding">
						    <div class="row">
					            <div class="wpb_column column_container col-sm-12">
					                <div class="column-inner">
					                    <div class="wpb_wrapper">
											<div class="image-carousel" data-show="1" data-arrow="true">

					                            <div>
										            <div class="image-item">
									                    <img src="https://via.placeholder.com/1170x550" alt="">
										            </div>
										        </div>

										        <div>
										            <div class="image-item">
									                    <img src="https://via.placeholder.com/1170x550" alt="">
										            </div>
										        </div>

										        <div>
										            <div class="image-item">
									                    <img src="https://via.placeholder.com/1170x550" alt="">
										            </div>
										        </div>

					                        </div>
					                    </div>
					                </div>
						        </div>
						    </div>
						</section>

						<section class="wpb_row row-fluid section-padd row-o-equal-height row-o-content-middle row-flex">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="section-head ">
						                            <h6><span>work process</span></h6>
						                            <h2 class="section-title">Description of the project</h2>
						                        </div>
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <p>Velit venenatis consequat donec ultricies accumsan orci, tincidunt habitasse non quisque malesuada urna suscipit, dapibus molestie pharetra sagittis vehicula. Ridiculus odio pulvinar lobortis vehicula quisque semper velit volutpat fermentum nunc proin, habitasse rhoncus per mollis cras auctor ac suscipit tincidunt ornare.</p>
						                                <div class="empty_space_30 md-hidden sm-hidden"></div>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-6">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_single_image wpb_content_element align_left">
						                            <figure class="wpb_wrapper figure">
						                                <div class="single_image-wrapper box_border_grey">
						                                	<img src="https://via.placeholder.com/750x550" alt="">
						                                </div>
						                            </figure>
						                        </div>
						                        <div class="empty_space_30 lg-hidden"></div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-6">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <h4>The Problem</h4>
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <p>Semper accumsan dignissim nec egestas proin torquent non fermentum ut sollicitudin viverra, sociis vulputate sodales leo praesent nulla dis sociosqu eleifend facilisi lacus.</p>

						                            </div>
						                        </div>
						                        <h4>The Strategy</h4>
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <p>Eu nascetur euismod viverra lacinia pretium vitae porta libero habitant phasellus pellentesque hac, velit tincidunt hendrerit ac varius vel nisi penatibus at sed rhoncus suspendisse.</p>

						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

						<section class="wpb_row row-fluid section-padd testi-section">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="section-head ">
						                            <h6><span>customer say</span></h6>
						                            <h2 class="section-title">What our clients say?</h2>
						                        </div>

						                        <div class="empty_space_30"></div>

						                        <div class="testi-slider" data-show="1" data-arrow="">

													<div>
										                <div class="testi-item-2">
										                    <img class="client-img" src="https://via.placeholder.com/200" alt="">                  
										                    <div class="client-info">
										                        <i class="ion-md-quote"></i>
										                        <p class="says italic">“I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence. I should be incapable of drawing a single stroke at the present moment.”</p>
										                        <div class="f-left">
										                            <h5>Maria Sharapova</h5>
										                            <span class="jobs font12">CEO Viral World</span>
										                        </div>
										                        <div class="f-left stars">
										                        	<img src="images/stars.png" alt="">
										                        </div>
										                    </div>
										                </div>
										            </div>
										            <div>
										                <div class="testi-item-2">
										                    <img class="client-img" src="https://via.placeholder.com/200" alt="">                  
										                    <div class="client-info">
										                        <i class="ion-md-quote"></i>
										                        <p class="says italic">“I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine and yet I feel that I never was a greater artist than now.”</p>
										                        <div class="f-left">
										                            <h5>Emily R. King</h5>
										                            <span class="jobs font12">Manager TRX</span>
										                        </div>
										                        <div class="f-left stars">
										                        	<img src="images/stars.png" alt="">
										                        </div>
										                    </div>
										                </div>
										            </div>
										            <div>
										                <div class="testi-item-2">
										                    <img class="client-img" src="https://via.placeholder.com/200" alt="">                  
										                    <div class="client-info">
										                        <i class="ion-md-quote"></i>
										                        <p class="says italic">“I should be incapable of drawing a single stroke at the present moment; I feel that I never was a greater artist than now. I feel that I never was a greater artist than now.”</p>
										                        <div class="f-left">
										                            <h5>Robert Dugoni</h5>
										                            <span class="jobs font12">CEO OCN Group</span>
										                        </div>
										                        <div class="f-left stars">
										                        	<img src="images/stars.png" alt="">
										                        </div>
										                    </div>
										                </div>
										            </div>

						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

					</div>
			    </div>
			</div>
		</div>
	    
@endsection