@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Contact Us</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li class="active">Contact Us</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="entry-content">
				<div class="container">
					<div class="boxed-content">
					
						<section class="wpb_row row-fluid section-padd no-bot">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12 col-md-9">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="section-head ">
						                            <h6><span>London Office</span></h6>
						                            <h2 class="section-title">149 OceanThemes St, Broughton Rd London, England</h2>
						                        </div>

						                        <div class="empty_space_12"><span class="empty_space_inner"></span></div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-8">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_text_column wpb_content_element contact-info">
						                            <div class="wpb_wrapper">
						                                <p><a href="mailto:info.consultax@gmail.com"><i class="fa fa-envelope"> info.consultax@gmail.com</i></a><a href="tel:+911 0113 0114"><i class="fa fa-phone-square"> +911 0113 0114</i></a></p>

						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-4">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_text_column wpb_content_element socials text-right md-hidden sm-hidden">
						                            <div class="wpb_wrapper">
						                                <p><a href="#" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook-official">fb</i></a><a href="#" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter">tw</i></a><a href="#" target="_blank" rel="noopener noreferrer"><i class="fa fa-pinterest">pr</i></a></p>

						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

						<section class="wpb_row row-fluid section-padd no-bot">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="gray-line">
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

						<section class="wpb_row row-fluid section-padd">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-12 col-md-7">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="section-head ">
						                            <h6><span>CONTACT FORM</span></h6>
						                            <h2 class="section-title">How can we help</h2>
						                        </div>

						                        <div class="empty_space_12"><span class="empty_space_inner"></span></div>
						                        <div role="form" class="wpcf7" id="wpcf7-f1989-p967-o1" lang="en-US" dir="ltr">
						                            <div class="screen-reader-response"></div>
						                            <form action="#" method="post" class="wpcf7-form">
						                                <div class="row">
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-fname"><input type="text" name="your-fname" value="" size="40" class="wpcf7-form-control" required="" placeholder="First Name"></span></div>
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-lname"><input type="text" name="your-lname" value="" size="40" class="wpcf7-form-control" required="" placeholder="Last Name"></span></div>
						                                </div>
						                                <div class="row">
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control" placeholder="Phone Number"></span></div>
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control" required="" placeholder="Email Address"></span></div>
						                                </div>
						                                <div class="row">
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-company"><input type="text" name="your-company" value="" size="40" class="wpcf7-form-control" placeholder="Your Company"></span></div>
						                                    <div class="col-md-6"><span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control" placeholder="Subject"></span></div>
						                                </div>
						                                <div class="contact-mess"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control" required="" placeholder="Messenger"></textarea></span></div>
						                                <p>
						                                    <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit btn">
						                                </p>
						                            </form>
						                        </div>
						                        <div class="empty_space_30 lg-hidden"><span class="empty_space_inner"></span></div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-12 col-md-5">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_single_image wpb_content_element align_center">
						                            <figure class="wpb_wrapper figure">
						                                <div class="single_image-wrapper box_border_grey"><img src="https://via.placeholder.com/750x990" class="single_image-img attachment-full" alt=""></div>
						                            </figure>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>
								
						<img src="https://via.placeholder.com/1170x500?text=google+map" alt="">

						<section class="wpb_row row-fluid address-section">
						    <div class="container">
						        <div class="row">
						            <div class="wpb_column column_container col-sm-6">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <h4>London Office</h4>
						                                <p>007 OceanThemes St, Broughton Rd, London, England</p>
														<div class="empty_space_30"><span class="empty_space_inner"></span></div>
						                            </div>
						                        </div>

						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <h4>Berlin Office</h4>
						                                <p>Hans-Wilhelm-Gasse 0/3, 98051 Schwarzenberg</p>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						            <div class="wpb_column column_container col-sm-6">
						                <div class="column-inner">
						                    <div class="wpb_wrapper">
						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <h4>New York Office</h4>
						                                <p>6803 Dickens Islands Apt. 567, Port Malikaview, TX 14942</p>
														<div class="empty_space_30"><span class="empty_space_inner"></span></div>
						                            </div>
						                        </div>

						                        <div class="wpb_text_column wpb_content_element">
						                            <div class="wpb_wrapper">
						                                <h4>Madrid Office</h4>
						                                <p>Travessera Eric, 896, 59° 8°, 61008, Sarabia Alta</p>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</section>

					</div>
			    </div>
			</div>
		</div>
	    
@endsection