@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Blog</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li class="active">Blog</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="entry-content">
				<div class="container">
					<div class="row">
					
						<div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12">
						    <main id="main" class="site-main">

						        <article class="post-box post type-post hentry">
						            <div class="entry-media">
						                <a href="post.html">
						                    <img src="https://via.placeholder.com/800x350" alt="">
						                </a>
						            </div>
						            <div class="inner-post">
						                <header class="entry-header">

						                    <div class="entry-meta">
						                        <span class="posted-on">
						                        	<time class="entry-date published">August 14, 2018</time>
						                        </span>
						                        <span class="posted-in">
						                        	<a href="#" rel="category tag">Business</a>
						                        </span>
						                    </div>
						                    <!-- .entry-meta -->
						                    <h4 class="entry-title"><a href="post.html" rel="bookmark">Investment Update, Fourth Quarter 2018</a></h4>
						                </header>
						                <!-- .entry-header -->

						                <div class="entry-summary">
						                    <p> Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt senectus felis platea natoque mattis. Dis lacinia pellentesque interdum tincidunt cubilia massa egestas primis ullamcorper velit ultrici molestie dui in feugiat lobortis erat vivamus hac condimentum est…</p>
						                </div>
						                <!-- .entry-content -->

						                <footer class="entry-footer">
						                    <a class="post-link" href="post.html">Read more</a>
						                </footer>
						                <!-- .entry-footer -->
						            </div>
						        </article>

						        <article class="post-box post type-post hentry">
						            <div class="entry-media">
						                <a href="post.html">
						                    <img src="https://via.placeholder.com/800x350" alt="">
						                </a>
						            </div>
						            <div class="inner-post">
						                <header class="entry-header">

						                    <div class="entry-meta">
						                        <span class="posted-on">
						                        	<time class="entry-date published">August 14, 2018</time>
						                        </span>
						                        <span class="posted-in">
						                        	<a href="#" rel="category tag">Business</a>
						                        </span>
						                    </div>
						                    <!-- .entry-meta -->
						                    <h4 class="entry-title"><a href="post.html" rel="bookmark">Plans for growing businesses</a></h4>
						                </header>
						                <!-- .entry-header -->

						                <div class="entry-summary">
						                    <p> Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt senectus felis platea natoque mattis. Dis lacinia pellentesque interdum tincidunt cubilia massa egestas primis ullamcorper velit ultrici molestie dui in feugiat lobortis erat vivamus hac condimentum est…</p>
						                </div>
						                <!-- .entry-content -->

						                <footer class="entry-footer">
						                    <a class="post-link" href="post.html">Read more</a>
						                </footer>
						                <!-- .entry-footer -->
						            </div>
						        </article>

						        <article class="post-box post type-post hentry">
						            <div class="entry-media">
						                <a href="post.html">
						                    <img src="https://via.placeholder.com/800x350" alt="">
						                </a>
						            </div>
						            <div class="inner-post">
						                <header class="entry-header">

						                    <div class="entry-meta">
						                        <span class="posted-on">
						                        	<time class="entry-date published">August 14, 2018</time>
						                        </span>
						                        <span class="posted-in">
						                        	<a href="#" rel="category tag">Business</a>
						                        </span>
						                    </div>
						                    <!-- .entry-meta -->
						                    <h4 class="entry-title"><a href="post.html" rel="bookmark">Cutting Your Restaurant’s Operations Costs</a></h4>
						                </header>
						                <!-- .entry-header -->

						                <div class="entry-summary">
						                    <p> Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt senectus felis platea natoque mattis. Dis lacinia pellentesque interdum tincidunt cubilia massa egestas primis ullamcorper velit ultrici molestie dui in feugiat lobortis erat vivamus hac condimentum est…</p>
						                </div>
						                <!-- .entry-content -->

						                <footer class="entry-footer">
						                    <a class="post-link" href="post.html">Read more</a>
						                </footer>
						                <!-- .entry-footer -->
						            </div>
						        </article>

						        <article class="post-box post type-post hentry">
						            <div class="entry-media">
						                <a href="post.html">
						                    <img src="https://via.placeholder.com/800x350" alt="">
						                </a>
						            </div>
						            <div class="inner-post">
						                <header class="entry-header">

						                    <div class="entry-meta">
						                        <span class="posted-on">
						                        	<time class="entry-date published">August 14, 2018</time>
						                        </span>
						                        <span class="posted-in">
						                        	<a href="#" rel="category tag">Business</a>
						                        </span>
						                    </div>
						                    <!-- .entry-meta -->
						                    <h4 class="entry-title"><a href="post.html" rel="bookmark">Solution financial for good startup</a></h4>
						                </header>
						                <!-- .entry-header -->

						                <div class="entry-summary">
						                    <p> Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt senectus felis platea natoque mattis. Dis lacinia pellentesque interdum tincidunt cubilia massa egestas primis ullamcorper velit ultrici molestie dui in feugiat lobortis erat vivamus hac condimentum est…</p>
						                </div>
						                <!-- .entry-content -->

						                <footer class="entry-footer">
						                    <a class="post-link" href="post.html">Read more</a>
						                </footer>
						                <!-- .entry-footer -->
						            </div>
						        </article>

						        <ul class="page-pagination none-style">
						            <li><span aria-current="page" class="page-numbers current">1</span></li>
						            <li><a class="page-numbers" href="#">2</a></li>
						            <li><a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a></li>
						        </ul>

						    </main>
						    <!-- #main -->
						</div>

						<aside id="sidebar" class="widget-area primary-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
						    <section id="search-2" class="widget widget_search">
						        <form role="search" method="get" id="search-form" class="search-form" action="#">
						            <input type="search" class="search-field" placeholder="Enter keyword..." value="" name="s">
						            <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
						        </form>
						    </section>
						    <section id="categories-2" class="widget widget_categories">
						        <h4 class="widget-title">Categories</h4>
						        <ul>
						            <li><a href="#">Business</a></li>
						            <li class="cat-item cat-item-3"><a href="#">Consulting</a></li>
						            <li class="cat-item cat-item-4"><a href="#">Finacial</a></li>
						            <li class="cat-item cat-item-5"><a href="#">Franchising</a></li>
						            <li class="cat-item cat-item-6"><a href="#">Personal Injury</a></li>
						            <li class="cat-item cat-item-1"><a href="#">Uncategorized</a></li>
						        </ul>
						    </section>
						    <section id="recent_news-1" class="widget widget_recent_news">
						        <h4 class="widget-title">Latest News</h4>
						        <ul class="recent-news clearfix">

						            <li class="clearfix ">
						                <div class="thumb">
						                    <a href="post.html">
						                        <img src="https://via.placeholder.com/68x70" alt="">
						                    </a>
						                </div>
						                <div class="entry-header">
						                    <h6>
						                        <a href="post.html">Investment Update, Fourth Quarter 2018</a>
						                    </h6>
						                    <span class="post-on">
						                        <span class="entry-date">August 14, 2018</span>
						                    </span>
						                </div>
						            </li>
						            <li class="clearfix ">
						                <div class="thumb">
						                    <a href="post.html">
						                        <img src="https://via.placeholder.com/68x70" alt="">
						                    </a>
						                </div>
						                <div class="entry-header">
						                    <h6>
						                        <a href="post.html">Cutting Your Restaurant’s Operations Costs</a>
						                    </h6>
						                    <span class="post-on">
						                        <span class="entry-date">April 22, 2018</span>
						                    </span>
						                </div>
						            </li>
						            <li class="clearfix ">
						                <div class="thumb">
						                    <a href="post.html">
						                        <img src="https://via.placeholder.com/68x70" alt="">
						                    </a>
						                </div>
						                <div class="entry-header">
						                    <h6>
						                        <a href="post.html">Solution financial for good startup</a>
						                    </h6>
						                    <span class="post-on">
						                        <span class="entry-date">September 11, 2017</span>
						                    </span>
						                </div>
						            </li>

						        </ul>

						    </section>
						    <section id="archives-3" class="widget widget_archive">
						        <h4 class="widget-title">Archives</h4>
						        <label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
						        <select id="archives-dropdown-3" name="archive-dropdown">

						            <option value="">Select Month</option>
						            <option value="#"> August 2018 </option>
						            <option value="#"> April 2018 </option>
						            <option value="#"> September 2017 </option>
						            <option value="#"> May 2017 </option>
						            <option value="#"> March 2017 </option>
						            <option value="#"> July 2016 </option>
						            <option value="#"> December 2015 </option>

						        </select>
						    </section>
						    <section id="tag_cloud-2" class="widget widget_tag_cloud">
						        <h4 class="widget-title">Tags</h4>
						        <div class="tagcloud"><a href="#">Advisor</a>
						            <a href="#">Agency</a>
						            <a href="#">Business</a>
						            <a href="#">Consuting</a>
						            <a href="#">Finacial</a>
						            <a href="#">Franchising</a></div>
						    </section>
						</aside>

					</div>
			    </div>
			</div>
		</div>
	    
@endsection