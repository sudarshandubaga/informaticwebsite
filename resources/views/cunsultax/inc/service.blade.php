@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Our Services</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li class="active">Our Services</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<section class="wpb_row row-fluid row-o-equal-height row-flex section-padd">
			    <div class="container">
			        <div class="row">
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Audit &amp; Assurance</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Financial Consulting</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Trades &amp; Stocks</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Strategic Planning</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Financial Projections</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Bonds &amp; Commodities</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Taxes and Efficiency</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30  lg-hidden"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>Finance and Restructuring</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                        <div class="empty_space_30  lg-hidden md-hidden"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="wpb_column column_container col-sm-6 col-md-4">
			                <div class="column-inner">
			                    <div class="wpb_wrapper">
			                        <div class="service-box image-box ">
			                            <img src="https://via.placeholder.com/750x550" alt="">
			                            <div class="content-box">
			                                <h4>International Business</h4>
			                                <p>Senectus accumsan malesuada cursus dapibus sem primis cubilia, per potenti fermentu massa pulvinar turpis taciti, pellentesque.</p>
			                                <a class="link-box pagelink" href="service-detail.html">Read more</a> </div>
			                        </div>

			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
		</div>
	    
@endsection