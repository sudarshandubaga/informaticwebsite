@extends('web.layouts.app')
@section('main_section')
<div class="header_mobile">
	            <div class="mlogo_wrapper clearfix">
	                <div class="mobile_logo">
	                    <a href="#"><img src="images/logo-white.svg" alt="Consultax"></a>
	                </div>
	                <div id="mmenu_toggle">
	                    <button></button>
	                </div>
	            </div>
	            <div class="mmenu_wrapper">
	                <div class="mobile_nav collapse">
	                    <ul id="menu-main-menu" class="mobile_mainmenu">
	                        <li class="current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="index.html">Home</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-home current-page_item page-item-1530 current_page_item menu-item-2017"><a href="index.html" aria-current="page">Home 1</a></li>
	                                <li class="menu-item-2016"><a href="home-2.html">Home 2</a></li>
	                                <li class="menu-item-2015"><a href="home-3.html">Home 3</a></li>
	                                <li class="menu-item-2059"><a href="home-4.html">Home 4</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1731"><a href="#">Pages</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1738"><a href="about.html">About Us</a></li>
	                                <li class="menu-item-1745"><a href="team.html">Our Team</a></li>
	                                <li class="menu-item-1742"><a href="how-it-work.html">How It Work</a></li>
	                                <li class="menu-item-1746"><a href="testimonials.html">Testimonials</a></li>
	                                <li class="menu-item-1757"><a href="services.html">Services Box</a></li>
	                                <li class="menu-item-1744"><a href="services-icon.html">Icon Box</a></li>
	                                <li class="menu-item-1740"><a href="career.html">Career</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1789"><a href="services.html">Services</a>
	                            <ul class="sub-menu">
	                                <li class="menu-item-1791"><a href="service-detail.html">Financial Consulting</a></li>
	                                <li class="menu-item-1758"><a href="service-detail.html">International Business</a></li>
	                                <li class="menu-item-1790"><a href="service-detail.html">Audit &amp; Assurance</a></li>
	                                <li class="menu-item-1760"><a href="service-detail.html">Taxes and Efficiency</a></li>
	                                <li class="menu-item-1761"><a href="service-detail.html">Bonds &amp; Commodities</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="projects.html">Cases Study</a>
	                            <ul class="sub-menu">
	                                <li><a href="projects.html">Cases Study 2 Columns</a></li>
	                                <li><a href="projects-2.html">Cases Study 3 Columns</a></li>
	                                
	                                <li><a href="project-detail.html">Cases Study Details</a></li>
	                            </ul>
	                        </li>
	                        <li class="menu-item-has-children"><a href="blog.html">Blog</a>
	                            <ul class="sub-menu">
	                                <li><a href="blog.html">Blog List</a></li>
	                                <li><a href="post.html">Blog Details</a></li>
	                            </ul>
	                        </li>
	                        <li><a href="contact.html">Contact</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </header>

		<div id="content" class="site-content">
			<div class="page-header">
			    <div class="container">
			        <div class="breadc-box no-line">
			            <div class="row">
			                <div class="col-md-6">
			                    <h1 class="page-title">Cases Studies 2 Columns</h1>
			                </div>
			                <div class="col-md-6 mobile-left text-right">
			                    <ul id="breadcrumbs" class="breadcrumbs none-style">
			                        <li><a href="index.html">Home</a></li>
			                        <li class="active">Cases Studies 2 Columns</li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>


			
			<section class="section-padd padd-bot-90">
				<div class="container">
					<div class="section-head ">
					    <h6><span>featured project</span></h6>
					    <h2 class="section-title">We are the leaders</h2>
					</div>
					<div class="empty_space_12"></div>
					<div class="project-slider projects" data-show="1" data-auto="true" data-arrow="true" data-dot="">
						<div>
			                <div class="project-item">
			                    <img class="radius" src="https://via.placeholder.com/1170x500" alt="">
			                    <div class="inner">
			                        <p class="contract">Contract Project: January 12, 2019</p>
			                        <h4><a href="project-detail.html">IEM Financial Statements</a></h4>
			                        <a class="pagelink" href="project-detail.html">View details</a>
			                    </div>
			                </div>       
					    </div>
					    <div>
			                <div class="project-item">
			                    <img class="radius" src="https://via.placeholder.com/1170x500" alt="">
			                    <div class="inner">
			                        <p class="contract">Contract Project: April 24, 2016</p>
			                        <h4><a href="project-detail.html">Business Growth Solutions</a></h4>
			                        <a class="pagelink" href="project-detail.html">View details</a>
			                    </div>
			                </div>       
					    </div>
					    <div>
			                <div class="project-item">
			                    <img class="radius" src="https://via.placeholder.com/1170x500" alt="">
			                    <div class="inner">
			                        <p class="contract">Contract Project: May 19, 2019</p>
			                        <h4><a href="project-detail.html">Planning The Development</a></h4>
			                        <a class="pagelink" href="project-detail.html">View details</a>
			                    </div>
			                </div>       
					    </div>
					</div>
					<div class="empty_space_70"></div>
					<div class="project-filter" data-column="2">
					    <div id="filters" class="cat-filter">
					        <a href="#" data-filter="*" class="filter-item all-cat selected">All</a>
					        <a href="#" data-filter=".audit-assurance" class="filter-item">Audit &amp; Assurance</a>
					        <a href="#" data-filter=".restructuring" class="filter-item">Restructuring</a>
					        <a href="#" data-filter=".strategic-planning" class="filter-item">Strategic Planning</a>
					        <a href="#" data-filter=".trades-stocks" class="filter-item">Trades &amp; Stocks</a>
					    </div>
					    <div id="projects" class="project-grid projects row ">
					        <div class="project-item trades-stocks col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt=""> </a>
					                <div class="p-info">
					                    <h6><span>Trades &amp; Stocks</span></h6>
					                    <h4>
			                                <a href="project-detail.html">Financial Report 2019</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					        <div class="project-item strategic-planning col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt="">
					                </a>
					                <div class="p-info">
					                    <h6><span>Strategic Planning</span></h6>
					                    <h4>
			                                <a href="project-detail.html">MO Insurance Finance</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					        <div class="project-item audit-assurance col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt="">
					                </a>
					                <div class="p-info">
					                    <h6><span>Audit &amp; Assurance</span></h6>
					                    <h4>
			                                <a href="project-detail.html">Enterprise Loan 2016</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					        <div class="project-item restructuring trades-stocks col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt="">
					                </a>
					                <div class="p-info">
					                    <h6><span>Restructuring</span><span>Trades &amp; Stocks</span></h6>
					                    <h4>
			                                <a href="project-detail.html">Money Market 2018</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					        <div class="project-item audit-assurance restructuring col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt="">
					                </a>
					                <div class="p-info">
					                    <h6><span>Audit &amp; Assurance</span><span>Restructuring</span></h6>
					                    <h4>
			                                <a href="project-detail.html">2019 Retirement Plan</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					        <div class="project-item strategic-planning col-md-6 col-sm-6">
					            <div class="inner">
					                <a href="project-detail.html">
					                    <img src="https://via.placeholder.com/750x450" alt="">
					                </a>
					                <div class="p-info">
					                    <h6><span>Strategic Planning</span></h6>
					                    <h4>
			                                <a href="project-detail.html">MO Insurance Pack 2</a>
			                            </h4>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
			</section>

		</div>
	    
@endsection
