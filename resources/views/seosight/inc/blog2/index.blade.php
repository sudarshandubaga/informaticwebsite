<!-- Right Menu -->
@extends('web.layouts.app')
@section('main_section')



<div class="mCustomScrollbar" data-mcs-theme="dark">

	<div class="popup right-menu">

		<div class="right-menu-wrap">

			<div class="user-menu-close js-close-aside">
				<a href="#" class="user-menu-content  js-clode-aside">
					<span></span>
					<span></span>
				</a>
			</div>

			<div class="logo">
				<a href="index.html" class="full-block-link"></a>
				<img src="img/logo-eye.png" alt="Seosight">
				<div class="logo-text">
					<div class="logo-title">Seosight</div>
				</div>
			</div>

			<p class="text">Investigationes demonstraverunt lectores legere me lius quod
				ii legunt saepius est etiam processus dynamicus.
			</p>

		</div>

		<div class="widget login">

			<h4 class="login-title">Sign In to Your Account</h4>
			<input class="email input-standard-grey" placeholder="Username or Email" type="text">
			<input class="password input-standard-grey" placeholder="Password" type="password">
			<div class="login-btn-wrap">

				<div class="btn btn-medium btn--dark btn-hover-shadow">
					<span class="text">login now</span>
					<span class="semicircle"></span>
				</div>

				<div class="remember-wrap">

					<div class="checkbox">
						<input id="remember" type="checkbox" name="remember" value="remember">
						<label for="remember">Remember Me</label>
					</div>

				</div>

			</div>

			<div class="helped">Lost your password?</div>
			<div class="helped">Register Now</div>

		</div>


		<div class="widget contacts">

			<h4 class="contacts-title">Get In Touch</h4>
			<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
				vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum.
			</p>

			<div class="contacts-item">
				<img src="img/contact4.png" alt="phone">
				<div class="content">
					<a href="#" class="title">8 800 567.890.11</a>
					<p class="sub-title">Mon-Fri 9am-6pm</p>
				</div>
			</div>

			<div class="contacts-item">
				<img src="img/contact5.png" alt="phone">
				<div class="content">
					<a href="#" class="title">info@seosight.com</a>
					<p class="sub-title">online support</p>
				</div>
			</div>

			<div class="contacts-item">
				<img src="img/contact6.png" alt="phone">
				<div class="content">
					<a href="#" class="title">Melbourne, Australia</a>
					<p class="sub-title">795 South Park Avenue</p>
				</div>
			</div>

		</div>

	</div>

</div>

<!-- End Right Menu -->

<div class="content-wrapper">

	<!-- Stunning Header -->

	<div class="stunning-header stunning-header-bg-lightviolet">
		<div class="stunning-header-content">
			<h1 class="stunning-header-title">Blog Details</h1>
			<ul class="breadcrumbs">
				<li class="breadcrumbs-item">
					<a href="index.html">Home</a>
					<i class="seoicon-right-arrow"></i>
				</li>
				<li class="breadcrumbs-item active">
					<span href="#">Blog Details</span>
					<i class="seoicon-right-arrow"></i>
				</li>
			</ul>
		</div>
	</div>

	<!-- End Stunning Header -->

	<!-- Overlay Search -->

	<div class="overlay_search">
		<div class="container">
			<div class="row">
				<div class="form_search-wrap">
					<form>
						<input class="overlay_search-input" placeholder="Type and hit Enter..." type="text">
						<a href="#" class="overlay_search-close">
							<span></span>
							<span></span>
						</a>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- End Overlay Search -->

	<!-- Post Details -->


	<div class="container">
		<div class="row medium-padding120">
			<main class="main">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<article class="hentry post post-standard-details">

						<div class="post-thumb">
							<img src="img/blog-details.jpg" alt="seo">
						</div>

						<div class="post__content">

							<h2 class="h2 post__title entry-title ">
								<a href="#">Standard Post Format</a>
							</h2>


							<div class="post-additional-info">

								<div class="post__author author vcard">
									<img src="img/avatar-b-details.png" alt="author">
									Posted by

									<div class="post__author-name fn">
										<a href="#" class="post__author-link">Admin</a>
									</div>

								</div>

								<span class="post__date">

								<i class="seoicon-clock"></i>

								<time class="published" datetime="2016-03-20 12:00:00">
									March 20, 2016
								</time>

							</span>

								<span class="category">
								<i class="seoicon-tags"></i>
								<a href="#">Business,</a>
								<a href="#">Seo</a>
							</span>

								<span class="post__comments">
								<a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
								6
							</span>

							</div>

							<div class="post__content-info">

								<p class="post__subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
									euismod tincidunt ut laoreet dolore.
								</p>

								<p class="post__text">Investigationes demonstraverunt lectores legere me lius quod ii legunt
									saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
									lectorum. Mirum
									<span class="c-primary">est notare quam littera gothica</span>, quam nunc putamus parum claram,
									anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima
									eodem modo typii quam nunc putamus parum claram, anteposuerit.
								</p>

								<div class="testimonial-item quote-left">

									<h5 class="h5 testimonial-text">
										Mirum est notare quam littera gothica, quam nunc putamus parum claram,
										anteposuerit litterarum formas humanitatis placerat facer possim assum.
									</h5>

									<div class="author-info-wrap table">
										<div class="author-info table-cell">
											<h6 class="author-name c-primary">Angelina Johnson</h6>
											<div class="author-company">Envato Market</div>
										</div>
									</div>

									<div class="quote">
										<i class="seoicon-quotes"></i>
									</div>

								</div>

								<p class="post__text">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
									suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure
									dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu
									feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit
									praesent luptatum quam nunc putamus parum claram, anteposuerit litterarum formas.
								</p>

								<h4 class="mb30">Qum Soluta Nobis Eleifend</h4>

								<p class="post__text">Iriure dolor in hendrerit in vulputate velit esse molestie consequat,
									vel illum dolore eu feugiat <span class="c-dark">nulla facilisis at vero eros</span>
									et accumsan et iusto odio dignissim qui blandit praesent luptatum quam nunc putamus parum claram.
								</p>

								<ul class="list list--secondary">
									<li>
										<i class="seoicon-check"></i>
										<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
											nonummy nibh <span class="c-primary">euismod tincidunt;</span>
										</a>
									</li>
									<li>
										<i class="seoicon-check"></i>
										<a href="#">Mirum est notare quam littera gothica;</a>
									</li>
									<li>
										<i class="seoicon-check"></i>
										<a href="#">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse
											molestie consequat, vel illum dolore eu feugiat nulla;
										</a>
									</li>
									<li>
										<i class="seoicon-check"></i>
										<a href="#">Investigationes demonstraverunt lectores.</a>
									</li>
								</ul>

								<p class="post__text">Quis autem vel eum iriure dolor in hendrerit in vulputate velit esse
									molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan
									et iusto odio dignissim qui blandit praesent quam nunc putamus parum claram.
								</p>

								<div class="widget w-tags">
									<div class="tags-wrap">
										<a href="#" class="w-tags-item">SEO</a>
										<a href="#" class="w-tags-item">Advertising</a>
										<a href="#" class="w-tags-item">Business</a>
										<a href="#" class="w-tags-item">Optimization</a>
									</div>
								</div>

							</div>
						</div>

						<div class="socials">Share:
							<a href="" class="social__item">
								<i class="seoicon-social-facebook"></i>
							</a>
							<a href="" class="social__item">
								<i class="seoicon-social-twitter"></i>
							</a>
							<a href="" class="social__item">
								<i class="seoicon-social-linkedin"></i>
							</a>
							<a href="" class="social__item">
								<i class="seoicon-social-google-plus"></i>
							</a>
							<a href="" class="social__item">
								<i class="seoicon-social-pinterest"></i>
							</a>
						</div>

					</article>

					<div class="blog-details-author">

						<div class="blog-details-author-thumb">
							<img src="img/blog-details-author.png" alt="Author">
						</div>

						<div class="blog-details-author-content">
							<div class="author-info">
								<h5 class="author-name">Philip Demarco</h5>
								<p class="author-info">SEO Specialist</p>
							</div>
							<p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
								nonummy nibh euismod.
							</p>
							<div class="socials">

								<a href="" class="social__item">
									<img src="svg/circle-facebook.svg" alt="facebook">
								</a>

								<a href="" class="social__item">
									<img src="svg/twitter.svg" alt="twitter">
								</a>

								<a href="" class="social__item">
									<img src="svg/google.svg" alt="google">
								</a>

								<a href="" class="social__item">
									<img src="svg/youtube.svg" alt="youtube">
								</a>

							</div>
						</div>
					</div>

					<div class="pagination-arrow">

						<a href="#" class="btn-prev-wrap">
							<svg class="btn-prev">
								<use xlink:href="#arrow-left"></use>
							</svg>
							<div class="btn-content">
								<div class="btn-content-title">Next Post</div>
								<p class="btn-content-subtitle">Claritas Est Etiam Processus</p>
							</div>
						</a>

						<a href="#" class="btn-next-wrap">
							<div class="btn-content">
								<div class="btn-content-title">Previous Post</div>
								<p class="btn-content-subtitle">Duis Autem Velius</p>
							</div>
							<svg class="btn-next">
								<use xlink:href="#arrow-right"></use>
							</svg>
						</a>

					</div>

					<div class="comments">

						<div class="heading">
							<h4 class="h1 heading-title">6 Comments</h4>
							<div class="heading-line">
								<span class="short-line"></span>
								<span class="long-line"></span>
							</div>
						</div>

						<ol class="comments__list">

							<li class="comments__item">

								<div class="comment-entry comment comments__article">

									<div class="comment-content comment">
										<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
											anteposuerit litterarum formas humanitatis per seacula quarta
											et quinta decima.
										</p>
									</div>

									<div class="comments__body display-flex">

										<a href="#" class="reply">
											<i class=" seoicon-arrow-back"></i>
										</a>

										<div class="comments__avatar">

											<img src="img/post-author3.png" alt="avatar">

										</div>

										<header class="comment-meta comments__header">

											<cite class="fn url comments__author">
												<a href="#" rel="external" class=" ">Jonathan Simpson</a>
											</cite>

											<div class="comments__time">
												<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
													<span class="at">at</span> 4:27 pm
												</time>
											</div>

										</header>

									</div>

								</div>
							</li>

							<li class="comments__item">

								<div class="comment-entry comment comments__article">

									<div class="comment-content comment">
										<p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
									</div>

									<div class="comments__body display-flex">

										<a href="#" class="reply">
											<i class=" seoicon-arrow-back"></i>
										</a>

										<div class="comments__avatar">

											<img src="img/post-author2.png" alt="avatar">

										</div>

										<header class="comment-meta comments__header">

											<cite class="fn url comments__author">
												<a href="#" rel="external">Angelina Johnson</a>
											</cite>

											<div class="comments__time">
												<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
													<span class="at">at</span> 4:27 pm
												</time>
											</div>

										</header>

									</div>

								</div>

								<ol class="children">

									<li class="comments__item">

										<div class="comment-entry comment comments__article">

											<div class="comment-content comment">
												<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
													anteposuerit litterarum formas humanitatis per seacula quarta decima et
													quinta decima facer possim assum.
												</p>
											</div>

											<div class="comments__body display-flex">

												<a href="#" class="reply">
													<i class=" seoicon-arrow-back"></i>
												</a>

												<div class="comments__avatar">

													<img src="img/post-author1.png" alt="avatar">

												</div>

												<header class="comment-meta comments__header">

													<cite class="fn url comments__author">
														<a href="#" rel="external">Philip Demarco</a>
														<span class="replied">replied Angelina</span>
													</cite>

													<div class="comments__time">
														<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
															<span class="at">at</span> 4:27 pm
														</time>
													</div>

												</header>

											</div>

										</div>

										<ol class="children">

											<li class="comments__item">

												<div class="comment-entry comment comments__article">

													<div class="comment-content comment">
														<p>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
														</p>
													</div>

													<div class="comments__body display-flex">

														<a href="#" class="reply">
															<i class=" seoicon-arrow-back"></i>
														</a>

														<div class="comments__avatar">

															<img src="img/post-author2.png" alt="avatar">

														</div>

														<header class="comment-meta comments__header">

															<cite class="fn url comments__author">
																<a href="#" rel="external">Angelina Johnson</a>
																<span class="replied">replied Angelina</span>
															</cite>

															<div class="comments__time">
																<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
																	<span class="at">at</span> 4:27 pm
																</time>
															</div>

														</header>

													</div>

												</div>
											</li>

										</ol>
									</li>

								</ol>
							</li>

							<li class="comments__item">

								<div class="comment-entry comment comments__article">

									<div class="comment-content comment">
										<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
											anteposuerit litterarum formas humanitatis per seacula quarta decima et
											quinta decima.
										</p>
									</div>

									<div class="comments__body display-flex">

										<a href="#" class="reply">
											<i class=" seoicon-arrow-back"></i>
										</a>

										<div class="comments__avatar">

											<img src="img/post-author3.png" alt="avatar">

										</div>

										<header class="comment-meta comments__header">

											<cite class="fn url comments__author">
												<a href="#" rel="external" class=" ">Jonathan Simpson</a>
											</cite>

											<div class="comments__time">
												<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
													<span class="at">at</span> 4:27 pm
												</time>
											</div>

										</header>

									</div>

								</div>


								<ol class="children">

									<li class="comments__item">

										<div class="comment-entry comment comments__article">

											<div class="comment-content comment">
												<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
													anteposuerit litterarum formas humanitatis per seacula quarta decima et
													quinta decima facer possim assum.
												</p>
											</div>

											<div class="comments__body display-flex">

												<a href="#" class="reply">
													<i class=" seoicon-arrow-back"></i>
												</a>

												<div class="comments__avatar">

													<img src="img/post-author1.png" alt="avatar">

												</div>

												<header class="comment-meta comments__header">

													<cite class="fn url comments__author">
														<a href="#" rel="external">Philip Demarco</a>
														<span class="replied">replied Angelina</span>
													</cite>

													<div class="comments__time">
														<time class="published" datetime="2016-04-20 12:00:00">20.04.2016
															<span class="at">at</span> 4:27 pm
														</time>
													</div>

												</header>

											</div>

										</div>
									</li>
								</ol>
							</li>
						</ol>
					</div>

					<div class="row">

						<div class="leave-reply contact-form">

							<form>

								<div class="col-lg-12">
									<div class="heading">
										<h4 class="h1 heading-title">Leave a Comment</h4>
										<div class="heading-line">
											<span class="short-line"></span>
											<span class="long-line"></span>
										</div>
									</div>
								</div>

								<div class="col-lg-12">

									<input class="email input-standard-grey" name="full_name" id="full_name" placeholder="Your Full Name" value="" type="text">

								</div>

								<div class="col-lg-6">

									<input class="email input-standard-grey" name="comment_email" id="comment_email" placeholder="Email Address" value="" type="text">

								</div>

								<div class="col-lg-6">

									<input class="email input-standard-grey" name="comment_website" id="comment_website" placeholder="Website" value="" type="text">

								</div>

								<div class="col-lg-12">

									<textarea name="order_comments" class="input-text input-standard-grey" id="order_comments" placeholder="Your Comment"></textarea>

								</div>

								<div class="col-lg-12">

									<div class="submit-block table">
										<div class="col-lg-4 table-cell">
											<div class="btn btn-small btn--primary">
												<span class="text">Submit</span>
											</div>
										</div>

										<div class="col-lg-8 table-cell">
											<div class="submit-block-text">
												You may use these HTML tags and attributes: <span> &lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt;
											&lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt;
											&lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </span>
											</div>
										</div>

									</div>

								</div>
							</form>
						</div>
					</div>


				</div>

				<!-- End Post Details -->

				<!-- Sidebar-->

				<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12 col-xs-12">
					<aside aria-label="sidebar" class="sidebar sidebar-right">
						<div class="widget">
							<form class="w-search">
								<input class="email search input-standard-grey" required="required" placeholder="Search" type="search">
								<button class="icon">
									<i class="seoicon-loupe"></i>
								</button>
							</form>
						</div>

						<div class="widget w-post-category">
							<div class="heading">
								<h4 class="heading-title">Post Category</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
							</div>
							<div class="post-category-wrap">
								<div class="category-post-item">
									<span class="post-count">168</span>
									<a href="#" class="category-title">SEO
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">52</span>
									<a href="#" class="category-title">Local SEO
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">40</span>
									<a href="#" class="category-title">Social Media Marketing
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">33</span>
									<a href="#" class="category-title">Email Marketing
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">21</span>
									<a href="#" class="category-title">PPC Management
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">18</span>
									<a href="#" class="category-title">Technology
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>

								<div class="category-post-item">
									<span class="post-count">5</span>
									<a href="#" class="category-title">Business
										<i class="seoicon-right-arrow"></i>
									</a>
								</div>
							</div>
						</div>

						<div class="widget w-about">
							<div class="heading">
								<h4 class="heading-title">About Us and
									This Blog</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
								<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit
									litterarum formas humanitatis per seacula quarta decima quinta.
								</p>
							</div>

							<a href="02_abouts.html" class="btn btn-small btn-border c-primary">
								<span class="text">Learn More</span>
								<i class="seoicon-right-arrow"></i>
							</a>
						</div>

						<div class="widget w-request bg-boxed-red">
							<div class="w-request-content">
								<img src="img/request.png" alt="request">
								<h4 class="w-request-content-title">Request
									a Free Quote</h4>
								<p class="w-request-content-text">Gothica, quam nunc putamus parum claram, anteposuerit
									litterarum formas humanitatis.
								</p>

								<a href="22_contacts.html" class="btn btn-small btn--dark btn-hover-shadow">
									<span class="text">Contact Now</span>
								</a>
							</div>
						</div>

						<div class="widget w-latest-news">
							<div class="heading">
								<h4 class="heading-title">Latest News</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
							</div>

							<div class="latest-news-wrap">
								<div class="latest-news-item">
									<div class="post-additional-info">

								<span class="post__date">

									<i class="seoicon-clock"></i>

									<time class="published" datetime="2016-04-23 12:00:00">
										April 23, 2016
									</time>

								</span>

									</div>

									<h5 class="post__title entry-title ">
										<a href="15_blog_details.html">Eodem modo typi, qui nunc nobis videntur</a>
									</h5>
								</div>

								<div class="latest-news-item">
									<div class="post-additional-info">

								<span class="post__date">

									<i class="seoicon-clock"></i>

									<time class="published" datetime="2016-04-08 12:00:00">
										April 8, 2016
									</time>

								</span>

									</div>

									<h5 class="post__title entry-title ">
										<a href="15_blog_details.html">Investigationes demonstraverunt lectores legere</a>
									</h5>
								</div>

								<div class="latest-news-item">
									<div class="post-additional-info">

								<span class="post__date">

									<i class="seoicon-clock"></i>

									<time class="published" datetime="2016-03-25 12:00:00">
										March 25, 2016
									</time>

								</span>

									</div>

									<h5 class="post__title entry-title ">
										<a href="15_blog_details.html">Qemonstraverunt legere</a>
									</h5>
								</div>

								<div class="latest-news-item">
									<div class="post-additional-info">

								<span class="post__date">

									<i class="seoicon-clock"></i>

									<time class="published" datetime="2016-03-12 12:00:00">
										March 12, 2016
									</time>

								</span>

									</div>

									<h5 class="post__title entry-title ">
										<a href="15_blog_details.html">Ut wisi enim ad minim veniam, quis nostrud exerci</a>
									</h5>
								</div>

							</div>

							<a href="14_blog.html" class="btn btn-small btn--dark btn-hover-shadow">
								<span class="text">All News</span>
								<i class="seoicon-right-arrow"></i>
							</a>
						</div>

						<div class="widget w-follow">
							<div class="heading">
								<h4 class="heading-title">Follow Us</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
							</div>

							<div class="w-follow-wrap">
								<div class="w-follow-item facebook-bg-color">
									<a href="#" class="w-follow-social__item table-cell">
										<i class="seoicon-social-facebook"></i>
									</a>
									<a href="#" class="w-follow-title table-cell">Facebook
										<span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
									</a>
								</div>
								<div class="w-follow-item twitter-bg-color">
									<a href="#" class="w-follow-social__item table-cell">
										<i class=" seoicon-social-twitter"></i>
									</a>
									<a href="#" class="w-follow-title table-cell">Twitter
										<span class="w-follow-add active">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
									</a>
								</div>
								<div class="w-follow-item linkedin-bg-color">
									<a href="#" class="w-follow-social__item table-cell">
										<i class="seoicon-social-linkedin"></i>
									</a>
									<a href="#" class="w-follow-title table-cell">Linkedin
										<span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
									</a>
								</div>
								<div class="w-follow-item google-bg-color">
									<a href="#" class="w-follow-social__item table-cell">
										<i class="seoicon-social-google-plus"></i>
									</a>
									<a href="#" class="w-follow-title table-cell">Google+
										<span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
									</a>
								</div>
								<div class="w-follow-item pinterest-bg-color">
									<a href="#" class="w-follow-social__item table-cell">
										<i class="seoicon-social-pinterest"></i>
									</a>
									<a href="#" class="w-follow-title table-cell">Pinterest
										<span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
									</a>
								</div>
							</div>

						</div>

						<div class="widget w-tags">
							<div class="heading">
								<h4 class="heading-title">Popular Tags</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
							</div>

							<div class="tags-wrap">
								<a href="#" class="w-tags-item">SEO</a>
								<a href="#" class="w-tags-item">Advertising</a>
								<a href="#" class="w-tags-item">Business</a>
								<a href="#" class="w-tags-item">Optimization</a>
								<a href="#" class="w-tags-item">Digital Marketing</a>
								<a href="#" class="w-tags-item">Social</a>
								<a href="#" class="w-tags-item">Keyword</a>
								<a href="#" class="w-tags-item">Strategy</a>
								<a href="#" class="w-tags-item">Audience</a>
							</div>
						</div>
					</aside>
				</div>

				<!-- End Sidebar-->

			</main>
		</div>
	</div>

	<!-- Subscribe Form -->

	<div class="container-fluid bg-green-color">
		<div class="row">
			<div class="container">

				<div class="row">

					<div class="subscribe scrollme">

						<div class="col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5 col-sm-12 col-xs-12">
							<h4 class="subscribe-title">Email Newsletters!</h4>
							<form class="subscribe-form" method="post" action="import.php">
								<input class="email input-standard-grey input-white" name="email" required="required" placeholder="Your Email Address" type="email">
								<button class="subscr-btn">subscribe
									<span class="semicircle--right"></span>
								</button>
							</form>
							<div class="sub-title">Sign up for new Seosignt content, updates, surveys & offers.</div>

						</div>

						<div class="images-block">
							<img src="img/subscr-gear.png" alt="gear" class="gear">
							<img src="img/subscr1.png" alt="mail" class="mail">
							<img src="img/subscr-mailopen.png" alt="mail" class="mail-2">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- End Subscribe Form -->

</div>
@endsection