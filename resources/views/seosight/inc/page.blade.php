@extends('seosight.layouts.app')
@section('main_section')

<div class="content-wrapper">

	<!-- Stunning header -->

	<div class="stunning-header stunning-header-bg-blue">
		<div class="stunning-header-content">
			<h1 class="stunning-header-title">{{ $post->title }}</h1>
			<ul class="breadcrumbs">
				<li class="breadcrumbs-item">
					<a href="{{ url('/') }}">Home</a>
					<i class="seoicon-right-arrow"></i>
				</li>
				<li class="breadcrumbs-item active">
					<span href="#">{{ $post->title }}</span>
					<i class="seoicon-right-arrow"></i>
				</li>
			</ul>
		</div>
	</div>

	<!-- End Stunning header -->


	<div class="container my-5">
		<div style="min-height: 100vh;">
			@if(!empty($post->image))
			<div class="pull-left">
				<img src="{{ url('storage/' . $post->image) }}" alt="">
			</div>
			@endif
	
			{!! $post->description !!}
		</div>
	</div>

	
</div>
@endsection